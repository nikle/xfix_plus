function oc(a)
{
	var o = {};
	
	for(var i=0;i<a.length;i++){o[a[i]]='';}
	return o;
}

function buildExternal(){

	$('a').each(function(){
		try{
			domain = $(this).attr("href").match(/:\/\/(.[^/]+)/)[1];
			
			exceptions = new Array('test.newark.org','www.newark.org','newark.org');
			
			/* OPEN PDFS AND EXTERNAL LINKS IN NEW WINDOW */
			if((domain in oc(exceptions)) || ($(this).attr("href").indexOf('.pdf') !== -1)){}else{
				$(this).addClass('extlink').attr("title","opens in external window");		

				$(this).unbind('click').bind('click',function(){
					window.open($(this).attr("href"));
					
					return false;		
				});
			}
		}catch(e){}		
	});
	
	$("a[href*=.pdf]").prepend('<img src="/images/pdf_icon.gif" class="pdf" />');
}



$(function() {

$('input[type="radio"],input[type="checkbox"]').addClass("radio");

$(".accordion").accordion({ autoHeight: false });

// ALTERNATE TABLE ROW COLORS
$('table.gray tr:odd').addClass('odd');
$('table.gray tr:even').addClass('even');
$('table.blue tr:odd').addClass('odd');
$('table.blue tr:even').addClass('even');

// NEW WINDOW(S) FOR ON-SITE FILES
$("a[href*=.pdf]").click(function(){
	window.open(this.href);
	return false;
});
$("a[href*=.gif]").click(function(){
	window.open(this.href);
	return false;
});
$("a[href*=.jpg]").click(function(){
	window.open(this.href);
	return false;
});
$("a[href*=.png]").click(function(){
	window.open(this.href);
	return false;
});

	$("#sidebar ul ul").each(function(){
			var span = $('<span class="expandr"></span>');
	
			$(this).prev('a').prepend(span);
			
			$(span).click(function(){
				var span = $(this);
				
				$(this).parent().siblings('ul').toggle(function(){
					if($(this).css('display') == "block"){
						$(span).addClass("collapse");
						$(span).removeClass("collapsed");
					}else{
						$(span).removeClass("collapse");					
						$(span).toggleClass("collapsed");
					}
				});

				return false;			
			});


	});

	/* BULD EXTERNAL LINKS */
	buildExternal();

	/* MAIN NAV DROPDOWNS */
/*	$("#mainNav > li").hover(
		function(){
			$(this).children('ul').show();
		},
		function(){
			$(this).children('ul').hide();
		}
	);
	$("#mainNav .nav-sub > li").hover(
		function(){
			$(this).children('ul').show();
		},
		function(){
			$(this).children('ul').hide();
		}
	);
*/
	$("#mainNav").superfish(); 

	/* CYCLE */
	try{
		$("#slideShow").cycle({
			fx:      'fade', 
			speed:    1000, 
			timeout:  10000 		
		});
	}catch(e){}
	
	/* SITE MAP */
	$("ul#sitemap > li > a").each(function(){
	
		if ($(this).text() == "Departments")
		{
			$(this).parent().attr("id", "departments"); 
		}
	
	});

	/* QUICK NAV DROPDOWNS */
	$(".select").hover(
		function(){
			$(this).children('ul').addClass("over");
		},
		function(){
			$(this).children('ul').removeClass("over");		
		}
	);

	// CLEAR SEARCH BOXES
	$('.default').each(function() {
		var default_value = this.value;
		$(this).focus(function() {
			if(this.value == default_value) {
			this.value = '';
			}
		});
		$(this).blur(function() {
			if(this.value == '') {
			this.value = default_value;
			}
		});
	});

	// NEWS TICKER
	if ($(".newsticker ul li").length > 1){
		// $(".old .newsticker").jCarouselLite({
		// 	vertical:true,
		// 	hoverPause:true,
		// 	visible:4,
		// 	auto:2000,
		// 	speed:4000
		// });	
		
		$(".new .newsticker").jCarouselLite({
			vertical:true,
			visible: 3,
			speed: 1000,
			scroll: 3,
			btnNext: "#newstickerWrapper .pagination .next",
			btnPrev: "#newstickerWrapper .pagination .prev"
		});
	}
	
	// AJAX CALENDAR
	loadcal();
	
	// BACK TO TOP LINK
	$("#backToTop").click(function(){
		$.scrollTo(0, 400);
		return false;
	});
	
	// TABLE SORT
	$("#tableSort").tablesorter({
		sortList: [[0,0]], 
		widthFixed: true,
		widgets: ['zebra'],
		headers: {
			2: {
				sorter: "text"
			},	
			3: {
				sorter: false
			},
			4: {
				sorter: false
			},
			6: {
				sorter: false
			}

		}
	
	
	}).tablesorterPager({container: $("#pager")});;
	
});

function loadcal() {
	$("#prevcal, #nextcal").unbind("click").bind("click", function() {
	$("#calendarWrap").load($(this).attr("href"), function() {loadcal();});
	return false;
	});
}


// JUMP MENUS
function initJumpMenus() {
	// Turns all <select> elements with the 'jumpmenu' class into jump menus
	var selectElements = document.getElementsByTagName("select");
	for( i = 0; i < selectElements.length; i++ ) {
		// Check for the class and make sure the element has an ID
		if( selectElements[i].className == "jumpmenu" && document.getElementById(selectElements[i].id) != "" ) {
			jumpmenu = document.getElementById(selectElements[i].id);
			jumpmenu.onchange = function() {
				if( this.options[this.selectedIndex].value != '' ) {
					// Redirect
					//location.href=this.options[this.selectedIndex].value;
					window.open(this.options[this.selectedIndex].value);
				}
			}
		}
	}
}
window.onload = function() {
	initJumpMenus();
}


// OPEN EXTERNAL LINKS IN A NEW TAB/WINDOW
//$("a[rel*='external']").attr('target','_blank');

// ADD CLASS TO EXTERNAL LINKS
// $("a[rel*='external']").attr('class','extlink');