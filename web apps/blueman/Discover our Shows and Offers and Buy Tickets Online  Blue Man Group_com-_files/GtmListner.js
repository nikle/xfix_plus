﻿
var isMobile = window.matchMedia("only screen and (max-width: 760px)");

if (window.dataLayer && window.dataLayer[0] && window.dataLayer[0].page && window.dataLayer[0].page.device == "") {
    if (isMobile.matches) {
        window.dataLayer[0].page.device = "mobile";
    } else {
        window.dataLayer[0].page.device = "desktop";
    }
}


$(document).ready(function () {
    function initGtmButtons() {
        var bns = $('[data-gtm]');
        
        var i;
        for (i = 0; i < bns.length; i++) {
            bns[i].addEventListener("click", pushEvent);
        };

        function cleanJson(strJson) {
            var s = strJson.replace(/\\n/g, "\\n")
                .replace(/\\'/g, "\\'")
                .replace(/\\"/g, '\\"')
                .replace(/\\&/g, "\\&")
                .replace(/\\r/g, "\\r")
                .replace(/\\t/g, "\\t")
                .replace(/\\b/g, "\\b")
                .replace(/\\f/g, "\\f");

            // remove non-printable and other non-valid JSON chars
            s = s.replace(/[\u0000-\u0019]+/g, "");
            return JSON.parse(s);

        }

        function buildTagProperties(obj) {
            return cleanJson(obj.dataset["gtm"]);
        }

        function pushEvent() {
            var eventAttributes = buildTagProperties(this);

            if (eventAttributes) {
                for (var i = 0; i < eventAttributes.length; i++) {
                    window.dataLayer.push(eventAttributes[i]);
                }
            }

        }

        //for (i = 0; i < gtm.length; i++) {
        //    gtm[i].addEventListener("click", pushEvent);
        //};
    }

    function init() {
        initGtmButtons();
    }

    init();
});