var WCAGApp = (function () {
    'use strict';
    
    var WCAGAppView = Backbone.View.extend({

        initializeTemplate: function() {
            this.template = _.template($('#' + this.template).html())
        }

        , events: {
			'click .js-wcag-switcher': 'wcagSwitch',
		}

		, initialize: function (options) {

			if (options.template){
				this.template = options.template
			}

			this.initializeTemplate()
            this.listenTo(this.model, 'change', this.render)

		}
        , wcagSwitch: function (event) {

            var dataset = event.currentTarget.dataset
                , action = dataset.action || null
                , key = dataset.key || null

            if((!_.isNull(action))&&(!_.isNull(key))){
                if (_.indexOf(this.model.switchers, action) > -1){
                    this.model.set(action, key)
                }

            }


            event.preventDefault()
        }

		, render: function () {

			if (this.model.changed.id !== undefined) {
				return;
			}

			this.$el.html(this.template(this.model.toJSON()))

			return this;
		}

    })

    var WCAGAppButtonView = Backbone.View.extend({

        initializeTemplate: function() {
            this.template = _.template($('#' + this.template).html())
        }

        , events: {
			'click': 'toggleWcag',
		}

		, initialize: function (options) {

			if (options.template){
				this.template = options.template;
			}

			this.initializeTemplate();
            this.listenTo(this.model, 'change:showplane', this.render)

		}
        ,toggleWcag: function (event) {

            var showplane = this.model.get("showplane")
            if(!_.isUndefined(showplane)){
                var state = showplane == "no" ? "yes" : "no"

                this.model.set({"showplane": state})
            }
            event.preventDefault()
        }


		, render: function () {

			if (this.model.changed.id !== undefined) {
				return;
			}

			this.$el.html(this.template(this.model.toJSON()))

			return this;
		}

    })

    var WCAGApp = Backbone.Model.extend({
        switcherSelector: "js-wcag-switcher"
        ,   switcherItemSelector: "js-wcag-panel-item"
        ,   switcherItemSelectorActive: "active"
        ,   wcagPlaneID: "wcag-panel"
        ,   wcagTemplate: "wcag-template"
        ,   wcagButtonID: "js-wcag-button"
        ,   wcagButtonTemplate: "wcag-button-template"
        ,   storageKey: "wcag-panel-app"
        ,   switchers: ["fontsize", "images", "color", "showplane"]
        ,   defaults: {
                "fontsize": "normal",
                "images": "yes",
                "color": "white",
                "showplane": "no"

        }


        , initialize: function (options) {
            this.$controlPanel = document.getElementById(this.wcagPlaneID)
            this.$collapseButton = document.getElementById(this.wcagButtonID)
            this.document = document.documentElement

            // get current state
            this.setInintialState()

            // init view
            this.planeView = new WCAGAppView({
                model: this,
                el: this.$controlPanel,
                template: this.wcagTemplate

            })

            this.buttonView = new WCAGAppButtonView({
                model: this,
                el: this.$collapseButton,
                template: this.wcagButtonTemplate
            })

            this.render()
            this.on('change', this.syncState)

        }

        , setInintialState: function () {
            var storage = this.getStorage()
                , state = localStorage.getItem(this.storageKey)
            if (state) {
                this.set(JSON.parse(state))

            }

            this.syncState()

        }

        , syncState:function () {

            var storage = this.getStorage()
                , state = this.toJSON()

            localStorage[this.storageKey] = JSON.stringify(state)
            if (this.get("showplane")=="yes"){
                // set document attrs
                _.each(_.pairs(state), function (item) {
                      this.document.dataset[item[0]] = item[1]
                }, this)
            }else{
            // dell document attrs
                _.each(this.switchers, function (item) {
                  this.document.removeAttribute("data-" + item)
                }, this)

            }


        }

        , getStorage:  function () {

            return localStorage
        }

        , render: function () {
            this.planeView.render()
            this.buttonView.render()

        }

    })

    return WCAGApp;
})();