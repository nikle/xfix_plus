/**
 * Created by maydakov on 15.02.2017.
 */
var SPAPP = SPAPP || {};

(function ($) {
	'use strict';

	// Todo Entity View
	// --------------

	// The DOM element for an entity item...
	SPAPP.EntityView = Backbone.View.extend({

		tagName:  'li'
		,	className: 'block-item'
		,   base_template: 'entity-template'
		,	base_type: 'baseentity'
        // Cache the template function for a single item.
    	,   initializeTemplate: function(prefix) {

			var $temp =  $('#' + this.app_name + '-' + prefix + '-' + this.model.get("type") + '-' + this.base_template);

			if (!$temp.length){
				$temp = $('#' + this.app_name + '-' + prefix + '-' + this.base_type + '-' + this.base_template);
			}

            this.template = _.template($temp.html());

        }

		// The DOM events specific to an item.
		,	events: {
			//'click .toggle': 'toggleCompleted',
			//'dblclick label': 'edit',
			//'click .destroy': 'clear',
			//'keypress .edit': 'updateOnEnter',
			//'keydown .edit': 'revertOnEscape',
			//'blur .edit': 'close'
		},

		initialize: function (options) {

			this.className = this.className + ' ' + options.templatePrefix + '-item';

			if (options.template){
				this.template = options.template;
			}
			this.app_name = options.app_name;

			this.initializeTemplate(options.templatePrefix);

			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'destroy', this.remove);
		},

		render: function () {

			if (this.model.changed.id !== undefined) {
				return;
			}

			this.$el.html(this.template(this.model.toJSON()));
			return this;
		},

		// Remove the item, destroy the model from *localStorage* and delete its view.
		clear: function () {
			this.model.destroy();
		}
	});
})(jQuery);
