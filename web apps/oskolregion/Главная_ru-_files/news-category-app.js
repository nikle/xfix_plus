/**
 * Created by maydakov on 15.02.2017.
 */
var SPAPP = SPAPP || {};
(function () {
	'use strict';

	SPAPP.NewsEntitiesAPP = SPAPP.EntitiesAPP.extend({

		 initialize: function (options) {
			 if(_.isUndefined(options.entitiesPreviewSelector)){
				 this.set({'entitiesPreviewSelector': '#entity-preview-' + options.app_name}, {silent: true});
			 }
			 if(_.isUndefined(options.itemPreviewTempletePrefix)){
				 this.set({'itemPreviewTempletePrefix': 'preview-' + this.get('itemTempletePrefix')}, {silent: true});
			 }
			 this.$entitiesPreview = $(this.get('entitiesPreviewSelector'));

			 if (options && options.spitCount){
				 this.spitCount = options.spitCount
			 }else {
				 this.spitCount = 4
			 }

			SPAPP.EntitiesAPP.prototype.initialize.call(this, options);

		 }


		,  eddEntity: function (entity, iterator) {
			var classname = this.get('itemTempletePrefix')+'-item item'
			if (iterator==0){
				classname = classname + ' active'
			}
			var view = new this.EntityView({
				tagName:  'li',
				model: entity ,
				className: classname,
				app_name: this.app_name,
				templatePrefix: this.get('itemTempletePrefix')});


			this.$entitiesContainer.append(view.render().el);

			return view

		}
		,  addPreview: function (entity) {

			var view = new this.EntityView({
				tagName:  'a',
				model: entity ,
				attributes: {'href': entity.get_absolute_url()},
				className: 'media-img',
				app_name: this.app_name,
				templatePrefix: this.get('itemPreviewTempletePrefix')});


			this.$entitiesPreview.html(view.render().el);

			return view

		}

		, addAllEntities: function () {
			this.$entitiesContainer.html('');
			if (this.entities.length>0){
				this.addPreview(this.entities.at(0));
				this.entities.each(this.eddEntity, this);
			}


		}

	});

})();