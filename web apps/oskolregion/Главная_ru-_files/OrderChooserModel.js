/**
 * Created by maydakov on 15.02.2017.
 */
var SPAPP = SPAPP || {};

(function () {
	'use strict';

	// Order Chooser Model
	// ----------

	SPAPP.OrderChooserModel = Backbone.Model.extend({
		defaults: {
			title: 'Order',
			value: 'value',
			choices: []
		}

		// Toggle the `completed` state of this todo item.
		,	setOrder: function (order_value) {
			var choices = this.get("choices"),
			index = _.findIndex(choices, function (item) {
				return item['value'] == order_value
			});
			if (index > -1){
				var choice = choices[index];
				this.set({'title': choice['title'], 'value': choice['value']});
			}
		}

		,   server_api: function() {

			return { 'order' : this.get('value')}

            }


	});
})();