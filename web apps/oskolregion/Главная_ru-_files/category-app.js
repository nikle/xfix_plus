/**
 * Created by maydakov on 15.02.2017.
 */
var SPAPP = SPAPP || {};
(function () {
	'use strict';

	SPAPP.EntitiesAPP = Backbone.Model.extend({

		url: function () {

			return this.get('url')
		},

		initialize: function (options) {
			this.app_name = options.app_name;
			this.app_title = options.app_title
			this.stateFilters = [];

			this.$entitiesContainer = $(this.get('entitiesContainerSelector'));
			this.$entitisOrderSelector = $(this.get('entitisOrderSelector'));
			this.$statSelector = $(this.get('statSelector'));
			this.$paginatorSelector = $(this.get('paginatorSelector'));
			this.$filterSelector = $(this.get('filterSelector'));

			if (options.show_date_range){
				this.$dateRangeSelector = $(this.get('dateRangeSelector'));
				this.DateRangeModel = SPAPP.DateRangeModel;
				this.DateRangeView = SPAPP.DateRangeView;
				this.dateRange = new this.DateRangeModel(options.date_range);
			}

			this.RubricModel = SPAPP.RubricModel;


			this.RubricCollection = Backbone.Collection.extend({
				model: this.RubricModel

				, server_api: function () {

					var rubrics = this.where({is_selected: true}),
							res = _.map(rubrics, function(item){ return item.get("id"); });
					if (_.isArray(res)){
						return {'rb': res}
					}else{
						return {}
					}
				}
			});

			this.rubricator = new this.RubricCollection(options.rubrics);
			this.stateFilters.push(this.rubricator);
			// add rubricator view
			if (options.show_rubrics){
				this.RubricView = SPAPP.RubricView;
			}
			// add ordering
			this.OrderChooserModel = SPAPP.OrderChooserModel;
			this.orderChooser = new this.OrderChooserModel(options.orderChooser);
			this.stateFilters.push(this.orderChooser);

			if (options.showOrderChooser){

				this.OrderChooseView = SPAPP.OrderChooserView;

			}

			this.EntityView = SPAPP.EntityView;
			this.EntityModel = SPAPP.EntityModel;

			this.PaginatorModel = SPAPP.PaginatorModel;
			this.PaginatorView = SPAPP.PaginatorView;
			this.StatView = SPAPP.StatView;


			this.EntityCollection = Backbone.Collection.extend({
				model: this.EntityModel
				, url: options.url
			});



			this.entities = new this.EntityCollection(options.initialEntities);
			this.paginator = new this.PaginatorModel(options.paginator);
			this.stateFilters.push(this.paginator);

			if (options.show_date_range){
				this.stateFilters.push(this.dateRange);
				this.listenTo(this.dateRange, 'change', this.updateApp);
			}

			this.initView(options);
			//add listeners

			this.listenTo(this.entities, 'reset', this.addAllEntities);
			this.listenTo(this.paginator, 'needupdate', this.updateApp);
			//this.listenTo(this.paginator, "needupdate", this.updateTest);

			if (options.show_rubrics) {
				this.listenTo(this.rubricator, 'change', this.updateApp);
				this.listenTo(this.rubricator, 'reset', this.addAllRubrics);
			}
			if (options.showOrderChooser){
				this.listenTo(this.orderChooser, 'change:value', this.updateApp);
			}
			//this.addAllEntities();


		}

		, parse: function(resp, options) {

			if(!_.isUndefined(this.entities)&&_.has(resp, 'initialEntities')){
				this.entities.reset(resp.initialEntities);
			}
			if(!_.isUndefined(this.orderChooser)&&_.has(resp, 'orderChooser')){
				this.orderChooser.set(resp.orderChooser);
			}
			if(!_.isUndefined(this.paginator)&&_.has(resp, 'paginator')){

				var pag = _.clone(resp.paginator)
				this.paginator.set(pag);
			}
			 if(!_.isUndefined(this.rubricator)&&_.has(resp, 'rubrics')){
				this.rubricator.reset(resp.rubrics);
			}

			return _.omit(resp, 'initialEntities', 'orderChooser', 'paginator', 'rubrics');
		}

		, initView: function(options){

			this.addAllEntities();
			if (options.show_rubrics) {
				this.addAllRubrics();
			}
			this.addOrderChooser();
			this.addStat();
			this.addPaginator();
			this.addDateRange();
		}

		, addDateRange: function () {

			if (this.get('show_date_range')){

				 var view = new this.DateRangeView({
					model: this.dateRange ,
					app_name: this.app_name,
				 });

				this.$dateRangeSelector.append(view.render().el);
				// render date range widget
				view.initWidget();
			}
		}

		,  eddEntity: function (entity) {

			var view = new this.EntityView({
				model: entity ,
				className: this.get('itemTempletePrefix')+'-item',
				app_name: this.app_name,
				//template: 'entity-'+ entity.get("type") +'-template',
				templatePrefix: this.get('itemTempletePrefix')});

			this.$entitiesContainer.append(view.render().el);
		}

		, addAllEntities: function () {

			this.$entitiesContainer.html('');
			this.entities.each(this.eddEntity, this);
		}


		,  addRubric: function (rubric) {

			var view = new this.RubricView({model: rubric, app_name: this.app_name });
			if (rubric.get("is_top_level")){
				this.$filterSelector.append(view.render().el);
			}else{
				var $parentSelector = $('#fl-rubric-' + rubric.get("parent_id"));
				if(!_.isUndefined($parentSelector)){
					$parentSelector.append(view.render().el);
				}
			}
		}

		, addAllRubrics: function () {

			if ((!_.isUndefined(this.rubricator))&&(!_.isUndefined(this.$filterSelector))){
				this.$filterSelector.html('');
				this.rubricator.each(this.addRubric, this);
			}

		}


		, addOrderChooser: function () {

			if(this.get('showOrderChooser')){
				var view = new this.OrderChooseView({
					model: this.orderChooser,
					app_name: this.app_name,
				});
				this.$entitisOrderSelector.html(view.render().el);
			}

		}
		, addStat: function () {

			var view = new this.StatView({
					model: this.paginator,
					app_name: this.app_name,
				});

			this.$statSelector.html(view.render().el);

		}
		, addPaginator: function () {

			var view = new this.PaginatorView({
					model: this.paginator,
					app_name: this.app_name,
				});

			this.$paginatorSelector.html(view.render().el)
		}

		, getFilterParams: function(){

			var filter_objects = this.stateFilters
				, queryAttributes = {}

			_.each(filter_objects, function (object) {
				_.extend(queryAttributes, _.result(object, "server_api"))
			})
			var ctm = Math.round(new Date().getTime())
			return _.extend(queryAttributes, {'format': 'json', 'ctm': ctm})

		}

		, updateApp: function (attrs) {

			var params = this.getFilterParams();

			this.fetch({ traditional: true, data: params});


		}

	})

})();