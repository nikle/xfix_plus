/**
 * Created by maydakov on 15.02.2017.
 */
var SPAPP = SPAPP || {};

(function () {
	'use strict';

	// Todo Model
	// ----------

	// Our basic **Todo** model has `title`, `order`, and `completed` attributes.
	SPAPP.RubricModel = Backbone.Model.extend({
		// Default attributes for the entity
		// and ensure that each todo created has `title` and `completed` keys.
		defaults: {
			title: '',
			slug: '',
			parent_id: null,
			is_selected: false,
			is_top_level: false
		}
		, get_absolute_url: function () {

			return '#'
		}
		, toJSON: function (options) {

			var attrs = Backbone.Model.prototype.toJSON.call(this, options)
			return _.extend(attrs, {'absolute_url': this.get_absolute_url()})
		},

		toggle: function () {

			this.set({
				is_selected: !this.get('is_selected')
			});
		}
	});
})();