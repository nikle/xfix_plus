/**
 * Created by maydakov on 15.02.2017.
 */
var SPAPP = SPAPP || {};
(function () {
	'use strict';

	SPAPP.PlateEntitiesAPP = SPAPP.EntitiesAPP.extend({

		 initialize: function (options) {

			 if (options && options.spitCount){
				 this.spitCount = options.spitCount
			 }else {
				 this.spitCount = 4
			 }
			 if (options && options.additionalClass){
				 this.additionalClass = options.additionalClass
			 }else {
				 this.additionalClass = 'col-xs-12 col-sm-12 col-md-6 col-lg-3'
			 }
			 this.spitClass = 'split-row'

			SPAPP.EntitiesAPP.prototype.initialize.call(this, options);

		 }


		,  eddEntity: function (entity) {

			var view = new this.EntityView({
				tagName:  'a',
				model: entity ,
				attributes: {'href': entity.get_absolute_url()},
				className: this.get('itemTempletePrefix')+'-item item '+ this.additionalClass,
				app_name: this.app_name,
				templatePrefix: this.get('itemTempletePrefix')});

			var $spliter = this.$entitiesContainer.find('.' + this.spitClass).last();

			if($spliter.length == 0){

				$spliter = this.$entitiesContainer.append('<div class="row ' + this.spitClass + '"></div>').find('.' + this.spitClass).last();

			}else{

				if ($spliter.children().length >= this.spitCount){
					$spliter = this.$entitiesContainer.append('<div class="row ' + this.spitClass + '"></div>').find('.' + this.spitClass).last();

				}
			}
			$spliter.append(view.render().el);

			return view

		}
		, addLinkButton: function () {

			this.showAllButton = new this.EntityModel({title: ' Все ' + this.app_title, relative_url: this.url()});

			if (!_.isUndefined(this.showAllButton)){
				var view = this.eddEntity(this.showAllButton);
				view.$el.find('.category-title').prepend('<i class="fa fa-chevron-right" aria-hidden="true"></i>');
				//<i class="fa fa-chevron-right" aria-hidden="true"></i>
				view.$el.addClass("last");
			}

		}

		, addAllEntities: function () {

			this.$entitiesContainer.html('');
			this.entities.each(this.eddEntity, this);
			this.addLinkButton();
		}

	});

})();