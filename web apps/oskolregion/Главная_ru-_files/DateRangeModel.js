/**
 * Created by maydakov on 15.02.2017.
 */
var SPAPP = SPAPP || {};

(function () {
	'use strict';

	SPAPP.DateRangeModel = Backbone.Model.extend({
		// Default attributes for the entity
		// and ensure that each todo created has `title` and `completed` keys.
		defaults: {
			name: 'date',
			title: '',
			start_date: null,
			end_date: null,
		}
		, get_absolute_url: function () {

			return '#'
		}
		, toJSON: function (options) {

			var attrs = Backbone.Model.prototype.toJSON.call(this, options);
			return _.extend(attrs, {'id': this.cid})
		},

		server_api: function () {
			var res = {};
			if(!_.isNull(this.get('start_date'))){
				var key = this.get('name')+'0';
				res[key]= this.get('start_date');
			}
			if(!_.isNull(this.get('end_date'))){
				var key = this.get('name')+'1';
				res[key] = this.get('end_date');
			}
			return res

		}
	});
})();