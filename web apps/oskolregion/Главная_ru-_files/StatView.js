/**
 * Created by maydakov on 15.02.2017.
 */
var SPAPP = SPAPP || {};

(function ($) {

	'use strict';

	SPAPP.StatView = Backbone.View.extend({

		tagName:  'small'
		//,	className: 'col-sm-3'

		,   template: 'entity-stat-template'

        // Cache the template function for a single item.
    	,   initializeTemplate: function() {
            this.template = _.template($('#' + this.app_name + '-' + this.template).html());
        }

		, initialize: function (options) {

			if (options.template){
				this.template = options.template;
			}
			this.app_name = options.app_name;
			this.initializeTemplate();
			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'destroy', this.remove);
			//this.listenTo(this.model, 'visible', this.toggleVisible);
		}

		// Re-render the titles of the todo item.
		, render: function () {

			if (this.model.changed.id !== undefined) {
				return;
			}

			this.$el.html(this.template(this.model.toJSON()));

			return this;
		}

		// Remove the item, destroy the model from *localStorage* and delete its view.
		, clear: function () {
			this.model.destroy();
		}
	});
})(jQuery);
