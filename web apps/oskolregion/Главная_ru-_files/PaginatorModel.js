/**
 * Created by maydakov on 15.02.2017.
 */
var SPAPP = SPAPP || {};

(function () {
	'use strict';

	// Order Chooser Model
	// ----------

	SPAPP.PaginatorModel = Backbone.Model.extend({
		defaults: {
			total_count: 0,
            start_index: 1,
            end_index: 12,
            offset: 0,
            limit: 12,
            has_other_pages: false,
			num_pages: 1,
			current_page_number: 1,
			slice: 9,
			orpans: 1
		}
		, setOffset: function (offset) {
			//TODO: validate

			if ((offset > -1)&&(offset < this.get('num_pages')*this.get('limit'))){
				this.set("offset", offset, {silent: true});
				this.trigger('needupdate');
			}

		}
		// Toggle the `completed` state of this todo item.
		,	getPrevious: function(){
			var res = {offset: 0, enabled: false}
			if(this.get('has_other_pages')){
				if(this.get('current_page_number')>1){
					res = {offset: this.get('offset')-this.get('limit'), enabled: true}
				}
			}
			return res
		}
		,	getNext: function(){
			var res = {offset: 0, enabled: false}
			if(this.get('has_other_pages')){
				if(this.get('current_page_number') < this.get('num_pages')){
					res = {offset: this.get('offset')+this.get('limit'), enabled: true}
				}
			}
			return res
		}
		,	getPages: function () {
			var res = [];

			if(this.get('has_other_pages')){
				var slice = this.get('slice'),
					orpans = this.get('orpans'),
					limit = this.get('limit'),
					num_pages = this.get('num_pages'),
					current_page_number = this.get('current_page_number');

				if (num_pages > slice){
					// left buttons
					if(current_page_number > slice-orpans-1){

						// left orphans
						for(var i=0; i< orpans; i++){
							var current = (i+1==current_page_number) ? true : false;
							res.push({page_number: i+1, active: current, offset: limit*i, enabled: true})
						}
						res.push({page_number: '...', active: true, offset: 0, enabled: false});

						if (current_page_number + orpans + 1 > num_pages){
							for(var i=num_pages-slice+orpans; i< num_pages; i++){
							var current = (i+1==current_page_number) ? true : false;
							res.push({page_number: i+1, active: current, offset: limit*i, enabled: true})
							}
						}else{
							//center
							var cnt = Math.floor((slice-2*orpans)/2);
							for(var i=current_page_number-cnt; i< current_page_number+cnt-1; i++){
								var current = (i+1==current_page_number) ? true : false;
								res.push({page_number: i+1, active: current, offset: limit*i, enabled: true})
							}
							res.push({page_number: '...', active: true, offset: 0, enabled: false});
							//right
							for(var i=num_pages-orpans; i< num_pages; i++){
							var current = (i+1==current_page_number) ? true : false;
							res.push({page_number: i+1, active: current, offset: limit*i, enabled: true})
							}
						}


					}else{
						// left pages
						for(var i=0; i< slice-orpans; i++){
							var current = (i+1==current_page_number) ? true : false;
							res.push({page_number: i+1, active: current, offset: limit*i, enabled: true})
						}
						res.push({page_number: '...', active: true, offset: 0, enabled: false});
						// right orphans
						for(var i=num_pages-orpans; i< num_pages; i++){
							var current = (i+1==current_page_number) ? true : false;
							res.push({page_number: i+1, active: current, offset: limit*i, enabled: true})
						}
					}




				}else{

					for (var i = 0; i <num_pages; i++){
						var current = (i+1==current_page_number) ? true : false;
						res.push({page_number: i+1, active: current, offset: limit*i, enabled: true})
					}
				}

			}
			return res
		}
		,   server_api: function() {

			return { 'offset' : this.get('offset'), 'limit': this.get('limit')}

            }


	});
})();