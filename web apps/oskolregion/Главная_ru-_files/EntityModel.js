/**
 * Created by maydakov on 15.02.2017.
 */
var SPAPP = SPAPP || {};

(function () {
	'use strict';

	// Todo Model
	// ----------

	// Our basic **Todo** model has `title`, `order`, and `completed` attributes.
	SPAPP.EntityModel = Backbone.Model.extend({
		// Default attributes for the entity
		// and ensure that each todo created has `title` and `completed` keys.
		defaults: {
			title: '',
			img: null,
			img_block: null,
			completed: false,
			relative_url: '#',
			external_url: null,
			has_content: false,
			is_file: false,
			article: null
		}

		, is_external: function () {
			if ((!_.isNull(this.get('external_url')))&&(!this.get('has_content'))){
				return true
			}
			return false
		}
		, get_absolute_url: function () {

			if (this.is_external()){
				return this.get('external_url')
			}else if ((!_.isUndefined(this.collection))&&(!this.get('is_file'))){
				var res = this.collection.url + this.get('relative_url')
				return res.replace('//', '/')
			}
			return this.get('relative_url')
		}
		, toJSON: function (options) {

			var attrs = Backbone.Model.prototype.toJSON.call(this, options)
			return _.extend(attrs, {'absolute_url': this.get_absolute_url(), 'is_external': this.is_external()})
		},
		// Toggle the `completed` state of this todo item.
		toggle: function () {
			this.save({
				completed: !this.get('completed')
			});
		}
	});
})();