/**
 * Created by maydakov on 15.02.2017.
 */
var SPAPP = SPAPP || {};

(function ($) {
	'use strict';

	// Todo Entity View
	// --------------

	// The DOM element for an entity item...
	SPAPP.OrderChooserView = Backbone.View.extend({

		tagName:  'div'
		,	className: 'dropdown'

		,   template: 'order-chooser-template'

        // Cache the template function for a single item.
    	,   initializeTemplate: function() {
            this.template = _.template($('#' + this.app_name + '-' + this.template).html());
        }


		// The DOM events specific to an item.
		,	events: {
			'click .order-choice': 'setOrder',
			//'dblclick label': 'edit',
			//'click .destroy': 'clear',
			//'keypress .edit': 'updateOnEnter',
			//'keydown .edit': 'revertOnEscape',
			//'blur .edit': 'close'
		},

		// The TodoView listens for changes to its model, re-rendering. Since
		// there's a one-to-one correspondence between a **Todo** and a
		// **TodoView** in this app, we set a direct reference on the model for
		// convenience.
		initialize: function (options) {

			if (options.template){
				this.template = options.template;
			}
			this.app_name = options.app_name;
			this.initializeTemplate();
			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'destroy', this.remove);
			//this.listenTo(this.model, 'visible', this.toggleVisible);
		},

		// Re-render the titles of the todo item.
		render: function () {

			if (this.model.changed.id !== undefined) {
				return;
			}

			var attrs = _.extend({'id': this.model.cid}, this.model.toJSON());


			this.$el.html(this.template(attrs));
			//this.$el.toggleClass('completed', this.model.get('completed'));
			//this.toggleVisible();
			//this.$input = this.$('.edit');
			return this;
		},
		setOrder: function (event) {

			var order_value = event.target.dataset.order;

			this.model.setOrder(order_value);
			event.preventDefault();

		},


		// Remove the item, destroy the model from *localStorage* and delete its view.
		clear: function () {
			this.model.destroy();
		}
	});
})(jQuery);
