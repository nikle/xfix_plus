/**
 * Created by maydakov on 15.02.2017.
 */
var SPAPP = SPAPP || {};

(function ($) {

	'use strict';

	SPAPP.DateRangeView = Backbone.View.extend({

		   template: 'daterange-template'

        // Cache the template function for a single item.
    	,   initializeTemplate: function() {
            this.template = _.template($('#' + this.app_name + '-' + this.template).html());
        }
        ,events: {
			'click .show-calendar': 'openCalendar',
			'click .clear-calendar': 'clearCalendar',
		}
		, initialize: function (options) {

			if (options.template){
				this.template = options.template;
			}
			this.app_name = options.app_name;

			this.initializeTemplate();

		}

		// Re-render the titles of the todo item.
		, render: function () {

			if (this.model.changed.id !== undefined) {
				return;
			}

			this.$el.html(this.template(this.model.toJSON()));

			return this;
		}
		, openCalendar: function (event) {
			//if this.widget

			if(!_.isUndefined(this.widget)){
				event.stopPropagation();
				this.widget.data('dateRangePicker').open();
			}
		}
		, clearCalendar: function (event) {
			//if this.widget

			if(!_.isUndefined(this.widget)){

				this.widget.data('dateRangePicker').clear();
				this.model.set({'start_date': null, 'end_date': null,});
			}
			event.preventDefault();
		}
		, initWidget: function () {

			var that = this,
				config = {
				autoClose: true,
				format: 'DD-MM-YYYY',
				separator: ' - ',
				setValue: function(s, start, end) {
					if(!$(this).attr('readonly') && !$(this).is(':disabled')){


						var date_dict = {};

						if(start && moment(start, 'DD-MM-YYYY').isValid()){
							date_dict.start_date =  start;
						}
						if(end && moment(end, 'DD-MM-YYYY').isValid()){
							date_dict.end_date =  end;
						}

						if (!_.isEmpty(date_dict)){
							that.model.set(date_dict);
						}
						if (s != $(this).val()){

							$(this).val(s);
						}


					}
				},

				};

			//if (!_.isNull(this.model.get('start_date'))){
			//	config.
			//}
			this.widget = this.$el.find(':input').dateRangePicker(config).bind('datepicker-change',function(event,obj) {
					/* This event will be triggered when second date is selected */

					var start_date = moment(obj.date1).format('DD-MM-YYYY')
						, end_date = moment(obj.date2).format('DD-MM-YYYY');

				});


			if(!_.isNull(this.model.get("start_date"))){
				if(!_.isNull(this.model.get("end_date"))){
					this.widget.data('dateRangePicker').setDateRange(this.model.get("start_date"), this.model.get("end_date"));
				}

				this.widget.data('dateRangePicker').setStart(this.model.get("start_date"));

			}else if(!_.isNull(this.model.get("end_date"))){

					this.widget.data('dateRangePicker').setEnd(this.model.get("end_date"));
			}

		}
		, remove: function() {
			  this.widget.data('dateRangePicker').destroy();
			  this._removeElement();
			  this.stopListening();
			  return this;
			}
		// Remove the item, destroy the model from *localStorage* and delete its view.
		, clear: function () {
			this.model.destroy();
		}
	});
})(jQuery);
