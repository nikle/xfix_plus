/**
 * Created by maydakov on 15.02.2017.
 */
var SPAPP = SPAPP || {};

(function ($) {
	'use strict';

	// Todo Entity View
	// --------------

	// The DOM element for rubric item...
	SPAPP.RubricView = Backbone.View.extend({

		tagName:  'li'
		,   template: 'rubric-template'
        // Cache the template function for a single item.
    	,   initializeTemplate: function(prefix) {

			var $temp =  $('#' + this.app_name + '-' + this.template);

            this.template = _.template($temp.html());

        }

		// The DOM events specific to an item.
		,	events: {
			'click .fl-checkbox': 'checkEvent',
		},

		initialize: function (options) {

			//this.className = this.className + ' ' + options.templatePrefix + '-item';

			if (options.template){
				this.template = options.template;
			}
			this.app_name = options.app_name;
			this.initializeTemplate();

			//this.listenTo(this.model, 'change', this.render);
			//this.listenTo(this.model, 'destroy', this.remove);
		},

		render: function () {

			if (this.model.changed.id !== undefined) {
				return;
			}

			this.$el.html(this.template(this.model.toJSON()));
			return this;
		},
		checkEvent: function (event) {

			this.model.toggle();
			event.stopPropagation();
		},
		// Remove the item, destroy the model from *localStorage* and delete its view.
		clear: function () {
			this.model.destroy();
		}
	});
})(jQuery);
