'use strict';

var host = (function(src) {
  var a = document.createElement('a');
  a.href = src;

  return 'https://' + a.hostname;
})(document.currentScript.src);

var endpoint = host + '/spxl?_=' + new Date().getTime();
var options = {
  screen: {
    detectScreenOrientation: false
  },
  fonts: {
    extendedJsFonts: true
  }
};

function loadFp() {
  var scr = document.createElement('script');
  scr.src = host + '/js/fp.min.js?_=' + new Date().getTime();
  scr.onload = startFp;
  document.head.appendChild(scr);
}

function startFp() {
  if (typeof window.Fingerprint2 === 'function') {
    window.Fingerprint2.get(options, process);
  } else {
    setTimeout(startFp, 100);
  }
}

function parseQueryString() {
  var queryArgs = {};
  window.location.search
    .replace('?', '')
    .split('&')
    .forEach(function(pair) {
      var keyValue = pair.split('=');
      queryArgs[keyValue[0]] = decodeURIComponent(keyValue[1]);
    });

  return queryArgs;
}

function process(components) {
  var queryArgs = parseQueryString();
  var values = components.map(function(component) {
    return component.value;
  });

  var data = {
    id: window.Fingerprint2.x64hash128(values.join('')),
    referer: document.referrer,
    domain: window.location.hostname,
    pathname: window.location.pathname,
    userAgent: navigator.userAgent,
    utm_campaign: queryArgs.utm_campaign || '',
    utm_source: queryArgs.utm_source || '',
    utm_medium: queryArgs.utm_medium || ''
  };

  var http = new XMLHttpRequest
  http.open('POST', endpoint)
  http.setRequestHeader('content-type', 'application/json')
  http.send(JSON.stringify(data))
}

window.onload = loadFp;
