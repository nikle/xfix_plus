﻿function BuildSiteLink(section, title, link) {
    section.append("<li><a target='" + link.Window + "' href='" + link.Url + "'>" + title + "</a></li>");
}

function BuildSocialLink(css, title, social) {
    if( social.Url != "" )
        $(".skin-social").append("<div class='social-item'><a target='" + social.Window + "' href='" + social.Url + "'><span class='social-icon " + css + "'></span><span class='social-title'>" + title + "</span></a></div>");
}

function BuildSocialLinkFA(css, fa, title, social)
{
    if( social.Url != "" )
    {
        $(".skin-social").append("<div class='social-item " + css + " '><a title='" + title + "' target='" + social.Window + "' href='" + social.Url + "'><span class='fa " + fa + " fa-inverse'></span></a></div>");       
    }
}

$(document).ready(function () {
    // site links
    BuildSocialLinkFA("social-icon-fb","fa-facebook", "Facebook", skinvars.SocialLinks.Facebook);
    BuildSocialLinkFA("social-icon-twitter", "fa-twitter", "Twitter", skinvars.SocialLinks.Twitter);
    BuildSocialLinkFA("social-icon-youtube", "fa-youtube", "YouTube", skinvars.SocialLinks.YouTube);
    BuildSocialLinkFA("social-icon-googleplus", "fa-google-plus", "Google+", skinvars.SocialLinks.GooglePlus);
    BuildSocialLinkFA("social-icon-instagram", "fa-instagram", "Instagram", skinvars.SocialLinks.Instagram);
    BuildSocialLinkFA("social-icon-flickr", "fa-flickr", "Flickr", skinvars.SocialLinks.Flickr);
    BuildSocialLinkFA("social-icon-blog", "fa-comments", "Blog", skinvars.SocialLinks.Blog);
    BuildSocialLinkFA("social-icon-email", "fa-envelope", "Email", skinvars.SocialLinks.Email);
    BuildSocialLinkFA("social-icon-rss", "fa-rss", "RSS", skinvars.SocialLinks.RSS);
    BuildSocialLinkFA("social-icon-widget", "fa-cog", "Widgets", { Url: "", Window: "" });   // fix  
});