(function() {
  var defaults = {
      0: {
        src: 'example-thumbnail.png'
      }
    },
    extend = function() {
      var args, target, i, object, property;
      args = Array.prototype.slice.call(arguments);
      target = args.shift() || {};
      for (i in args) {
        object = args[i];
        for (property in object) {
          if (object.hasOwnProperty(property)) {
            if (typeof object[property] === 'object') {
              target[property] = extend(target[property], object[property]);
            } else {
              target[property] = object[property];
            }
          }
        }
      }
      return target;
    },
    getComputedStyle = function(el, pseudo) {
      return function(prop) {
        if (window.getComputedStyle) {
          return window.getComputedStyle(el, pseudo)[prop];
        } else {
          return el.currentStyle[prop];
        }
      };
    },
    offsetParent = function(el) {
      if (el.nodeName !== 'HTML' && getComputedStyle(el)('position') === 'static') {
        return offsetParent(el.offsetParent);
      }
      return el;
    },
    getVisibleWidth = function(el, width) {
      var clip;

      if (width) {
        return parseFloat(width);
      }

      clip = getComputedStyle(el)('clip');
      if (clip !== 'auto' && clip !== 'inherit') {
        clip = clip.split(/(?:\(|\))/)[1].split(/(?:,| )/);
        if (clip.length === 4) {
          return (parseFloat(clip[1]) - parseFloat(clip[3]));
        }
      }
      return 0;
    },
    getScrollOffset = function() {
      if (window.pageXOffset) {
        return {
          x: window.pageXOffset,
          y: window.pageYOffset
        };
      }
      return {
        x: document.documentElement.scrollLeft,
        y: document.documentElement.scrollTop
      };
    };

  /**
   * register the thubmnails plugin
   */
  videojs.plugin('thumbnails', function(options) {
    var div, settings, img, player, progressControl, duration, moveListener, moveCancel, bifFrameCount, bifFrameLength, bifImageIndex, currentActiveTime;
    settings = extend({}, defaults, options);
    player = this;

    (function() {
      var progressControl, addFakeActive, removeFakeActive;
      // Android doesn't support :active and :hover on non-anchor and non-button elements
      // so, we need to fake the :active selector for thumbnails to show up.
      if (navigator.userAgent.toLowerCase().indexOf("android") !== -1) {
        progressControl = player.controlBar.progressControl;

        addFakeActive = function() {
          progressControl.addClass('fake-active');
        };
        removeFakeActive = function() {
          progressControl.removeClass('fake-active');
        };

        progressControl.on('touchstart', addFakeActive);
        progressControl.on('touchend', removeFakeActive);
        progressControl.on('touchcancel', removeFakeActive);
      }
    })();

    // create the thumbnail
    div = document.createElement('div');
    div.className = 'vjs-thumbnail-holder';
    img = document.createElement('img');
    img.className = 'vjs-thumbnail';
    div.appendChild(img);
    if(settings.bifSrc) {
    	if (typeof window.FileReader === "undefined") {
    		return;
    	}
    	extend(img.style, settings.style);
    	
    	var setupBifImageIndex = function(data) {
    		var dv = new DataView(data);

    		bifImageIndex = {};
    		var startByte;
    		var key;
    		var offset
    		for(var i = 0; i < bifFrameCount; i++) {
    			key = i * bifFrameLength;
        		offset = (i * 8) + 4;
        		startByte = dv.getUint32(offset, true);
        		if(key > 0) {
        			bifImageIndex[key - bifFrameLength].end = startByte - 1;
        		}
        		bifImageIndex[key] = {"start": startByte, "loadStatus": 0}
        	}
    		offset = (bifFrameCount * 8) + 4;
    		bifImageIndex[key].end = dv.getUint32(offset, true);
    	}
    	
    	var onFrameCountSuccess = function( data ) {
        	var dv = new DataView(data);
        	bifFrameCount = dv.getUint32(0, true);
        	bifFrameLength = dv.getUint32(4, true) / 1000;
        	
        	var endByteRange = (bifFrameCount * 8) + 71;
    		$.ajax({
                url: settings.bifSrc,
                dataType: "binary",
                responseType: "arraybuffer",
                processData: false,
                headers: {Range: "bytes=64-"+endByteRange},
                success: setupBifImageIndex
              });
        }
    	
    	$.ajax({
            url: settings.bifSrc,
            dataType: "binary",
            responseType: "arraybuffer",
            processData: false,
            headers: {Range: "bytes=12-19"},
            success: onFrameCountSuccess
          });
    	
    } else {
    	img.src = settings['0'].src;
        extend(img.style, settings['0'].style);

        // center the thumbnail over the cursor if an offset wasn't provided
        if (!img.style.left && !img.style.right) {
          img.onload = function() {
            img.style.left = -(img.naturalWidth / 2) + 'px';
          };
        }
    }
    

    // keep track of the duration to calculate correct thumbnail to display
    duration = player.duration();
    
    // when the container is MP4
    player.on('durationchange', function(event) {
      duration = player.duration();
    });

    // when the container is HLS
    player.on('loadedmetadata', function(event) {
      duration = player.duration();
    });

    // add the thumbnail to the player
    progressControl = player.controlBar.progressControl;
    progressControl.el().appendChild(div);
    
    var convertBlobToImage = function(blob) {
    	var reader = new window.FileReader();
    	reader.onloadend = function() {
    		img.src = "data:image/jpeg;base64" + reader.result;
		}
    	reader.readAsDataURL(blob);
    }
    
    var getImageAtTime = function(time) {
    	var image = bifImageIndex[time];
    	if(typeof image === "undefined" || image.loadStatus) {
    		if(image.loadStatus == 2) {
    			convertBlobToImage(image.blob);
        	}
  	        return;
  	    }
    	
    	bifImageIndex[time].loadStatus = 1;
    	
    	var onGetImageAtTimeSuccess = function(blob) {
    		bifImageIndex[time].blob = blob;
    		bifImageIndex[time].loadStatus = 2;
        	if(time != currentActiveTime) {
        		return;
        	}
        	convertBlobToImage(blob);
        }
    	
    	var startByteRange = image.start;
    	var endByteRange = image.end;
    	$.ajax({
            url: settings.bifSrc,
            dataType: "binary",
            processData: false,
            headers: {Range: "bytes=" + startByteRange + "-" + endByteRange},
            success: onGetImageAtTimeSuccess
          });
    }
    
    moveListener = function(event) {
	      var mouseTime, time, active, left, setting, pageX, right, width, halfWidth, pageXOffset, clientRect;
	      active = 0;
	      pageXOffset = getScrollOffset().x;
	      clientRect = offsetParent(progressControl.el()).getBoundingClientRect();
	      right = (clientRect.width || clientRect.right) + pageXOffset;
	
	      pageX = event.pageX;
	      if (event.changedTouches) {
	        pageX = event.changedTouches[0].pageX;
	      }
	
	      // find the page offset of the mouse
	      left = pageX || (event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft);
	      // subtract the page offset of the positioned offset parent
	      left -= offsetParent(progressControl.el()).getBoundingClientRect().left + pageXOffset;
	      
	      var mouseTimeHMS  = $(".vjs-mouse-display").attr("data-current-time").split(":");
	      var mouseTime = 0;
	      for(var i = mouseTimeHMS.length - 1; i >= 0; i--)
	          mouseTime += parseInt((i == mouseTimeHMS.length-1) ? (mouseTimeHMS[i]) : ((i == mouseTimeHMS.length-2) ? (mouseTimeHMS[i] * 60) : ((i == mouseTimeHMS.length-3) ? (mouseTimeHMS[i] * 60 * 60) : (0))));

	      if(settings.bifSrc) {
	    	  if (typeof bifImageIndex === "undefined") {
	    	      return;
	    	  }
	    	  
	    	  for (time in bifImageIndex) {
			      if (mouseTime >= time) {
			    	  active = Math.max(active, time);
			      }
    	      }
	    	  currentActiveTime = active;
	    	  getImageAtTime(active);
	    	  
	    	  width = settings.style.width;
	      } else {
	    	  for (time in settings) {
    	        if (mouseTime > time) {
    	          active = Math.max(active, time);
    	        }
    	      }
	    	  
    	      setting = settings[active];
    	      if (setting.src && img.src != setting.src) {
    	        img.src = setting.src;
    	      }
    	      if (setting.style && img.style != setting.style) {
    	        extend(img.style, setting.style);
    	      }

    	      width = getVisibleWidth(img, setting.width || settings[0].width);
	      }
	      halfWidth = width / 2;
	      
	      if(left - halfWidth < 0) {
	    	  left = 0;
	      } else if(left + halfWidth > right) {
	    	  left = right - halfWidth * 2;
	      } else {
	    	  left -= halfWidth;
	      }

	      div.style.left = left + 'px';
    };

    // update the thumbnail while hovering
    progressControl.on('mousemove', moveListener);
    progressControl.on('touchmove', moveListener);

    moveCancel = function(event) {
      div.style.left = '-1000px';
    };

    // move the placeholder out of the way when not hovering
    progressControl.on('mouseout', moveCancel);
    progressControl.on('touchcancel', moveCancel);
    progressControl.on('touchend', moveCancel);
    player.on('userinactive', moveCancel);
  });
})();
