
// jQuery SWFObject v1.1.1 MIT/GPL @jon_neal
// http://jquery.thewikies.com/swfobject

(function($, flash, Plugin) {
	var OBJECT = 'object',
		ENCODE = true;

	function _compareArrayIntegers(a, b) {
		var x = (a[0] || 0) - (b[0] || 0);

		return x > 0 || (
			!x &&
			a.length > 0 &&
			_compareArrayIntegers(a.slice(1), b.slice(1))
		);
	}

	function _objectToArguments(obj) {
		if (typeof obj != OBJECT) {
			return obj;
		}

		var arr = [],
			str = '';

		for (var i in obj) {
			if (typeof obj[i] == OBJECT) {
				str = _objectToArguments(obj[i]);
			}
			else {
				str = [i, (ENCODE) ? encodeURI(obj[i]) : obj[i]].join('=');
			}

			arr.push(str);
		}

		return arr.join('&');
	}

	function _objectFromObject(obj) {
		var arr = [];

		for (var i in obj) {
			if (obj[i]) {
				arr.push([i, '="', obj[i], '"'].join(''));
			}
		}

		return arr.join(' ');
	}

	function _paramsFromObject(obj) {
		var arr = [];

		for (var i in obj) {
			arr.push([
				'<param name="', i,
				'" value="', _objectToArguments(obj[i]), '" />'
			].join(''));
		}

		return arr.join('');
	}

	try {
		var flashVersion = Plugin.description || (function () {
			return (
				new Plugin('ShockwaveFlash.ShockwaveFlash')
			).GetVariable('$version');
		}())
	}
	catch (e) {
		flashVersion = 'Unavailable';
	}

	var flashVersionMatchVersionNumbers = flashVersion.match(/\d+/g) || [0];

	$[flash] = {
		available: flashVersionMatchVersionNumbers[0] > 0,

		activeX: Plugin && !Plugin.name,

		version: {
			original: flashVersion,
			array: flashVersionMatchVersionNumbers,
			string: flashVersionMatchVersionNumbers.join('.'),
			major: parseInt(flashVersionMatchVersionNumbers[0], 10) || 0,
			minor: parseInt(flashVersionMatchVersionNumbers[1], 10) || 0,
			release: parseInt(flashVersionMatchVersionNumbers[2], 10) || 0
		},

		hasVersion: function (version) {
			var versionArray = (/string|number/.test(typeof version))
				? version.toString().split('.')
				: (/object/.test(typeof version))
					? [version.major, version.minor]
					: version || [0, 0];

			return _compareArrayIntegers(
				flashVersionMatchVersionNumbers,
				versionArray
			);
		},

		encodeParams: true,

		expressInstall: 'expressInstall.swf',
		expressInstallIsActive: false,

		create: function (obj) {
			var instance = this;

			if (
				!obj.swf ||
				instance.expressInstallIsActive ||
				(!instance.available && !obj.hasVersionFail)
			) {
				return false;
			}

			if (!instance.hasVersion(obj.hasVersion || 1)) {
				instance.expressInstallIsActive = true;

				if (typeof obj.hasVersionFail == 'function') {
					if (!obj.hasVersionFail.apply(obj)) {
						return false;
					}
				}

				obj = {
					swf: obj.expressInstall || instance.expressInstall,
					height: 137,
					width: 214,
					flashvars: {
						MMredirectURL: location.href,
						MMplayerType: (instance.activeX)
							? 'ActiveX' : 'PlugIn',
						MMdoctitle: document.title.slice(0, 47) +
							' - Flash Player Installation'
					}
				};
			}

			attrs = {
				data: obj.swf,
				type: 'application/x-shockwave-flash',
				id: obj.id || 'flash_' + Math.floor(Math.random() * 999999999),
				width: obj.width || 320,
				height: obj.height || 180,
				style: obj.style || ''
			};

			ENCODE = typeof obj.useEncode !== 'undefined' ? obj.useEncode : instance.encodeParams;

			obj.movie = obj.swf;
			obj.wmode = obj.wmode || 'opaque';

			delete obj.fallback;
			delete obj.hasVersion;
			delete obj.hasVersionFail;
			delete obj.height;
			delete obj.id;
			delete obj.swf;
			delete obj.useEncode;
			delete obj.width;

			var flashContainer = document.createElement('div');

			flashContainer.innerHTML = [
				'<object ', _objectFromObject(attrs), '>',
				_paramsFromObject(obj),
				'</object>'
			].join('');

			return flashContainer.firstChild;
		}
	};

	$.fn[flash] = function (options) {
		var $this = this.find(OBJECT).andSelf().filter(OBJECT);

		if (/string|object/.test(typeof options)) {
			this.each(
				function () {
					var $this = $(this),
						flashObject;

					options = (typeof options == OBJECT) ? options : {
						swf: options
					};

					options.fallback = this;

					flashObject = $[flash].create(options);

					if (flashObject) {
						$this.children().remove();

						$this.html(flashObject);
					}
				}
			);
		}

		if (typeof options == 'function') {
			$this.each(
				function () {
					var instance = this,
					jsInteractionTimeoutMs = 'jsInteractionTimeoutMs';

					instance[jsInteractionTimeoutMs] =
						instance[jsInteractionTimeoutMs] || 0;

					if (instance[jsInteractionTimeoutMs] < 660) {
						if (instance.clientWidth || instance.clientHeight) {
							options.call(instance);
						}
						else {
							setTimeout(
								function () {
									$(instance)[flash](options);
								},
								instance[jsInteractionTimeoutMs] + 66
							);
						}
					}
				}
			);
		}

		return $this;
	};
}(
	jQuery,
	'flash',
	navigator.plugins['Shockwave Flash'] || window.ActiveXObject
));
// JavaScript Document

// this is used for any GLOBAL javascript. In most cases, things should NOT be added here.

function unsetNoJavascript () {

	// remove default state if we have JS:
	$('body').removeClass('noJavascript');

}
// JavaScript Document

// this is used to store all parameters from any jsTemplates

var allParams = new Object();

function getParams ( paramType, defaults, JSONParameters ) {
	
	if (allParams[paramType] == undefined) {
		// param defaults
		//alert(allParams[paramType]);
		allParams[paramType] = new Object();
		
		for (var i in defaults) {allParams[paramType][i]=defaults[i];}
		allParams[paramType].initiated = true;
		
		for (var i in JSONParameters) {allParams[paramType][i]=JSONParameters[i];}
			//console.log(this.param);
	}
	
	return allParams[paramType];
	
}
/*
 * Metadata - jQuery plugin for parsing metadata from elements
 *
 * Copyright (c) 2006 John Resig, Yehuda Katz, J�örn Zaefferer, Paul McLanahan
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 * Revision: $Id: jquery.metadata.js 3640 2007-10-11 18:34:38Z pmclanahan $
 *
 */

/**
 * Sets the type of metadata to use. Metadata is encoded in JSON, and each property
 * in the JSON will become a property of the element itself.
 *
 * There are four supported types of metadata storage:
 *
 *   attr:  Inside an attribute. The name parameter indicates *which* attribute.
 *          
 *   class: Inside the class attribute, wrapped in curly braces: { }
 *   
 *   elem:  Inside a child element (e.g. a script tag). The
 *          name parameter indicates *which* element.
 *   html5: Values are stored in data-* attributes.
 *          
 * The metadata for an element is loaded the first time the element is accessed via jQuery.
 *
 * As a result, you can define the metadata type, use $(expr) to load the metadata into the elements
 * matched by expr, then redefine the metadata type and run another $(expr) for other elements.
 * 
 * @name $.metadata.setType
 *
 * @example <p id="one" class="some_class {item_id: 1, item_label: 'Label'}">This is a p</p>
 * @before $.metadata.setType("class")
 * @after $("#one").metadata().item_id == 1; $("#one").metadata().item_label == "Label"
 * @desc Reads metadata from the class attribute
 * 
 * @example <p id="one" class="some_class" data="{item_id: 1, item_label: 'Label'}">This is a p</p>
 * @before $.metadata.setType("attr", "data")
 * @after $("#one").metadata().item_id == 1; $("#one").metadata().item_label == "Label"
 * @desc Reads metadata from a "data" attribute
 * 
 * @example <p id="one" class="some_class"><script>{item_id: 1, item_label: 'Label'}</script>This is a p</p>
 * @before $.metadata.setType("elem", "script")
 * @after $("#one").metadata().item_id == 1; $("#one").metadata().item_label == "Label"
 * @desc Reads metadata from a nested script element
 * 
 * @example <p id="one" class="some_class" data-item_id="1" data-item_label="Label">This is a p</p>
 * @before $.metadata.setType("html5")
 * @after $("#one").metadata().item_id == 1; $("#one").metadata().item_label == "Label"
 * @desc Reads metadata from a series of data-* attributes
 *
 * @param String type The encoding type
 * @param String name The name of the attribute to be used to get metadata (optional)
 * @cat Plugins/Metadata
 * @descr Sets the type of encoding to be used when loading metadata for the first time
 * @type undefined
 * @see metadata()
 */

(function($) {

$.extend({
  metadata : {
    defaults : {
      type: 'class',
      name: 'metadata',
      cre: /({.*})/,
      single: 'metadata'
    },
    setType: function( type, name ){
      this.defaults.type = type;
      this.defaults.name = name;
    },
    get: function( elem, opts ){
      var settings = $.extend({},this.defaults,opts);
      // check for empty string in single property
      if ( !settings.single.length ) settings.single = 'metadata';
      
      var data = $.data(elem, settings.single);
      // returned cached data if it already exists
      if ( data ) return data;
      
      data = "{}";
      
      var getData = function(data) {
        if(typeof data != "string") return data;
        
        if( data.indexOf('{') < 0 ) {
          data = eval("(" + data + ")");
        }
      }
      
      var getObject = function(data) {
        if(typeof data != "string") return data;
        
        data = eval("(" + data + ")");
        return data;
      }
      
      if ( settings.type == "html5" ) {
        var object = {};
        $( elem.attributes ).each(function() {
          var name = this.nodeName;
          if(name.match(/^data-/)) name = name.replace(/^data-/, '');
          else return true;
          object[name] = getObject(this.nodeValue);
        });
      } else {
        if ( settings.type == "class" ) {
          var m = settings.cre.exec( elem.className );
          if ( m )
            data = m[1];
        } else if ( settings.type == "elem" ) {
          if( !elem.getElementsByTagName ) return;
          var e = elem.getElementsByTagName(settings.name);
          if ( e.length )
            data = $.trim(e[0].innerHTML);
        } else if ( elem.getAttribute != undefined ) {
          var attr = elem.getAttribute( settings.name );
          if ( attr )
            data = attr;
        }
        object = getObject(data.indexOf("{") < 0 ? "{" + data + "}" : data);
      }
      
      $.data( elem, settings.single, object );
      return object;
    }
  }
});

/**
 * Returns the metadata object for the first member of the jQuery object.
 *
 * @name metadata
 * @descr Returns element's metadata object
 * @param Object opts An object contianing settings to override the defaults
 * @type jQuery
 * @cat Plugins/Metadata
 */
$.fn.metadata = function( opts ){
  return $.metadata.get( this[0], opts );
};

})(jQuery);
function madeByMindUnit (ie6) {
	//alert('a');
	// path - /lib/madeByMindUnit/
	// init
	var $madeByMU = $('#madeByMU a');
	var $img = $('img',$madeByMU);
	
	// browser/OS detection
	var is_mac = /macintosh/.test( navigator.userAgent.toLowerCase() );
	var is_chrome = /chrome/.test( navigator.userAgent.toLowerCase() );
	
	if (is_mac)
		$img.attr('src', $img.attr('src').replace(/\/mozilla\//,'/mac/'));
	else if (is_chrome)
		$img.attr('src', $img.attr('src').replace(/\/mozilla\//,'/chrome/'));
	else if ($.browser.msie)
		$img.attr('src', $img.attr('src').replace(/\/mozilla\//,'/ie/'));
			
	if(ie6 && ($.browser.msie && parseFloat($.browser.version.substr(0,3))<=6)) { $img.css("display","none"); }
	
	// image hover
	$madeByMU.hover(function(){
		$img.attr('src', $img.attr('src').replace(/_h.png$/,'.png').replace(/.png$/,'_h.png'));
	},function(){
		$img.attr('src', $img.attr('src').replace(/_h.png$/,'.png'));
	});	
	
	$madeByMU.css("visibility","visible");
	$madeByMU.hide();
	$madeByMU.fadeIn(600);

}
$(document).ready(function () {
		setTimeout ( madeByMindUnit, 1500 );
});
/*!
 Slimbox v2.02 - The ultimate lightweight Lightbox clone for jQuery
 (c) 2007-2009 Christophe Beyls <http://www.digitalia.be>
 Modified by Ian Budden to show video as well as images
 MIT-style license.
 */

(function($)
{

	// Global variables, accessible to Slimbox only
	var win = $(window), options, images, activeImage = -1, activeURL, prevImage, nextImage, compatibleOverlay, middle, centerWidth, centerHeight, ie6 = !window.XMLHttpRequest,
	operaFix = window.opera && (document.compatMode == "CSS1Compat") && ($.browser.version >= 9.3), documentElement = document.documentElement,

	// Preload images
	preload =
	{
	}, preloadPrev = new Image(), preloadNext = new Image(),

	// DOM elements
	overlay, center, image, sizer, prevLink, nextLink, bottomContainer, bottom, caption, number;

	/*
	 Initialization
	 */

	$( function()
	{
		// Append the Slimbox HTML code at the bottom of the document
		$("body").append(
		$([
		overlay = $('<div id="lbOverlay" />')[0],
		center = $('<div id="lbCenter" />')[0],
		bottomContainer = $('<div id="lbBottomContainer" />')[0]
		]).css("display", "none")
		);

		image = $('<div id="lbImage" />').appendTo(center).append(
		sizer = $('<div style="position: relative;" />').append([
		prevLink = $('<a id="lbPrevLink" href="#" />').click(previous)[0],
		nextLink = $('<a id="lbNextLink" href="#" />').click(next)[0]
		])[0]
		)[0];

		bottom = $('<div id="lbBottom" />').appendTo(bottomContainer).append([
		$('<a id="lbCloseLink" href="#" />').add(overlay).click(close)[0],
		caption = $('<div id="lbCaption" />')[0],
		number = $('<div id="lbNumber" />')[0],
		$('<div style="clear: both;" />')[0]
		])[0];
	});
	/*
	API
	*/

	// Open Slimbox with the specified parameters
	$.slimbox = function(_images, startImage, _options)
	{

		//alert(_images);

		options = $.extend(
		{
			loop: false,				// Allows to navigate between first and last images
			overlayOpacity: 0.8,			// 1 is opaque, 0 is completely transparent (change the color in the CSS file)
			overlayFadeDuration: 400,		// Duration of the overlay fade-in and fade-out animations (in milliseconds)
			resizeDuration: 400,			// Duration of each of the box resize animations (in milliseconds)
			resizeEasing: "swing",			// "swing" is jQuery's default easing
			initialWidth: 250,			// Initial width of the box (in pixels)
			initialHeight: 250,			// Initial height of the box (in pixels)
			imageFadeDuration: 400,			// Duration of the image fade-in animation (in milliseconds)
			captionAnimationDuration: 400,		// Duration of the caption animation (in milliseconds)
			counterText: "Image {x} of {y}",	// Translate or change as you wish, or set it to false to disable counter text for image groups
			closeKeys: [27, 88, 67],		// Array of keycodes to close Slimbox, default: Esc (27), 'x' (88), 'c' (67)
			previousKeys: [37, 80],			// Array of keycodes to navigate to the previous image, default: Left arrow (37), 'p' (80)
			nextKeys: [39, 78]			// Array of keycodes to navigate to the next image, default: Right arrow (39), 'n' (78)
		}, _options);

		// The function is called for a single image, with URL and Title as first two arguments
		if (typeof _images == "string")
		{
			_images = [[_images, startImage]];
			startImage = 0;
		}

		middle = win.scrollTop() + ((operaFix ? documentElement.clientHeight : win.height()) / 2);
		centerWidth = options.initialWidth;
		centerHeight = options.initialHeight;
		$(center).css(
		{
			top: Math.max(0, middle - (centerHeight / 2)),
			width: centerWidth,
			height: centerHeight,
			marginLeft: -centerWidth/2
		}).show();
		compatibleOverlay = ie6 || (overlay.currentStyle && (overlay.currentStyle.position != "fixed"));
		if (compatibleOverlay)
			overlay.style.position = "absolute";
		$(overlay).css("opacity", options.overlayOpacity).fadeIn(options.overlayFadeDuration);
		position();
		setup(1);

		images = _images;
		options.loop = options.loop && (images.length > 1);
		return changeImage(startImage);
	};
	/*
	 options:	Optional options object, see jQuery.slimbox()
	 linkMapper:	Optional function taking a link DOM element and an index as arguments and returning an array containing 2 elements:
	 the image URL and the image caption (may contain HTML)
	 linksFilter:	Optional function taking a link DOM element and an index as arguments and returning true if the element is part of
	 the image collection that will be shown on click, false if not. "this" refers to the element that was clicked.
	 This function must always return true when the DOM element argument is "this".
	 */
	$.fn.slimbox = function(_options, linkMapper, linksFilter)
	{
		linkMapper = linkMapper ||
		function(el)
		{
			//IB added class
			return [el.href, el.title];
		};

		linksFilter = linksFilter ||
		function()
		{
			return true;
		};

		var links = this;

		return links.unbind("click").click( function()
		{
			// Build the list of images that will be displayed
			var link = this, startIndex = 0, filteredLinks, i = 0, length;
			filteredLinks = $.grep(links, function(el, i)
			{
				return linksFilter.call(link, el, i);
			});
			// We cannot use jQuery.map() because it flattens the returned array
			for (length = filteredLinks.length; i < length; ++i)
			{
				if (filteredLinks[i] == link)
					startIndex = i;
				filteredLinks[i] = linkMapper(filteredLinks[i], i);
			}

			return $.slimbox(filteredLinks, startIndex, _options);
		});
	};
	/*
	 Internal functions
	 */

	function position()
	{
		var l = win.scrollLeft(), w = operaFix ? documentElement.clientWidth : win.width();
		$([center, bottomContainer]).css("left", l + (w / 2));
		if (compatibleOverlay)
			$(overlay).css(
			{
				left: l,
				top: win.scrollTop(),
				width: w,
				height: win.height()
			});
	}

	function setup(open)
	{
		//alert($("object").html());
		// bug on IE (visibility unknown)
		/*$("object").add(ie6 ? "select" : "embed").each(function(index, el) {
		 if (open) $.data(el, "slimbox", el.style.visibility);
		 el.style.visibility = open ? "hidden" : $.data(el, "slimbox");
		 });*/
		var fn = open ? "bind" : "unbind";
		win[fn]("scroll resize", position);
		$(document)[fn]("keydown", keyDown);
	}

	function keyDown(event)
	{
		var code = event.keyCode, fn = $.inArray;
		// Prevent default keyboard action (like navigating inside the page)
		return (fn(code, options.closeKeys) >= 0) ? close()
		: (fn(code, options.nextKeys) >= 0) ? next()
		: (fn(code, options.previousKeys) >= 0) ? previous()
		: false;
	}

	function previous()
	{
		return changeImage(prevImage);
	}

	function next()
	{
		return changeImage(nextImage);
	}

	function changeImage(imageIndex)
	{
		if (imageIndex >= 0)
		{
			activeImage = imageIndex;
			activeURL = images[activeImage][0];

			//alert(activeURL);

			prevImage = (activeImage || (options.loop ? images.length : 0)) - 1;
			nextImage = ((activeImage + 1) % images.length) || (options.loop ? 0 : -1);

			stop();
			center.className = "lbLoading";

			preload = new Image();

			preload.onload = animateBox;

			// video split
			//alert(activeURL.split('|')[0]);
			preload.src = activeURL.split('|')[0];
		}

		return false;
	}

	function animateBox()
	{

		center.className = "";

		$(image).css(
		{
			backgroundImage: "url(" + activeURL + ")",
			visibility: "hidden",
			display: ""
		});
		//alert('animateBox');
		// remove any flash
		$('#lbFlash',image).remove();
		// if the url also has a video, then play over the image

		// REPLACE WITH .mediaPlayer

		if (activeURL.indexOf('|') != -1)
		{
			//alert('video');
			var urlParts = activeURL.split('|');
			
			if ( urlParts[1].indexOf('http:') != -1) {
				// external embedded video
				var $flashPlayer = $('<div id="lbFlash"></div>');
			
				if ( urlParts[1].indexOf('youtube') != -1) {
					/* YOUTUBE
					http://www.youtube.com/watch?v=7zLUsNU5Hpg&feature=aso
					<iframe class="youtube-player" type="text/html" width="640" height="385" src="http://www.youtube.com/embed/VIDEO_ID" frameborder="0" webkitAllowFullScreen allowFullScreen></iframe>*/
					// get the ID out of the url
					var ytCode = urlParts[1].match(/v=[a-z0-9\-\_]+/gi)[0].replace('v=','');
					$flashPlayer.append('<iframe class="youtube-player" type="text/html" width="'+preload.width+'" height="'+preload.height+'" src="http://www.youtube.com/embed/'+ytCode+'" frameborder="0" webkitAllowFullScreen allowFullScreen></iframe>');	
				
				} else if ( urlParts[1].indexOf('vimeo') != -1) {
					/* VIMEO
					//http://vimeo.com/30312312
					<iframe src="http://player.vimeo.com/video/31005042?title=0&amp;byline=0&amp;portrait=0" width="400" height="225" frameborder="0" webkitAllowFullScreen allowFullScreen></iframe>*/				
					var viCode = urlParts[1].match(/[0-9\-\_]+/gi)[0];
					$flashPlayer.append('<iframe class="vimeo-player" src="http://player.vimeo.com/video/'+viCode+'?title=0&amp;byline=0&amp;portrait=0" width="'+preload.width+'" height="'+preload.height+'" frameborder="0" webkitAllowFullScreen allowFullScreen></iframe>');	
				
				}
				$(image).append($flashPlayer);
			
			} else {
			
				// Generate a unique ID
				var id = Math.floor( Math.random() * 999 );

				var $flashPlayer = $('<div id="lbFlash"></div>');
				var $videoTag = $('<![if !IE]><video width="' +preload.width+ '" height="'
							+  preload.height + '" controls="controls" poster="'
							+  urlParts[0] + '" src="'
							+  urlParts[1]
							+ '" id="popupVideo' + id + '"><source src="' + urlParts[1] + '" type="video/mp4"/></video><![endif]>'
				
							// IE fallback
							+ '<!--[if IE]>'
							+ '<div class="iefall" id="popupVideo' + id + '" width="' + preload.width + '" height="' + preload.height + '" '
							+ 'file="' + urlParts[1] + '" '
							+ 'style="width: ' + preload.width + 'px; height: ' + preload.height + 'px;" '
							+ 'poster="' + urlParts[0] + '" '
							+ '>&nbsp;</div>'
							+ '<![endif]-->');

			//alert( $videoTag.html( ) );

				$flashPlayer.append($videoTag);
				$(image).append($flashPlayer);
				$flashPlayer.mediaPlayer();
			}

		}
		/**/
		$(sizer).width(preload.width);
		//change next prev height to avoid flv play controls
		$([prevLink, nextLink]).height(preload.height-30);
		$(sizer).height(preload.height);

		$(caption).html(images[activeImage][1] || "");
		$(number).html((((images.length > 1) && options.counterText) || "").replace(/{x}/, activeImage + 1).replace(/{y}/, images.length));

		if (prevImage >= 0)
			preloadPrev.src = images[prevImage][0];
		if (nextImage >= 0)
			preloadNext.src = images[nextImage][0];

		centerWidth = image.offsetWidth;
		centerHeight = image.offsetHeight;
		var top = Math.max(0, middle - (centerHeight / 2));
		if (center.offsetHeight != centerHeight)
		{
			$(center).animate(
			{
				height: centerHeight,
				top: top
			}, options.resizeDuration, options.resizeEasing);
		}
		if (center.offsetWidth != centerWidth)
		{
			$(center).animate(
			{
				width: centerWidth,
				marginLeft: -centerWidth/2
			}, options.resizeDuration, options.resizeEasing);
		}
		$(center).queue( function()
		{
			$(bottomContainer).css(
			{
				width: centerWidth,
				top: top + centerHeight,
				marginLeft: -centerWidth/2,
				visibility: "hidden",
				display: ""
			});
			$(image).css(
			{
				display: "none",
				visibility: "",
				opacity: ""
			}).fadeIn(options.imageFadeDuration, animateCaption);
		});
	}

	function animateCaption()
	{
		if (prevImage >= 0)
			$(prevLink).show();
		if (nextImage >= 0)
			$(nextLink).show();
		$(bottom).css("marginTop", -bottom.offsetHeight).animate(
		{
			marginTop: 0
		}, options.captionAnimationDuration);
		bottomContainer.style.visibility = "";
	}

	function stop()
	{
		$player = $(image).find( '.mediaPlayer' );
		if( $player.length )
		{
			// Retrieve the player api
			var player = $player.eq(0).data('api');
			player.stop( );
			
			$player.remove( );
		}
		
		preload.onload = null;
		preload.src = preloadPrev.src = preloadNext.src = activeURL;
		$([center, image, bottom]).stop(true);
		$([prevLink, nextLink, image, bottomContainer]).hide();
	}

	function close()
	{
		if (activeImage >= 0)
		{
			stop();
			activeImage = prevImage = nextImage = -1;
			$(center).hide();
			$(overlay).stop().fadeOut(options.overlayFadeDuration, setup);
			$('#lbFlash').remove();
		}
		//$('#lbOverlay,#lbCenter,#lbBottomContainer').remove();
		return false;
	}

})(jQuery);
$(document).ready( function ()
{

	if (/android|iphone|ipod|series60|symbian|windows ce|blackberry/i.test(navigator.userAgent))
	{
		return;
	}

	var JSONdefaults = new Object(
	{
		"targetElement": "#container ol.gallery li"
	});

	// php generated overrides
	var JSONParameters=[];
	var params = getParams( 'slimbox', JSONdefaults, JSONParameters );

	$(params.targetElement).slimbox(params, function ( item )
	{

		var $dd = $(item);

		// add video to url, if there is one

		var caption= '';

		$.metadata.setType("elem", "script");

		var itemData = $dd.metadata();

		var href = itemData.imagePath;
		if (itemData.videoPath)
			href += '|'+itemData.videoPath;

		var caption = '<h3>'+itemData.imageTitle+'</h3>';
		if (itemData.description.length > 2)
			caption += '<p>'+itemData.description+'</p>';

		if (itemData.download)
		{
			caption += '<p><a class="download" href="'+itemData.download+'" title="Select this link to download this item">Download</a> ('+itemData.format+', '+itemData.fileSize+')</p>';

		}
		// assemble caption out of the meta data of the dd
		//<a href='/downloadImage.php?p=$path' alt='A'>Download</a><span class='fileInfo'>({$format}, {$filesize})</span>

		return [href, caption];

	});
});

function imageMap() {
	
	
	// put all other items back to a low z-index, and fade out any active ones
	var $popup = $(this).next('.popupBox');
	
	initCufon('.moreLink',$popup);
	
	if (jQuery.browser.msie) {
  		ie = parseInt(jQuery.browser.version);
		if(ie == 6 || ie == 7) { $popup.css('top',-50); }
		//alert(parseInt(jQuery.browser.version));
	}
	
	//alert($popup.html());
	if (!$popup.hasClass('active')) {
		$('div.marker').cleanUpPopups();
		$popup.fadeIn().addClass('active');
		$popup.parent().css('zIndex',110);
	}
	
	
	
}

jQuery.fn.cleanUpPopups = function() { return this.each(function(){
	$(this).css('zIndex',100);
	if ( $('div.popupBox',this).hasClass('active') ) $('div.popupBox',this).fadeOutPopup();
});}
jQuery.fn.fadeOutPopup = function() { return this.each(function(){
	$(this).fadeOut().removeClass('active');
});}
// JavaScript Document
function removePopup() {

	$('#popupHolder').fadeOut (500,function() { $(this).remove(); });
	$('#popupMask').fadeOut(500,function() { $(this).remove(); });
	return false;
}			


function initPopup() {
	var $popupHolder = $('<div id="popupHolder"></div>');
	var $popupMask = $('<div id="popupMask"></div>');
	$popupMask.appendTo('body');
	$popupHolder.appendTo('body');
	$('#popupMask').css('opacity','0.5' ); // hack for ie6 opacity
	$('#popupHolder').css('top',($('html').scrollTop()+450) ); // make enlarge is on screen

	$popupMask.click(removePopup);

	$popupMask.fadeIn(makePopupContent( $(this).parent().siblings('.fullText').html() ));
	return false;
}

function makePopupContent(data) {

	$('#popupHolder').append(data).show();
	$('#popupHolder .moreLink').click(removePopup);
	$('#popupHolder h3').html($('#popupHolder h3 cufontext').html() );

	Cufon.replace('#popupHolder h3, #popupHolder a.moreLink', {forceHitArea:true});

	$('#popupHolder').fadeIn();
}


function slideBox() {

	var $parent = $(this).parent();

	if (!$(this).hasClass('active')) {
		$('.active',$parent).removeClass('active').next('.fullText').slideToggle();
		$(this).addClass('active').next('.fullText').slideToggle();
	}

}

$(document).ready(function () { 


	//hide any fullTexts:
	$('.singleRecord #container.whatsNew .col .fullText').hide();
	$('.singleRecord #container.author .col .fullText').hide();
	$('.singleRecord #container.editionArchive .col .fullText').hide();
	$('#container.event .col .fullText').hide();

	// show 'intro' texts on reviews and extracts
	$('.singleRecord #Reviews a').show();
	$('.singleRecord #ReadExtract a').show();
	$('.singleRecord #Reviews p').show();
	$('.singleRecord #ReadExtract p').show();
	$('.singleRecord #Reviews h3').show();
	$('.singleRecord #ReadExtract h3').show();
	$('.singleRecord #Reviews .fullText a').show();
	$('.singleRecord #ReadExtract .fullText a').show();


	// whats new content popups
	$('.singleRecord .tab .trigger').click (initPopup); 


	// whats new editions slideboxes
	$('.singleRecord #editionsBox h4').click(slideBox);

	// events slideboxes
	$('#eventsBox h4').click(slideBox);
	
	$('a.trigger').hover(imageMap);
	// hover off pop up, fade out
	$('div.popupBox').hover(function() {},function(){$(this).fadeOutPopup()});

	$('#eventsBox h4:first').click( );
	
	$('.img .imgBox a').slimbox({});
});
// JavaScript Document
// Author: Wagner
// 25/06/2009
// go through all form fields and clear the value, if they start with 'enter' or 'please'
jQuery.fn.clearAllDefaultsFromTextFields = function(target, exclude){

    return this.each(function()
    {
        selector = "input:text, textarea";

        $(selector, $(this)).each(function()
        {
            $target = $(this);

            if (exclude)
            {
                $target = $(this).not(exclude);
                if (!$target.length)
                return;
            }

            if ($target.val().toLowerCase().substring(0, 6) == "enter " ||
            $target.val().toLowerCase().substring(0, 7) == "please ")
            {
                $target.val("");
            }
        });
    });
};

// give all text fields a focus action, so that when they are focussed on, they trigger the command to go through all fields as above
$(document).ready(function(){

    $("input:text, textarea").focus(function()
    {
        $(this).closest("form").clearAllDefaultsFromTextFields('');
    });

    // find any forms and make sure that before submit, we clear the defaults too
    $("form").submit(function()
    {
        // Exclude delivery notes field
        $(this).clearAllDefaultsFromTextFields('', '.frdeliveryNotes textarea');
        return true;
    });

    $("form").each(function(){

        if ($(this).hasClass("updateForm"))
        {
            // Exclude delivery notes field
            $(this).clearAllDefaultsFromTextFields('', '.frdeliveryNotes textarea');
        }
    });
});


(function($) {

	$.fn.mediaSwapper = function(opts) {
	   
		// build main options before element iteration
		//var opts = $.extend({}, $.fn.mediaSwapper.defaults, options);
		// iterate and reformat each matched element
		
		// A few safeguards - we need this in case there's no media swapper on the page
		if( this.length < 1 )
		{
			$.fn.mediaSwapper.onInit = function( ) {}
		}	
		
		return this.each(function() {
			
			var $theSwapper = $(this);
			var $items = $(opts.itemSelector, $theSwapper);
			var $selectors = false;
			var timer = [];
			var $current = false;
			var playing = false;
			// Get the base z-index
			var baseZ = parseInt( $theSwapper.eq(0).css( 'z-index' ) );
			
			var initFired = false;
			
			
			$.fn.mediaSwapper.onInitCB = [];
			$.fn.mediaSwapper.onInit = function( callback ) 
			{ 
				$.fn.mediaSwapper.onInitCB.push( callback );
				
				if( initFired )
				{
					// The init has already been fired, so we don't wait for it - call the callback at once
					for($i = 0; $i < $items.length; $i++)
					{
						callback( $items.eq($i) );
					}
				} 
			}
				
			// loop through each of the items and assign all of the actions
			
			
			
			function startContent () {
				
				// and end detection
				
				if (playing && $items.length > 1) {
				
					// trigger countdown to next item
					// unless is a video, which we play, and put an event listener on to move on

					if ($current.hasClass('video')) {
						var api = $('div.mediaAsset div.mediaPlayer',$current).data('api');
						if( api ) {
							
							if( opts.autoplayVideo )
								api.play();
								
							api.onEnd( $.fn.mediaSwapper.next );
						}
						
					} else {
						// we do a push here because if someone hammers the next button you get loads of intervals starting - not good.
						timer.push( setInterval( $.fn.mediaSwapper.next,  opts.interval ) );
					}
				}
				
			}
			
			function stopContent () {
			
				// and clear any intervals
				for (var i=0;i<timer.length;i++) clearTimeout(timer[i]);
				// pause any playing video content
				
				if ($current.hasClass('video')) {
					var api = $('div.mediaAsset div.mediaPlayer',$current).data('api');
					if( api ) {
						api.pause();
					}
				}
				
			}
			
			function moveTo ( $next ) {
				
				if ($next == $current)
					return false;
				
				stopContent();
				
				// callback for closing slides
				$.fn.mediaSwapper.onCloseSlide ( $current, $next );
				$.fn.mediaSwapper.bringIn($next);
				
			}
			
			/* PREVIEWS */
			/* PREVIEWS */
			
			function makeItemPreviews () {
				$items.each (function () {
					// make a div for each item
					
					var $theItem = $(this);
					var $theSelector = $('<div class="transport_chooser"></div>');
					
					$('#transport_playpause', $theSwapper).append( $theSelector );
					
					$theSelector.hover(
						function () { $(this).addClass('hover'); },
						function () { $(this).removeClass('hover'); }
					);
					
					$theSelector.click(function () {
						moveTo($theItem);
					});
					
					if (opts.imagePreview) {
						
						var imgURL = '';
						
						// not working because the jwplayer has already stepped in
						if ($(this).hasClass('video')) 
						{
						//alert($(this).html());
							//var api = $('div.mediaPlayer', this).data('api');
							
							var patt=/poster="([a-z0-9\/\.\-\_]+)/i;//\"
							var imgURL=patt.exec($(this).html());
							imgURL = imgURL[1];
							//alert(imgURL);
							//imgURL = patt=/pattern/modifiers;//api.getPosterImage();
						} 
						else 
						{
							imgURL = $('img',$theItem).attr('src');
						}
						// either the image, or the video poster image
						//alert(imgURL);
						
					// if we are showing imagePreviews, we'll show a mini image to click
						var image = '<div class="preview"><img src="'+imgURL+'" alt="*" width="'+opts.thumbWidth+'" height="'+opts.thumbHeight+'" />';
						
						if( opts.previewMask )
						{
						  image += '<div class="mask" style="width:' + opts.thumbWidth + 'px; height:' + opts.thumbHeight + 'px;">&nbsp;</div>';
						}
						
						image += '</div>';
						
						$theSelector.append(image);
					}
					// and save the items selector so that when it becomes active, we can mark it
					$theItem.data('selector', $theSelector);
					
					
				});
			}
			//makeItemPreviews();
			
			/* TRANSPORT CONTROLS */
			/* TRANSPORT CONTROLS */
			/* TRANSPORT CONTROLS */
			/* TRANSPORT CONTROLS */
			/* TRANSPORT CONTROLS */
			if ($items.length > 1) {
			
				var $transport = $('<div id="transport"><div id="transport_playpause"><div id="transport_play"></div></div><div id="transport_nextPrevious"><div id="transport_previous"></div><div id="transport_next"></div></div></div>');
		
				$('#transport_next, #transport_previous, #transport_play',$transport).hover(
					function () { $(this).addClass('hover'); },
					function () { $(this).removeClass('hover'); }
				);
				$('#transport_next',$transport).click(function () {
					$.fn.mediaSwapper.next();			
				});
				$('#transport_previous',$transport).click(function () {
					$.fn.mediaSwapper.previous();
				});
				$('#transport_play',$transport).click(function () {
					//playPause();
					if ($(this).hasClass('playing')) {
						$.fn.mediaSwapper.pause();
					} else {
						$.fn.mediaSwapper.play();
					}
					//$.fn.mediaSwapper.previous();
				});
			
			
				$theSwapper.prepend( $transport );
			
			}
			
			
			/* PLAY PAUSE CONTROLS */
			/* PLAY PAUSE CONTROLS */
			/* PLAY PAUSE CONTROLS */
			/* PLAY PAUSE CONTROLS */
			/* PLAY PAUSE CONTROLS */
			
			$.fn.mediaSwapper.next = function() {
				var $next = $current.next() == 'undefined' || $current.next().html() == null ? $items.eq(0) : $current.next();
				
				if( $current.hasClass( 'video' ) ) {
					var api = $('div.mediaAsset div.mediaPlayer',$current).data('api');
					if( api ) {
						api.stop();
					}
				}
				
				moveTo($next);
			};
			
			$.fn.mediaSwapper.previous = function() {
				var $prev = $current.prev().attr('id')== 'transport' ? $items.eq($items.length - 1) : $current.prev();
				
				moveTo($prev);
				
			};
			$.fn.mediaSwapper.play = function() {
			
				if ($items.length > 1) {
				// there must be a better way
					$('#transport_play', $theSwapper).addClass('playing');
				//alert("play");
					playing = true;
				}
				startContent();
				
			};
			$.fn.mediaSwapper.pause = function() {
				// there must be a better way
				$('#transport_play', $theSwapper).removeClass('playing');
				
				playing = false;
				// if there is an interval, unset it
				stopContent();
				
	//			alert("pause");
			};
			$.fn.mediaSwapper.onOpenSlide = function( $currentItem ) {};
			$.fn.mediaSwapper.onCloseSlide = function( $currentItem, $nextItem ) {};
			
			
			/* LAYOUT THE IMAGES */
			/* LAYOUT THE IMAGES */
			/* LAYOUT THE IMAGES */
			/* LAYOUT THE IMAGES */
			/* LAYOUT THE IMAGES */
			/* LAYOUT THE IMAGES */
			
			
			function init () {
				
				// Get the base z-index
				if(!baseZ) baseZ=0;
				
				//$items.css({"position":"absolute","top":"0px","left":"0px"}).hide();
				$items.css({ "position":"absolute",
							 "top":"0px",
							 "left":"0px", 
							 zIndex: baseZ + 1,
							 "opacity" : '0.000001'});
				
				// side to side
				//if (opts.mode == 'slide') layoutHorizontally();
				
				makeItemPreviews();
											
				if (opts.autoplay == true && $items.length > 1) {
					$('#transport_play', $theSwapper).addClass('playing');
					playing = true;
				}
				
				for($i = 0; $i < $items.length; $i++)
				{
					for( cb in this.onInitCB ) 
					{
						this.onInitCB[cb]( $items.eq($i) );
					} //Callback
				}
				
				// moan if mode is fadeOut and the image is big
				if ( $theSwapper.width() > 900 && opts.mode == 'fadeOut') alert("mediaSwapper.mode 'fadeOut' does not work well with large images - consider using 'wipeRight' or 'crossFade' instead." );
				
				initFired = true;
				
				$.fn.mediaSwapper.bringIn ( $items.eq(0) );
			};
			
			/* ANIMATION */
			/* ANIMATION */
			/* ANIMATION */
			/* ANIMATION */
			/* ANIMATION */
			
			// @todo put back in the Z-index functionality because hide/show breaks in IE - CL 01-08-2010
			// @todo check transitions to ensure they use Z-index and not hide
			function andOut ( $previous ) {
				if ($previous != null) {
					$previous.css('display','none');
					$previous.removeClass('previous');
				}
				// callback for opening slides
				$.fn.mediaSwapper.onOpenSlide ( $current );
				startContent();
			};
			
			function customAnimation ($next, $previous) {
				//$.fn.mediaSwapper.customAnimation($next, $previous);
			
			};
			
			function wipeRight ($next, $previous) {
				// we start, at the left, with width 1, opacity 1, and animate up to the width of the imageswapper
				$next.css({
					'width':'1px',
					'overflow':'hidden',
					'opacity':1
				});
				$next.animate({
 				    width:  $theSwapper.css('width')
				}, opts.animationSpeed, opts.easing, function() {
					andOut ( $previous );
  				});
			};

			function wipeUp ($next, $previous) {
			// we start, at the left, with width 1, opacity 1, and animate up to the width of the imageswapper
			// the image needs to stay still, so 
				$next.css({
					'paddingTop':$theSwapper.css('height'),
					'opacity':1
				});
				$next.animate({
 				    paddingTop:  '0px'
				}, opts.animationSpeed, opts.easing, function() {
					andOut ( $previous );
  				});
			
			
			};
			function wipeDown ($next, $previous) {
				$next.css({
					'height':'1px',
					'overflow':'hidden',
					'opacity':1
				});
				$next.animate({
 				    height:  $theSwapper.css('height')
				}, opts.animationSpeed, opts.easing, function() {
					andOut ( $previous );
  				});
			
			};
			function wipeLeft ($next, $previous) {
			// we start, at the left, with width 1, opacity 1, and animate up to the width of the imageswapper
			// the image needs to stay still, so 
				$next.css({
					'paddingLeft':$theSwapper.css('width'),
					'opacity':1
				});
				$next.animate({
 				    paddingLeft:  '0px'
				}, opts.animationSpeed, opts.easing, function() {
					andOut ( $previous );
  				});
			
			};
			function fadeOut ($next, $previous) {
				// fades out current, then fades in new
				// if there is a current
				//$next.css({
					//'paddingLeft':$theSwapper.css('width'),
					//'opacity':0.0001
				//});
				
				if ($previous == null) {
					crossFade ($next, $previous);
				
				} else {
					
					$previous.animate({
 				    	opacity:  0
					}, opts.animationSpeed, opts.easing, function() {//
					// Animation complete.
						$next.animate({
 				   			opacity:  1
							}, opts.animationSpeed, opts.easing, function() {//
								andOut ( $previous );
						});
  					});
				}
				
			};
			function crossFade ($next, $previous) {
				$next.css({
					//'paddingLeft':$theSwapper.css('width'),
					//'opacity':0.0001
				});
				$next.animate({
 				    opacity:  1
				}, opts.animationSpeed, opts.easing, function() {//
					// Animation complete.
					andOut ( $previous );
  				});
			};
			
			
			$.fn.mediaSwapper.bringIn = function ( $next ) {
	
				var $previous = null;
				
				/**
				 * The images could be prefixed by a # because the PHP is trying not to let the
				 * browser load it to save on loading time, this removes the # so that the image
				 * is loaded before the swap takes place
				 */ 
				$imgSource = $next.find('div.mediaAsset img').attr('src');
				
				if($imgSource) {
					$pos = $imgSource.indexOf('#');
					if( $pos != -1)
						$next.find('div.mediaAsset img').attr('src', $imgSource.substr(($pos+1)));
				}
				
				// mark the last one
				if ($current != false) 
				{
					$current.addClass('previous');
					$previous = $current;
				}
				// mark the new current
				$current = $next;
				
				// TRANSITIONS SHOULD KICK IN HERE
				
				// now we make sure the new one is visible and showing as hidden, so that it loads
				$current.css( { display: 'block' } );
				$current.css( { opacity: '0.0001' } );
				$current.css( { zIndex: baseZ + 3 } );
				if ($previous != null) $previous.css( { zIndex: baseZ + 2 } );
				
				// TRANSITIONS SHOULD END HERE
				
				// mark as current
				$items.removeClass('current');
				$current.addClass('current');
				
				// mark the selector with active/inactive
				$items.each(function () { $(this).data('selector').removeClass('current'); });
				$current.data('selector').addClass('current');
				
				// now we have different handlers for each fade method
				
				// user selected
				if (opts.animateIn != null) customAnimation( $next, $previous ) ;
				else if (opts.mode == 'wipeRight') wipeRight( $next, $previous );
				else if (opts.mode == 'wipeUp') wipeUp( $next, $previous );
				else if (opts.mode == 'wipeDown') wipeDown( $next, $previous );
				else if (opts.mode == 'wipeLeft') wipeLeft( $next, $previous );
				else if (opts.mode == 'fadeOut') fadeOut( $next, $previous );
				else if (opts.mode == 'crossFade') crossFade( $next, $previous );
				
		
				return $next;
			};
			
			
			init();
			
		});
		
	};
	
	//
	// end of closure
	//

})(jQuery);

$(document).ready( function() {

  	var JSONdefaults = new Object({
		'interval': 5000, // time on each image
		'animationSpeed': 750, // time taken over animations
		'easing': 'linear', // time taken over animations
		'hgap': 1, // gutter for slide mode
		'autoplay':true, // whether to start automatically
		'imagePreview': false, // use little images instead of dots on the selectors
		'imageSwapperSelector': 'div.mediaSwapper', // class to look for
		'itemSelector': 'div.mediaItem', // class to look for
		'mode': 'wipeRight', // fadeOut, crossFade, wipeRight, wipeUp, wipeDown, wipeLeft or custom, for which you must define a function $.fn.mediaSwapper.customAnimation = function () {}; (ok - this is not fully implemented)
		'thumbWidth': 100,
		'thumbHeight': 50,
		'previewMask': false,
		'excludeTemplates': 'textMultipleImages',
		'autoplayVideo': true
	});
	
	// php generated overrides
	var JSONParameters={"mode":"fadeOut"};
	var params = getParams( 'mediaSwapper', JSONdefaults, JSONParameters );	
	
 	if( params.excludeTemplates ) {
      var exclude = params.excludeTemplates.split(',');
      for( i = 0; i < exclude.length; i++) {
          if( $('#container').hasClass( '.' + exclude[i] ) )
              return;
      }
    }

	$(params.imageSwapperSelector).mediaSwapper(params);

});


/* define your own custom functions to override functionality 
$.fn.mediaSwapper.customAnimation = function ($next, $previous) {};
$.fn.mediaSwapper.onOpenSlide = function ($current) {};
*/
