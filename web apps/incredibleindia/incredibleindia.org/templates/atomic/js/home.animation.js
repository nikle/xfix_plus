var fullslideWidth;
				var travelslide;
				var tradeslide;
				var mediaslide;
				var openSlide;
				var half;
				var full;
				jQuery(document).ready(function(){
					fixLayout();
					jQuery(window).resize(function(){
						fixLayout();
					});
					
					full = parseInt(jQuery('#logoimg').width());
					half = Math.ceil(parseInt(full*0.5));

					jQuery(".slide").mouseover( function(){
						var id = jQuery(this).attr('id');
						if(id == "travel" && openSlide != "travel"){
							openSlide = "travel";
							hideLogo(0);
							jQuery(this).stop().animate({width: fullslideWidth+"px"}, {duration:500});
							jQuery(".travel-inner").fadeIn('slow');
						}
						if(id == "trade" && openSlide != "trade"){
							openSlide = "trade";
							hideLogo(0);
							jQuery(this).stop().animate({width: fullslideWidth+"px"}, { queue:false, duration:500});
							jQuery(".trade-inner").fadeIn('slow');
						}
						if(id == "media" && openSlide != "media"){
							openSlide = "media";
							hideLogo(0);
							jQuery(this).stop().animate({width: fullslideWidth+"px"}, { queue:false, duration:500});
							jQuery(".media-inner").fadeIn('slow');
						}
					});

					jQuery(".slide").mouseleave( function(){
						var id = jQuery(this).attr('id');
						if(id == "travel"){
							openSlide = "";
							jQuery(this).stop().animate({width: travelslide+"px"}, { queue:false, duration:500});
							jQuery(".travel-inner").fadeOut('slow');

						}
						if(id == "trade"){
							openSlide = "";
							jQuery(this).stop().animate({width: tradeslide+"px"}, { queue:false, duration:500});
							jQuery(".trade-inner").fadeOut('slow');
						}
						if(id == "media"){
							openSlide = "";
							jQuery(this).stop().animate({width: mediaslide+"px"}, { queue:false, duration:500});
							jQuery(".media-inner").fadeOut('slow');
						}
					});

					jQuery("#tabs").mouseleave(function(){
						hideLogo(1);
					})
				})
				function hideLogo( id ){
					if(id == 0){
						if(parseInt(jQuery("#logoimg").width()) >= full)
						{
							jQuery("#logoimg").stop().animate({width:'50%'});
							jQuery("#welcome").each(function(){
								var font = Math.ceil(parseInt(parseInt(jQuery(this).css('font-size'))*0.5));
								jQuery(this).stop().animate({fontSize:font+'px'});
							});
						}
					}else{
						if(jQuery("#logoimg").width() <= half)
						{
							jQuery("#logoimg").stop().animate({width:'100%'});
							jQuery("#welcome").each(function(){
								var font = Math.ceil(parseInt(parseInt(jQuery(this).css('font-size'))*2));
								jQuery(this).stop().animate({fontSize:font+'px'});
							});
						}
					}
				}
				function fixLayout(){
					var winHeight = jQuery(window).height();
					var winWidth = jQuery(window).width();
					var percent = parseInt(parseFloat(winWidth/1920)*100)/100;
					slideWidth = (jQuery(window).width()*14.5 )/100;
					fullslideWidth = (jQuery(window).width()*66 )/100;
					travelslide = (jQuery(window).width()*42 )/100;
					tradeslide = (jQuery(window).width()*28 )/100;
					mediaslide = (jQuery(window).width()*14 )/100;

					jQuery(".full-height").css({'height': jQuery(window).height()+'px'});
					jQuery("#logo").css({'width': (jQuery(window).width()*48 )/100+'px'});
					jQuery(".main-links li").css({'width': slideWidth+'px'});

					jQuery("#travel").css({'width': travelslide+'px'});
					jQuery("#trade").css({'width': tradeslide+'px'});
					jQuery("#media").css({'width': mediaslide+'px'});

				}