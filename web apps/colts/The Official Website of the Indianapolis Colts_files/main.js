;window.distFolder="/f2c85621dafeaeca7d532c0cbbaa28b1";﻿(function (d, w, r) {
  w.time = (w.console && w.console.time ? function (name) {
    w.console.time('[TIMING] ' + name);
  } : function () { });
  w.timeEnd = (w.console && w.console.time ? function (name) {
    w.console.timeEnd('[TIMING] ' + name);
  } : function () { });
  r.config({
    waitSeconds: 30,
    baseUrl: (w.baseUrl || '') + '/compiledassets/js' + (w.distFolder || ''),
    paths: {
      jquery: 'vendor/jquery/jquery-3.2.1.min',
      jquery_ui: 'vendor/jquery/jquery-ui.min',
      jquery_visible: 'vendor/jquery/plugins/jquery.visible.min',
      owlcarousel2: 'vendor/owlcarousel2/owl.carousel-2.3.4.min',
      jquery_metadata: 'vendor/tablesorter/jquery.metadata',
      tablesorter: 'vendor/tablesorter/jquery.tablesorter',
      clipboard: 'vendor/clipboard/clipboard.min',
      autocomplete: 'vendor/autocomplete/jquery.autocomplete.min',
      liveblog_integration: 'vendor/liveblog/liveblog.integration.min',
      cookieconsent: 'vendor/cookieconsent/cookieconsent.min',
      moment: 'vendor/moment/moment-2.22.1.min',
      zoomph: 'https://visuals.zoomph.com/visual/loader',
      facebook: 'https://connect.facebook.net/en_US/sdk',
      twitter: 'https://platform.twitter.com/widgets',
      instagram: 'https://www.instagram.com/embed',
      marketo: 'https://' + window.marketoHostName + '/js/forms2/js/forms2',
      playaction: 'https://www.nfl.com/libs/playaction/api',
      iframe_resizer: 'vendor/iframe-resizer/iframeResizer.min',
      fevo: 'https://sdk.fevo.com/v1/fevo',
      md5: 'vendor/md5/md5',
      cookies: 'vendor/js-cookie/js.cookie.min'
    },
    map: {
      '*': {
        'jquery': 'modules/noconflict'
      },
      'modules/liveblog': {
        'jquery': 'jquery'
      },
      'modules/noconflict': {
        'jquery': 'jquery'
      }
    },
    shim: {
      'owlcarousel2': {
        deps: ['jquery']
      },
      'jquery_visible': {
        deps: ['jquery']
      },
      'clipboard': {},
      'facebook': { exports: 'FB' },
      'autocomplete': {
        deps: ['jquery']
      },
      'cookieconsent': {
        deps: ['jquery']
      },
      'iframe_resizer': {
        deps: ['jquery']
      },
      'fevo': { exports: 'Fevo' },
      'md5': { exports: 'md5' },
      'cookies': {
        deps: ['jquery']
      }
    }
  });
  if (w.MiniProfiler && w.MiniProfiler.$) {
    r.undef('jquery');
  }

  var dataRequireCallback = function () {
    var requiredModules = { modules: [] };
    var i = 0;
    var elements = d.querySelectorAll('*[data-require]');
    var l = elements.length;
    for (i; i < l; i++) {
      var element = elements[i];
      var attr = element.getAttribute('data-require');
      if (!attr) {
        return;
      }
      if (!requiredModules[attr]) {
        requiredModules[attr] = [];
      }
      requiredModules[attr].push(element);
      if (requiredModules.modules.indexOf(attr) === -1) {
        requiredModules.modules.push(attr);
      }
    }
    i = 0;
    l = requiredModules.modules.length;
    var requiredCallback = function (moduleId) {
      return function (loadedModule) {
        if (!loadedModule) {
          console.warn(moduleId, 'is undefined. skipping!');
          return;
        }
        if (typeof loadedModule.initByElement === 'function') {
          var rElements = requiredModules[moduleId];
          for (var j = 0; j < rElements.length; j++) {
            if (!rElements[j].initialized) {
              loadedModule.initByElement(rElements[j]);
              rElements[j].initialized = true;
            }
          }
        }
      };
    };
    for (i; i < l; i++) {
      var module = requiredModules.modules[i];
      r([module], requiredCallback(module));
    }
    requestAnimationFrame(dataRequireCallback);
  };

  if ('IntersectionObserver' in window) {
    var observeImages = function (selector, from, to) {
      var lazyImageObserver = new window.IntersectionObserver(function (entries, observer) {
        var i = 0, l = entries.length;
        for (i; i < l; i++) {
          var entry = entries[i];
          if (!entry.isIntersecting) {
            continue;
          }
          var lazyImage = entry.target;
          lazyImage.setAttribute(to, lazyImage.getAttribute(from));
          observer.unobserve(lazyImage);
        }
      });
      var lazyItems = d.querySelectorAll(selector), i = 0, l = lazyItems.length;
      for (i; i < l; i++) {
        lazyImageObserver.observe(lazyItems[i]);
      }
    };
    observeImages('picture:not([is-lazy]) img[data-src], .d3-o-footer img[data-src]', 'data-src', 'src');
    observeImages('picture:not([is-lazy]) source[data-srcset]', 'data-srcset', 'srcset');
  }
  else {
    var lazyCallback = function (selector, from, to) {
      return function () {
        var lazyItems = d.querySelectorAll(selector), i = 0, l = lazyItems.length;
        var rafSetImg = function (item, _from, _to) {
          return function () {
            item.setAttribute(_to, item.getAttribute(_from));
          };
        };
        for (i; i < l; i++) {
          var item = lazyItems[i];
          requestAnimationFrame(rafSetImg(item, from, to));
        }
      };
    };
    requestAnimationFrame(lazyCallback('picture:not([is-lazy]) img[data-src], .d3-o-footer img[data-src]', 'data-src', 'src'));
    requestAnimationFrame(lazyCallback('picture:not([is-lazy]) source[data-srcset]', 'data-srcset', 'srcset'));
  }
  r(['modules/tealium', 'modules/lazyload']);
  requestAnimationFrame(dataRequireCallback);
  var liveElements = d.querySelectorAll('*[data-live]');
  if (liveElements.length) {
    r(['modules/services/liveService']);
  }

  w.isMobileViewport = function () {
    return w.innerWidth < 1024;
  };
}(document, window, require));
