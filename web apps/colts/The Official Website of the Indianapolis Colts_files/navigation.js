﻿define('modules/navigation', ['jquery'], function ($) {
  var consts = {
    navWrapperClass: '.d3-o-nav__wrap',
    navLogoClass: '.d3-o-nav__logo',
    searchClass: '.d3-o-nav__search',
    searchIsOpenClass: 'd3-o-search--is-open',
    hamburgerIsOpenClass: 'd3-o-hamburger--is-active',
    hamburgerClass: '.d3-o-nav__hamburger',
    navIsOpenClass: 'd3-o-nav--is-open',
    moreIsClicked: 'd3-o-nav__more-is-clicked',
    searchInput: '.d3-o-search__input',
    searchOpenBtn: '.d3-o-search__open-btn',
    searchCloseBtn: '.d3-o-search__close-btn',
    navIsStickyClass: 'd3-o-nav--is-sticky',
    hasStickyNavClass: 'nfl-has-sticky-nav',
    hasSecondaryNavClass: 'd3-o-nav__has-secondary-nav',
    preventScrollClass: 'nfl-u-disable--scrolling'
  };
  var $body = $('body');
  var $html = $('html');
  var navigationPlugin = (function () {
    return {
      $navigation: $(),
      options: {
        onSearchClick: function (navigationPlugin) {
          return function (event) {
            var $target = $(event.target);
            $target = $target.is('button') ? $target : $target.parents('button');
            if ($target.is(consts.searchOpenBtn) || $target.closest(consts.searchOpenBtn).length) {
              if (!navigationPlugin.$navigation.hasClass(consts.searchIsOpenClass)) {
                event.preventDefault();
                navigationPlugin.$navigation.addClass(consts.searchIsOpenClass).find(consts.searchInput).focus();
              }
            }
            else if ($target.is(consts.searchCloseBtn) || $target.closest(consts.searchCloseBtn).length) {
              navigationPlugin.$navigation.removeClass(consts.searchIsOpenClass);
              navigationPlugin.$navigation.find(consts.searchOpenBtn).focus();
            }
            //leave search when hitting ESC key
            if (navigationPlugin.$navigation.hasClass(consts.searchIsOpenClass)) {
              $target.parents(consts.searchClass).on('keyup.nav.search', function (e) {
                if (e.which === 27) { //ESC
                  $target.parents(consts.searchClass).find(consts.searchCloseBtn).trigger('click');
                }
              });
            } else {
              $target.parents(consts.searchClass).off('keyup.nav.search');
            }
          };
        },
        onHamburgerClick: function (navigationPlugin) {
          return function (event) {
            event.preventDefault();
            var $target = $(event.target);
            if ($target.closest(consts.hamburgerClass).length) {
              $target = $target.closest(consts.hamburgerClass);
            }
            $target.toggleClass(consts.hamburgerIsOpenClass).attr('aria-pressed', $target.hasClass(consts.hamburgerIsOpenClass));
            navigationPlugin.$navigation.toggleClass(consts.navIsOpenClass);
            navigationPlugin.preventBodyScroll(navigationPlugin);

            //leave hamburguer menu when hitting ESC key
            if ($target.hasClass(consts.hamburgerIsOpenClass)) {
              $target.parents(consts.navWrapperClass)
                .on('keyup.nav.hamburguer', function (e) {
                  if (e.which === 27) { //ESC
                    $target.trigger('click.nav.hamburger');
                  }
                });
              //loop through menu items on TAB/SHIFT+TAB
              $target.parents(consts.navWrapperClass)
                .on('keydown.nav.hamburguer', function (e) {
                  if (e.which !== 9) {
                    return;
                  }
                  var $focusedElement = $(document.activeElement);
                  var $allMenuItems = $(this).find('.d3-o-nav__item:visible');
                  requestAnimationFrame(function () {
                    if (e.shiftKey) { // SHIFT pressed
                      if ($focusedElement.is(consts.navLogoClass)) { //first item
                        $allMenuItems.last().find('a').focus();
                        e.preventDefault();
                      }
                    } else {
                      var $focusedElementListItem = $focusedElement.is('a') ? $focusedElement.parent('.d3-o-nav__item') : $focusedElement;
                      if ($allMenuItems.index($focusedElementListItem) === $allMenuItems.length - 1) { //last item
                        $(consts.navLogoClass).focus();
                        e.preventDefault();
                      }
                    }
                  });
                });
            } else {
              $target.parents(consts.navWrapperClass)
                .off('keyup.nav.hamburguer')
                .off('keydown.nav.hamburguer');
            }

          };
        },
        onDropdownTriggerClick: function (navigationPlugin) {
          return function (event) {
            event.preventDefault();
            var $target = $(event.target);
            if (!$target.is('a[data-value="dropdown_trigger"]')) {
              $target = $target.closest('a[data-value="dropdown_trigger"]');
            }
            $target.toggleClass(consts.moreIsClicked);
            navigationPlugin.$navigation.find('.d3-o-nav__dropdown').toggleClass(consts.navIsOpenClass);
          };
        },
        onScroll: function (navigationPlugin) {
          return function () {
            var startOffset = navigationPlugin.getStartOffset(navigationPlugin);
            navigationPlugin.addStickyClass(navigationPlugin, $(window).scrollTop(), startOffset);

            requestAnimationFrame(navigationPlugin.options.onScroll(navigationPlugin));
          };
        }
      },
      addStickyClass: function (navigationPlugin, currentOffset, headerOffset) {
        if (currentOffset >= headerOffset) {
          navigationPlugin.$navigation.addClass(consts.navIsStickyClass);
          $body.addClass(consts.hasStickyNavClass);
        } else {
          navigationPlugin.$navigation.removeClass(consts.navIsStickyClass);
          $body.removeClass(consts.hasStickyNavClass);
        }
      },
      getStartOffset: function (navigationPlugin) {
        var navigationOffset = navigationPlugin.$navigation.offset().top;
        var $previousElement = navigationPlugin.$navigation.prevAll().first();
        if ($previousElement.length) {
          navigationOffset = $previousElement.height() + $previousElement.offset().top;
        }
        navigationOffset += navigationPlugin.$navigation.height();
        if (typeof (navigationPlugin.$navigation.data('startOffset')) === 'undefined') {
          navigationPlugin.$navigation.data('startOffset', navigationOffset);
        }
        return navigationPlugin.$navigation.data('startOffset');
      },
      addActionRelatedLinkToPrimaryNavigation: function (navigationPlugin) {
        var $lastItemInPrimaryNav = navigationPlugin.$navigation.find('.d3-o-nav__item--has-submenu');
        var $rightNav = navigationPlugin.$navigation.find('.d3-o-nav--action-related--mobile');
        var $rightNavList = $rightNav.find('.d3-o-list li');

        $rightNavList.addClass('d3-o-nav__item--action-related');
        $rightNavList.insertBefore($lastItemInPrimaryNav);
        $rightNav.remove();
      },
      checkSecondaryNavigation: function ($navigation) {
        if ($navigation.hasClass(consts.hasSecondaryNavClass)) {
          $body.addClass('nfl-has-secondary-nav');
        }
      },
      preventBodyScroll: function (navigationPlugin) {
        $html.toggleClass(consts.preventScrollClass);
        $body.toggleClass(consts.preventScrollClass);
      },
      initScroll: function (navigationPlugin) {
        return function () {
          navigationPlugin.options.onScroll(navigationPlugin)();
        };
      },
      initByElement: function (target) {
        var _this = this;
        var $navigation = $(target);
        _this.$navigation = $navigation;

        $navigation.find('.d3-o-search__close-btn, .d3-o-search__open-btn').on('click.nav.search', _this.options.onSearchClick(_this));
        $navigation.find(consts.hamburgerClass).on('click.nav.hamburger', _this.options.onHamburgerClick(_this));
        $navigation.find('nav li a[data-value="dropdown_trigger"]').on('click.nav.dropdowntrigger', _this.options.onDropdownTriggerClick(_this));

        if (window.googletag && window.googletag.pubadsReady) {
          window.googletag.pubads().addEventListener('slotOnload', function (event) {
            _this.initScroll(_this)();
          });
          $(window).on('load.nav', function () {
            _this.initScroll(_this)();
          });
        } else {
          _this.initScroll(_this)();
        }
        _this.addActionRelatedLinkToPrimaryNavigation(_this);
        _this.checkSecondaryNavigation($navigation);
      }
    };
  }());
  return navigationPlugin;
});
