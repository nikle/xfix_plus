﻿define('modules/cookieconsent', ['jquery', 'cookieconsent', 'modules/services/deferredService'], function ($, cookieconsent, deferredService) {
  var cookieConsentDefaultOptions = {
    content: {
      message: 'We use our own and third-party cookies to improve your experience and our services. By continuing and using the site, including by remaining on the landing page, you consent to the use of cookies.',
      link: 'Click here for more information on our <strong>Cookie Policy</strong>.'
    },
    elements: {
      header: '<span class="d3-o-cookie-header">{{header}}</span>&nbsp;',
      message: '<span id="cookieconsent:desc" class="d3-o-cookie-message">{{message}}</span>',
      messagelink: '<span id="cookieconsent:desc" class="d3-o-cookie-message">{{message}} <a aria-label="learn more about cookies" tabindex="0" class="d3-o-cookie-link" href="{{href}}" target="_blank">{{link}}</a></span>',
      dismiss: '<a aria-label="dismiss cookie message" tabindex="0" class="cc-btn cc-dismiss d3-o-cookie-btn d3-o-cookie-btn--dismiss">{{dismiss}}</a>',
      allow: '<a aria-label="allow cookies" tabindex="0" class="cc-btn cc-allow d3-o-cookie-btn d3-o-cookie-btn--allow">{{allow}}</a>',
      deny: '<a aria-label="deny cookies" tabindex="0" class="cc-btn cc-deny d3-o-cookie-btn d3-o-cookie-btn--deny">{{deny}}</a>',
      link: '<a aria-label="learn more about cookies" tabindex="0" class="d3-o-cookie-link" href="{{href}}" target="_blank">{{link}}</a>',
      close: '<span aria-label="dismiss cookie message" tabindex="0" class="d3-o-cookie-link d3-o-cookie-link--close">{{close}}</span>',
    },
    window: '<div role="dialog" aria-label="cookieconsent" aria-describedby="cookieconsent:desc" class="cc-window d3-o-cookie {{classes}}">{{children}}</div>'
  };

  function getResponseHeader(mockConsentOptions, options) {
    deferredService.getResponseHeader({
      type: 'HEAD',
      url: (mockConsentOptions && mockConsentOptions.url) ? mockConsentOptions.url : window.location.href
    }, 'X-NFL-Geo')
      .done(function (data) {
        if (!data) {
          return;
        }
        options.law.countryCode = getCountryCode(data);
        window.cookieconsent.initialise(options);
      })
      .fail(function (jqXHR, textStatus, errorThrown) {
        return new Error();
      });
  }

  function getCountryCode(geoLocationHeader) {
    var geoLocationSplit = geoLocationHeader.split('=');
    if (!(geoLocationSplit.length && geoLocationSplit[1])) {
      return;
    }
    return geoLocationSplit[1];
  }

  var cookieConsentPlugin = (function () {
    return {
      getCountryCode: undefined,
      options: {
      },
      init: function (mockConsentOptions) {
        this.getCountryCode = getCountryCode;
        var $cookieConsent = $('.nfl-c-cookie-consent');
        var cookiePolicyUrl = $cookieConsent.attr('data-cookiepolicyurl');
        var cookieDomain = $cookieConsent.attr('data-cookiedomain');
        var cookieVariables = {
          content: {
            href: cookiePolicyUrl
          },
          cookie: {
            domain: cookieDomain
          }
        };
        var geoLocationVariable = {
          law: {
            countryCode: '',
            regionalLaw: false,
            hasLaw: ['AL', 'AD', 'AM', 'AT', 'AZ', 'BY', 'BE', 'BA', 'BG', 'HR', 'CY', 'CZ', 'DK', 'EE', 'FO', 'FI', 'FR', 'GE', 'DE', 'GI',
              'GR', 'GG', 'HU', 'IS', 'IE', 'IM', 'IT', 'JE', 'XK', 'LV', 'LI', 'LT', 'LU', 'MK', 'MT', 'MD', 'MC', 'ME', 'NL', 'NO',
              'PL', 'PT', 'RO', 'RU', 'SM', 'RS', 'SK', 'SI', 'ES', 'SJ', 'SE', 'CH', 'TR', 'UA', 'GB', 'VA', 'AX', 'UK', 'EU']
          }
        };
        this.options = $.extend(true, {
          container: (mockConsentOptions && mockConsentOptions.container) ? mockConsentOptions.container : document.querySelector('body')
        }, cookieConsentDefaultOptions, this.options, cookieVariables, geoLocationVariable, mockConsentOptions);

        getResponseHeader(mockConsentOptions, this.options);
      }
    };
  }());
  cookieConsentPlugin.init();
  return cookieConsentPlugin;
});