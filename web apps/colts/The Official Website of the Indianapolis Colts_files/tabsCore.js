﻿define('modules/core/tabsCore', ['jquery'], function ($) {
  var defaultOptions = {
    selector: '.d3-o-tabs__wrap',
    events: {
      onItemClick: $.noop,
      onItemKeyUp: $.noop,
      onTabKeyUp: $.noop,
      onInit: $.noop,
      onNavigate: $.noop,
      onTabClick: function (e, opts) {
        var $target = $(e.target);
        if (!$target.is('button')) {
          $target = $target.closest('button');
        }
        var $panel = $($target.attr('href'));
        opts.events.onItemClick(opts, $target, $panel);
      }
    }
  };

  var tabsCore = function (options) {
    var opts = $.extend(true, {}, defaultOptions, options);
    var $tabsWrap = $(opts.selector);

    var deviceAttribute = $tabsWrap.attr('data-device');
    if (typeof (deviceAttribute) !== 'undefined' && deviceAttribute === 'mobile') {
      if (!window.isMobileViewport()) {
        return;
      }
    }

    var $buttons = $tabsWrap.find('button[href!=""]')
      .off('click.tabs').on('click.tabs', function (e) {
        opts.events.onTabClick(e, opts);
      }).off('keyup.tabs').on('keyup.tabs', function (e) {
        opts.events.onTabKeyUp(e, opts);
      });

    var $prevButton = $tabsWrap.find('button[class$="_prev"]')
      .off('click.tabs').on('click.tabs', function (e) {
        e.direction = 'prev';
        opts.events.onNavigate(e, opts);
      });

    var $nextButton = $tabsWrap.find('button[class$="_next"]')
      .off('click.tabs').on('click.tabs', function (e) {
        e.direction = 'next';
        opts.events.onNavigate(e, opts);
      });

    var $items = $tabsWrap.find('nav li')
      .off('keyup.tabs.item').on('keyup.tabs.item', function (e) {
        opts.events.onItemKeyUp(e, opts);
      });

    var $visibleButton = $buttons.filter('[aria-selected="true"]');
    $buttons.filter('[aria-selected="false"]').each(function () {
      var button = this;
      var $panel = $(button.getAttribute('href'));
      if ($panel.length) {
        $panel.attr('aria-hidden', 'true').removeClass('d3-is-selected').hide();
      }
    });
    opts.events.onInit($visibleButton);
  };

  return tabsCore;
});