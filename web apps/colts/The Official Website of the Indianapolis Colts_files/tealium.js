﻿define('modules/tealium', ['jquery'], function ($) {
  var defaultOptions = {
    events: {
      onItemClick: function (tealiumPlugin) {
        return function (e) {
          if (!window.utag) {
            return;
          }
          var $target = $(e.currentTarget);
          if (!$target.is('a[data-link_url]')) {
            $target = $target.closest('a[data-link_url]');
          }

          try {
            var linkType = $target.attr('data-link_type');
            tealiumPlugin.setLinkCookie('linkType', linkType);
            var linkName = $target.attr('data-link_name');
            tealiumPlugin.setLinkCookie('linkName', linkName);
            var linkURL = $target.attr('data-link_url');
            tealiumPlugin.setLinkCookie('linkURL', linkURL);
            var linkPosition = $target.attr('data-link_position');
            tealiumPlugin.setLinkCookie('linkPosition', linkPosition);
          } catch (ex) {
            window.utag.DB(ex);
            tealiumPlugin.removeLinkCookies(['linkType', 'linkName', 'linkURL', 'linkPosition']);
          }
        };
      }
    }
  };
  var tealiumPlugin = (function () {
    return {
      opts: undefined,
      $items: undefined,
      init: function (options) {
        var _this = this;
        _this.opts = $.extend({}, defaultOptions, options);
        $('a[data-link_url]').off('click.tealium').on('click.tealium', _this.opts.events.onItemClick(_this));
      },
      setLinkCookie: function (key, value) {
        var utagKey = 'utag_' + key;
        document.cookie = [utagKey + '=', value, ';path=/;domain=', window.utag.cfg.domain, ';expires='].join('');
        window.utag.data['cp.' + utagKey] = value;
      },
      removeLinkCookies: function (keys) {
        if (!keys) {
          return;
        }
        for (var i = 0; i < keys.length; i++) {
          var utagKey = 'utag_' + keys[i];
          document.cookie = [utagKey + '=;path=/;domain=', window.utag.cfg.domain, ';expires='].join('');
          window.utag.data['cp.' + utagKey] = undefined;
        }
      }
    };
  }());
  tealiumPlugin.init();
  return tealiumPlugin;
});