﻿define('modules/tabs', ['jquery', 'modules/core/tabsCore'], function ($, tabsCore) {
  var tabsPlugin = (function () {

    return {
      init: function () {
        var core = new tabsCore({
          events: {
            onItemClick: function (opts, $target, $panel) {
              var $tabsButtons = $target.closest(opts.selector).find('button[href!=""]');
              $tabsButtons.each(function () {
                var $this = $(this);
                $this.attr('aria-selected', 'false');
                var _$panel = $($this.attr('href'));
                _$panel.removeClass('d3-is-selected').attr('aria-hidden', 'true').hide();
              });
              $target.attr('aria-selected', 'true');
              $panel.addClass('d3-is-selected').attr('aria-hidden', 'false').show();
            },
            onTabKeyUp: function (e, opts) {
              var $target = $(e.target);
              var $listItem = $target.parent();
              var $list = $target.parents(opts.selector);
              var $listItems = $list.children('li');

              var index = $list.children('li').index($listItem);
              requestAnimationFrame(function () {
                switch (e.which) {
                  case 37: //arrow left
                  case 38: //arrow up
                    $listItems.eq(index === 0 ? 0 : index - 1).find('button').focus();
                    break;
                  case 39: //arrow right
                  case 40: //arrow down
                    $listItems.eq(index === $listItems.length - 1 ? $listItems.length - 1 : index + 1).find('button').focus();
                    break;
                }
              });
            }
					}
        });
      }
    };

  }());
  tabsPlugin.init();
  return tabsPlugin;
});