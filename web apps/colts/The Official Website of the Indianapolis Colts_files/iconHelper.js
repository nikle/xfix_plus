﻿define('modules/iconHelper', function () {
  var iconHelper = (function () {
    return {
      get: function (iconIDs, size) {
        size = size || 'x-large';
        if (typeof iconIDs !== typeof []) {
          iconIDs = [iconIDs];
        }
        var i = 0, l = iconIDs.length, sb = [];
        for (i; i < l; i++) {
          var iconID = iconIDs[i];
          var includedIcon = document.querySelector('.nfl-o-icon-included--' + iconID);
          if (!includedIcon) {
            return;
          }
          var iconHtml = includedIcon.innerHTML;
          sb.push('<span class="js-iconhelper--', iconID, ' nfl-o-icon nfl-o-icon--', size, '"><svg class="nfl-o-icon-', iconID, '" viewBox="', includedIcon.getAttribute('data-viewbox'), '">', iconHtml, '</svg></span>');
        }
        return sb.join('');
      }
    };
  }());
  return iconHelper;
});