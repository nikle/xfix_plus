﻿define('modules/search', ['jquery', 'autocomplete'], function ($, autocomplete) {
  var defaultOptions = {
    selector: '#query',
    suggestionSelector: 'query-suggestion',
    appendToSelector: '.d3-o-nav__search-result',
    closeButton: '.d3-o-search__close-btn',
    maxHeight: 700,
    minChars: 3,
    noResultFoundLabel: ' ',
    events: {
      onKeyUp: function (searchPlugin) {
        return function (e) {
          var $target = $(e.target);
          var value = $target.val();
          if (value.length < 3 || e.key === 'ArrowUp' || e.key === 'ArrowDown') {
            document.getElementById(defaultOptions.suggestionSelector).innerHTML = '';
          }
        };
      },
      onCloseButton: function (searchPlugin) {
        return function () {
          document.getElementById(defaultOptions.suggestionSelector).innerHTML = '';
          searchPlugin.$selector.val('');
        };
      }
    },
    resultMapping: function (response) {
      var responseObj = JSON.parse(response);
      return {
        suggestions: $.map(responseObj, function (dataItem) {
          return { value: dataItem.title || dataItem.keyword || '', data: dataItem.url || '' };
        })
      };
    },
    autocompleteInput: function (query, suggestions) {
      var pathRegex = '^' + query;
      var regex = new RegExp(pathRegex, 'i');
      var autocompleteContainer = document.getElementById(defaultOptions.suggestionSelector);
      for (var i = 0; i < suggestions.length; i++) {
        if (suggestions[i].value.match(regex)) {
          autocompleteContainer.innerHTML = suggestions[i].value;
          return;
        }
      }
      autocompleteContainer.innerHTML = '';
    },
    onSelect: function (suggestion) {
      window.location.href = suggestion.data;
    },
    autocompleteConfig: function (searchPlugin) {
      return {
        minChars: searchPlugin.options.minChars,
        maxHeight: searchPlugin.options.maxHeight,
        deferRequestBy: 200,
        appendTo: searchPlugin.options.appendToSelector,
        serviceUrl: '/api/search/autocomplete',
        transformResult: searchPlugin.resultMapping,
        onSearchComplete: searchPlugin.autocompleteInput,
        onSelect: searchPlugin.options.onSelect,
        showNoSuggestionNotice: true,
        noSuggestionNotice: searchPlugin.options.noResultFoundLabel,
        autoSelectFirst: false,
        triggerSelectOnValidInput: false
      };
    }
  };

  var searchPlugin = (function () {
    return {
      $wrap: $(),
      $selector: undefined,
      options: undefined,
      resultMapping: undefined,
      autocompleteInput: undefined,
      $closeButton: undefined,
      minChars: undefined,
      initByElement: function (target) {
        var _this = this;
        _this.$wrap = $(target);
        _this.options = $.extend({}, defaultOptions);
        _this.$closeButton = _this.$wrap.find(_this.options.closeButton);
        _this.$selector = $(_this.options.selector);
        _this.resultMapping = _this.options.resultMapping;
        _this.autocompleteInput = _this.options.autocompleteInput;
        _this.$selector.autocomplete(_this.options.autocompleteConfig(_this));
        _this.$selector.on('keyup.search', _this.options.events.onKeyUp(_this));
        _this.$closeButton.on('click.search', _this.options.events.onCloseButton(_this));
      }
    };
  }());

  return searchPlugin;
});