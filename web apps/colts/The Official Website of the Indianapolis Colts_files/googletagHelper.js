﻿/* Google Publisher Tag reference: https://developers.google.com/doubleclick-gpt/reference */

define('modules/helpers/googletagHelper', ['jquery'], function ($) {
  var googletagHelper = (function () {
    var googletag = window.googletag || {};
    googletag.cmd = window.googletag && window.googletag.cmd ? window.googletag.cmd : [];

    var getSlots = function () {
      return googletag && googletag.pubads ? googletag.pubads().getSlots() || [] : [];
    };

    var getSlotsCount = function () {
      return getSlots().length;
    };

    return {
      setTargeting: function (campaignID, slotNameRegexp) {
        var slotCount = getSlotsCount();
        if (slotCount) {
          googletag.cmd.push(function () {
            var slots = getSlots();
            for (var i = 0; i < slotCount; i++) {
              var slot = slots[i];
              var slotName = slot.o.m;
              if (slotName.match(slotNameRegexp)) {
                slot.clearTargeting();
                slot.setTargeting(campaignID);
              }
            }
          });
          return true;
        }
        return false;
      },

      refreshSlot: function (slotNameRegexp) {
        var slotCount = getSlotsCount();
        if (slotCount) {
          googletag.cmd.push(function () {
            var slots = getSlots();
            for (var i = 0; i < slotCount; i++) {
              var slot = slots[i];
              var slotName = slot.o.m;
              if (googletag.pubads && slotName.match(slotNameRegexp)) {
                googletag.pubads().refresh(slot);
              }
            }
          });
          return true;
        }
        return false;
      },

      setSlots: function (idList) {
        if (typeof (idList) === 'string' || $.isArray(idList)) {
          idList = {
            ids: idList,
            campaignID: 'gameDay',
            size: 'leaderboard'
          };
        }
        var slots = [];
        var gptSlots = window.gptSlots || [];
        var adUnit = window.adUnit || '';
        var gptconfig = window.gptconfig || {};

        var callback = function (id, slots) {
          return (function () {
            var slot = googletag.defineSlot(adUnit, gptconfig.adSizes, id)
              .setTargeting('slot', idList.campaignID)
              .defineSizeMapping(gptconfig.responsiveMappings[idList.size])
              .addService(googletag.pubads());
            gptSlots.push(
              slot
            );
            slots.push(slot);
          });
        };
        for (var i = 0; i < idList.ids.length; i++) {
          googletag.cmd.push(callback(idList.ids[i], slots));
        }

        if (googletag.pubads) {
          googletag.pubads().refresh(slots);
        }
        return slots;
      },

      destroySlots: function (slots) {
        if (!slots || slots.length <= 0) {
          return;
        }
        googletag.cmd.push(function () {
          googletag.destroySlots(slots);
        });
      },

      displaySlot: function (divID) {
        if (typeof (divID) !== 'string') {
          return;
        }
        googletag.cmd.push(function () {
          googletag.display(divID);
        });
      }
    };
  }());

  return googletagHelper;
});