﻿define('modules/iframe', ['jquery'], function ($) {
  var defaultOptions = {
    selector: '.nfl-o-iframe__content',
    mobileOption: 'akmobile=ios',

    setSrcOption: function (src, options, $selector) {
      if (!src || !options) {
        return;
      }

      var srcUpdate;
      var width = defaultOptions.getWidth($selector);
      if (!width) {
        return src + options;
      }
      var rgx = new RegExp(/(width)=[\d]*/gi);

      if (rgx.test(src)) {
        var widthReplace = 'width=' + width;
        srcUpdate = src.replace(rgx, widthReplace);
      }
      else {
        srcUpdate = src + '?width=' + width;
      }
      return srcUpdate;
    },
    getSrc: function ($selector) {
      if (!($selector && $selector.length)) {
        return;
      }
      var initialSrc = $selector.attr('src');
      return initialSrc;
    },
    getWidth: function ($selector) {
      if (!($selector && $selector.length)) {
        return;
      }
      var width;
      if ($selector.parents().find('.d3-l-col__col-12').length) {
        width = $selector.parents().find('.d3-l-col__col-12').first().width();
      }
      else {
        width = window.innerWidth;
      }

      return width;
    },
    events: {
      onChange: function (iframePlugin) {
        return function (event) {
          var $selectors = iframePlugin.$iframe;
          $selectors.each(function (index, selector) {
            var $selector = $(selector);
            var src = iframePlugin.getSrc($selector);
            if (!src) {
              return;
            }
            var updatedSrc = iframePlugin.setSrcOption(src, iframePlugin.options, $selector);
            $selector.attr('src', updatedSrc);
          });
        };
      }
    }
  };
  var iframePlugin = (function () {
    return {
      options: undefined,
      $iframe: undefined,
      $wrapIframe: undefined,
      getSrc: undefined,
      setSrcOption: undefined,
      init: function () {
        var _this = this;
        _this.options = $.extend({}, defaultOptions);
        _this.$iframe = $(_this.options.selector);
        _this.$wrapIframe = $(_this.options.wrapSelector);
        _this.getSrc = _this.options.getSrc;
        _this.setSrcOption = _this.options.setSrcOption;

        $(window).on('load.iframe', _this.options.events.onChange(iframePlugin));
      }
    };
  }());
  iframePlugin.init();
  return iframePlugin;
});