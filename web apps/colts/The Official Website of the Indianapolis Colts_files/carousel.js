﻿define('modules/carousel', ['jquery', 'modules/core/carouselCore', 'modules/iconHelper'], function ($, carousel, iconHelper) {
  var getAutocycleOptions = function (autocycleTime) {
    return {
      owlCarousel: {
        loop: true,
        autoplayHoverPause: true,
        autocycleTime: autocycleTime
      }
    };
  };

  var paddingOptions = {
    owlCarousel: {
      responsive: {
        0: {
          stagePadding: 0
        }
      }
    }
  };

  var onKeyUp = function ($thisCarousel, core) {
    $thisCarousel.on('refreshed.owl.carousel', function (ev) {
      if (!ev.namespace) {
        return;
      }

      var $this = $(this);

      var $outerStage = $this.find('.owl-stage-outer');
      if (!$outerStage.attr('tabindex')) {
        $outerStage.attr('tabindex', '0');
      }

      //owl-stage and owl-items only
      $outerStage.off('keydown').on('keydown', function (e) {
        if (e.which !== 37 && e.which !== 39 && (e.which === 9 && !$(document.activeElement).is('.owl-stage-outer'))) {
          e.preventDefault();
        }
      });

      var moveToNextItem = function (owlCarousel, $owl, itemsPerPage, isLastOwlItem) {
        if (itemsPerPage === 1) {
          owlCarousel.next();
        }
        else {
          if (isLastOwlItem) {
            return;
          }

          var $focusedElement = $(document.activeElement);
          var allItems = $owl.find('.owl-item');

          var currentIndex = allItems.index($focusedElement.parents('.owl-item'));
          var nextIndex = currentIndex + 1;

          var shouldChangePage = nextIndex % itemsPerPage === 0;

          if (shouldChangePage) {
            //moves to the next page
            owlCarousel.next();
          }

          var item = allItems.eq(nextIndex);
          if (item.has('a').length > 0) {
            //item.find('a').focus();
          } else {
            owlCarousel.next();
          }
        }
      };

      var moveToPreviousItem = function (owlCarousel, $owl, itemsPerPage, isFirstOwlItem) {
        if (itemsPerPage === 1) {
          owlCarousel.prev();
        } else {
          if (isFirstOwlItem) {
            return;
          }

          var $focusedElement = $(document.activeElement);
          var allItems = $owl.find('.owl-item');

          var currentIndex = allItems.index($focusedElement.parents('.owl-item'));
          var prevIndex = currentIndex - 1;

          var shouldChangePage = currentIndex % itemsPerPage === 0;

          if (shouldChangePage) {
            //moves to the next page
            owlCarousel.prev();
          }

          var item = allItems.eq(prevIndex);
          if (item.has('a').length > 0) {
            //item.find('a').focus();
          } else {
            owlCarousel.prev();
          }
        }
      };

      $outerStage.off('keyup').on('keyup', function (e) {
        if (e.which !== 9 && e.which !== 37 && e.which !== 39) {
          return;
        }

        var $target = $(e.target);
        var isShiftKeyPressed = e.shiftKey;
        var $owl = $target.parents('.owl-carousel');
        var owlCarousel = $owl.data('owl.carousel');
        var itemsPerPage = owlCarousel && owlCarousel.settings ? owlCarousel.settings.items : undefined;
        itemsPerPage = itemsPerPage || (owlCarousel && owlCarousel.options ? owlCarousel.options.items : 1);
        var $focusedElement = $(document.activeElement);
        var $targetElement = $owl.find('.owl-item.active').first();
        if (itemsPerPage > 1 && isShiftKeyPressed) {
          $targetElement = $owl.find('.owl-item.active').last();
        }
        var type;
        var owlLength = owlCarousel.items().length;
        var owlItemIndex = $owl.find('.owl-item').index($focusedElement.is('a') ? $focusedElement.parents('.owl-item') : $targetElement);
        var isFirstOwlItem = owlItemIndex === 0;
        var isLastOwlItem = owlItemIndex === owlLength - 1;

        switch (e.which) {
          case 9:   //tab
            if (isShiftKeyPressed) {
              if (isFirstOwlItem) {
                //$owl.find('.owl-stage-outer').focus(); //focus on stage
                return;
              } else {
                type = 'prev';
              }
            } else {
              if (isFirstOwlItem && $focusedElement.is('a')) {
                type = 'next';
              } else if (isFirstOwlItem && !$focusedElement.is('a')) {
                var item = $owl.find('.owl-item').first();
                if (item.has('a').length > 0) {
                  //item.find('a').focus();
                } else {
                  //set focus to owl-nav
                  //$owl.find('.owl-nav button').first().focus();
                  return;
                }
              }
              else if (isLastOwlItem) {
                //$owl.find('.owl-nav button').first().focus();
                return;
              }
              else {
                type = 'next';
              }
            }
            break;

          case 37:  //arrow left
            type = isFirstOwlItem ? undefined : 'prev';
            break;

          case 39:  //arrow right
            type = isLastOwlItem ? undefined : 'next';
            break;
        }

        if (type === 'prev') {
          moveToPreviousItem(owlCarousel, $owl, itemsPerPage, isFirstOwlItem);
        }

        if (type === 'next') {
          moveToNextItem(owlCarousel, $owl, itemsPerPage, isLastOwlItem);
        }
      });

      //navigation buttons only
      $this.find('.owl-nav, .owl-dots').off('keydown').on('keydown', function (e) {
        if (e.which !== 9 && e.which !== 13 && e.which !== 37 && e.which !== 39) {
          return;
        }

        var $target = $(e.target);
        var $owl = $target.parents('.owl-carousel');
        var owlCarousel = $owl.data('owl.carousel');
        var $focusedElement = $(document.activeElement);
        var isShiftKeyPressed = e.shiftKey;
        var type;

        switch (e.which) {
          case 9:
            if ($focusedElement.is('.owl-prev') && isShiftKeyPressed) {
              if (owlCarousel.current() === owlCarousel.items().length - 1) { //if current item is the last one
                //$owl.find('.owl-item.active a').focus(); //no need to change the slide, just set the focus.
                e.preventDefault();
                e.stopPropagation();
              } else {
                owlCarousel.to(owlCarousel.items().length - 1); //change the current slide to the last one.
              }
              return;
            }
            break;
          case 37:  //arrow left
            type = 'prev';
            break;
          case 39:  //arrow right
            type = 'next';
            break;
        }

        if (type === 'prev') {
          owlCarousel.prev();
        }

        if (type === 'next') {
          owlCarousel.next();
        }
      });
    });

    $thisCarousel.on('translate.owl.carousel', function (e) {
      if (!e.namespace) {
        return;
      }

      var $target = $(e.target);
      var $owl = $target.is('.owl-carousel') ? $target : $target.parents('.owl-carousel');
      var owlCarousel = $owl.data('owl.carousel');
      var $focusedElement = $(document.activeElement);
      var itemsPerPage = owlCarousel && owlCarousel.settings ? owlCarousel.settings.items : undefined;
      itemsPerPage = itemsPerPage || (owlCarousel && owlCarousel.options ? owlCarousel.options.items : 1);

      if ($focusedElement.is('button') && !$focusedElement.is('.owl-prev') && !$focusedElement.is('.owl-next')) {
        if (itemsPerPage === 1) {
          //$owl.find('.owl-dot.active').focus();
        }
      }
    });

    $thisCarousel.on('translated.owl.carousel', function (e) {
      if (!e.namespace) {
        return;
      }

      var $target = $(e.target);
      var $owl = $target.is('.owl-carousel') ? $target : $target.parents('.owl-carousel');
      var owlCarousel = $owl.data('owl.carousel');
      var $focusedElement = $(document.activeElement);
      var itemsPerPage = owlCarousel && owlCarousel.settings ? owlCarousel.settings.items : undefined;
      itemsPerPage = itemsPerPage || (owlCarousel && owlCarousel.options ? owlCarousel.options.items : 1);

      if ($focusedElement.is('a')) {
        if (itemsPerPage === 1) {
          //$owl.find('.owl-item.active a').focus();
        }
      }
    });
  };

  var carouselPosition = function (carousel) {
    var itemsDisplayed = carousel.options.items;
    var leftSidePagination = carousel.relative(carousel.current()) === 0 ? carousel.relative(carousel.current()) + 1
      : Math.ceil((carousel.relative(carousel.current() / itemsDisplayed) + 1));

    var rightSidePagination = Math.ceil(carousel.items().length / itemsDisplayed);

    return leftSidePagination.toString() + ' / ' + rightSidePagination.toString();
  };

  var onChangePagination = function ($thisCarousel, core) {
    $thisCarousel.on('initialize.owl.carousel changed.owl.carousel', function (e) {
      if (!e.namespace) {
        return;
      }

      var carousel = e.relatedTarget;

      var pagination = $(e.target).find('.owl-dots');
      pagination
        .text(carouselPosition(carousel))
        .addClass('owl-dots--visible');
    });
  };

  var onAutoplay = function ($thisCarousel, autoplayTime) {

    //refreshed.owl.carousel is when internal state of plugin changes
    $thisCarousel.on('refreshed.owl.carousel', function (e) {
      var $target = $(e.relatedTarget.$element);
      $target.data('owl.carousel').options.autoplay = true;
      $target.data('owl.carousel').options.autoplayTimeout = autoplayTime;

      $target.trigger('stop.owl.autoplay');
      $target.trigger('play.owl.autoplay', autoplayTime);

      $target.mouseout(function () {
        $target.trigger('stop.owl.autoplay');
        $target.trigger('play.owl.autoplay', autoplayTime);
      });
    });
  };

  var paginationLarge = {
    owlCarousel: {
      dots: false
    }
  };

  var arrowIcon = iconHelper.get('down', 'small');

  var defaultOptions = {
    owlCarousel: {
      loop: false,
      nav: true,
      dots: true,
      navText: [arrowIcon, arrowIcon],
      autoHeight: true,
      responsiveClass: true,
      responsive: {
        0: {
          items: 1,
          stagePadding: 50
        }
      }
    }
  };

  var carouselPlugin = (function () {
    return {
      $carousel: $(),
      init: function (mockWindow) {
        var _this = this;
        $(carousel.defaultOptions.selector).each(function () {
          var $this = $(this);
          var autocycle = $this.data('cycle');
          var autocycleTime = $this.data('speed');
          var isContentTray = $this.hasClass('d3-is-content-tray');
          var isMobileContentTray = (mockWindow || window).isMobileViewport() && isContentTray;
          var itemsDisplayed = parseInt($this.data('items') || 1, 10);
          var itemCount = parseInt($this.data('count') || 1, 10);
          var pageCount = Math.ceil(itemCount / itemsDisplayed);

          var padding = $this.data('padding');
          var opts = $.extend(true, {}, defaultOptions, {
            selector: this,
            owlCarousel: {
              items: itemsDisplayed,
              slideBy: itemsDisplayed,
              responsive: {
                1024: {
                  items: itemsDisplayed,
                  slideBy: itemsDisplayed,
                  margin: 24
                }
              }
            }
          });
          


          if (!isMobileContentTray && autocycle && itemsDisplayed === 1) {
            var autocycleOptions = getAutocycleOptions(autocycleTime);
            opts = $.extend(true, {}, opts, autocycleOptions);
          }

          if (itemCount === 1 || padding !== undefined || itemsDisplayed === 1) {
            opts = $.extend(true, {}, opts, paddingOptions);
          }

          if (pageCount > 5 && itemsDisplayed >= 1) {
            opts = $.extend(true, {}, opts, paginationLarge);
          }

          var core = new carousel.core(opts);

          if (pageCount > 5 && itemsDisplayed >= 1) {
            onChangePagination($this, core);
          }

          onKeyUp($this, core);

          core.initOwl();

          /* This here to fix native wrong owl timeout */
          if (!isMobileContentTray && autocycle && itemsDisplayed === 1) {
            //autocycleTime = autocycleTime;
            onAutoplay($this, autocycleTime);
          }

          _this.$carousel.add(core.$carousel);
        });
      }
    };
  }());

  carouselPlugin.init();
  $(window).on('load.carousel', function () {
    $(carousel.defaultOptions.selector).trigger('refresh.owl.carousel');
  });

  return carouselPlugin;
});