﻿define('modules/services/deferredService', ['jquery'], function ($) {
  var deferredService = (function () {
    return {
      get: function (options) {
        var deferred = $.Deferred();
        var defaultOptions = {
          dataType: 'json'
        };
        var params = $.extend({}, defaultOptions, options);
        if (!params.url || typeof (params.url) !== 'string') {
          return;
        }
        var call = $.ajax(params)
          .done(function (data) {
            deferred.resolve(data);
          })
          .fail(function (jqXHR, textStatus, errorThrown) {
            deferred.reject({
              success: 'ko',
              message: errorThrown
            });
          });
        return deferred.promise();
      },
      getResponseHeader: function (params, headerName) {
        var deferred = $.Deferred();
        if (!params.url || typeof (params.url) !== 'string') {
          return;
        }
        var call = $.ajax(params)
          .done(function (data, textStatus, jqXHR) {
            var responseHeaders;
            if (headerName) {
              responseHeaders = jqXHR.getResponseHeader(headerName);
            }
            else {
              responseHeaders = jqXHR.getAllResponseHeaders();
            }
            deferred.resolve(responseHeaders);
          })
          .fail(function (jqXHR, textStatus, errorThrown) {
            deferred.reject({
              success: 'ko',
              message: errorThrown
            });
          });
        return deferred.promise();
      }
    };
  }());
  return deferredService;
});