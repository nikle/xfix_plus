﻿define('modules/footer', ['modules/core/tabsCore', 'jquery'], function (tabsCore, $) {
  var footerPlugin = (function () {
    return {
      options: {
        onResize: function (footerPlugin) {
          return function () {
            var $selector = $('.d3-o-footer__nav');
            var $panelLink = $('.d3-o-footer__panel-links');
            var initialHeights = parseInt($selector.height(), 10);
            var marginHeight = parseInt($panelLink.css('padding-top'), 10);
            var tabsHeight = parseInt((window.getComputedStyle($selector[0], ':before') || { height: 0 }).height, 10);
            var heights = $('footer nav').map(function () {
              return $(this).height();
            }).get();
            var maxHeight = Math.max.apply(null, heights);
            if (maxHeight > initialHeights) {
              $selector.height(maxHeight + tabsHeight + marginHeight);
            }
            requestAnimationFrame(function () {
              footerPlugin.options.onResize(footerPlugin)();
            });
          };
        }
      },
      init: function () {
        var _this = this;
        var hideButtons = function ($target, opts) {
          var $tabsButtons = $target.closest(opts.selector).find('button[href!=""][aria-selected=true]').not($target);
          $tabsButtons.each(function () {
            var $this = $(this);
            $this.attr('aria-selected', 'false');
            var _$panel = $($this.attr('href'));
            _$panel.removeClass('d3-is-selected').attr('aria-hidden', 'true').hide();
          });
        };
        var core = new tabsCore({
          selector: '.d3-o-footer__nav',
          events: {
            onItemClick: function (opts, $target, $panel) {
              var isMobile = window.isMobileViewport();
              if (isMobile) {
                if ($target.attr('aria-selected') === 'true') {
                  $target.attr('aria-selected', 'false');
                  $panel.removeClass('d3-is-selected').attr('aria-hidden', 'true').hide();
                }
                else {
                  $target.attr('aria-selected', 'true');
                  $panel.addClass('d3-is-selected').attr('aria-hidden', 'false').show();
                }
                hideButtons($target, opts);
              }
              else {
                hideButtons($target, opts);
                $target.attr('aria-selected', 'true');
                $panel.addClass('d3-is-selected').attr('aria-hidden', 'false').show();
              }
            },
            onTabKeyUp: function (e, opts) {
              var $target = $(e.target);
              var $listItem = $target.parent();
              var $list = $target.parents(opts.selector);
              var $listItems = $list.children('li');

              var index = $list.children('li').index($listItem);

              switch (e.which) {
                case 37: //arrow left
                case 38: //arrow up
                  requestAnimationFrame(function () {
                    $listItems.eq(index === 0 ? 0 : index - 1).find('button').focus();
                  });
                  break;
                case 39: //arrow right
                case 40: //arrow down
                  requestAnimationFrame(function () {
                    $listItems.eq(index === $listItems.length - 1 ? $listItems.length - 1 : index + 1).find('button').focus();
                  });
                  break;
              }
            },
            onItemKeyUp: function (e, opts) {
              var isMobile = window.isMobileViewport();
              var $target = $(e.target);
              var $listItem = $target.parent();
              var $list = $target.parents('.d3-o-footer__panel-links');
              var $allLists = $target.parents('nav').find('.d3-o-footer__panel-links');

              var listRowItemIndex = $list.find('li:has(a)').index($listItem);
              var listColItemIndex = $allLists.index($list);

              var isFirstList = listColItemIndex === 0;
              var isLastList = listColItemIndex === ($allLists.length - 1);

              var isFirstListItem = listRowItemIndex === 0;
              var isLastListItem = listRowItemIndex === ($list.find('li:has(a)').length - 1);

              var $firstSiblingListItems;

              switch (e.which) {
                case 37: //arrow left
                  requestAnimationFrame(function () {
                    if (!isMobile) {
                      $firstSiblingListItems = $allLists.eq(isFirstList ? $allLists.length - 1 : listColItemIndex - 1).find('li:has(a)');
                      $firstSiblingListItems.eq(listRowItemIndex).find('a').focus();
                    } else {
                      if (!isFirstListItem) {
                        $listItem.prev('li:has(a)').find('a').focus();
                      } else {
                        if (!isFirstList) {
                          $list.prev('ul').find('li:has(a)').last().find('a').focus();
                        }
                      }
                    }
                  });
                  break;
                case 39: //arrow right
                  requestAnimationFrame(function () {
                    if (!isMobile) {
                      $firstSiblingListItems = $allLists.eq(isLastList ? 0 : listColItemIndex + 1).find('li:has(a)');
                      $firstSiblingListItems.eq(listRowItemIndex).find('a').focus();
                    } else {
                      if (!isLastListItem) {
                        $listItem.next('li:has(a)').find('a').focus();
                      } else {
                        if (!isLastList) {
                          $list.next('ul').find('li:has(a)').first().find('a').focus();
                        }
                      }
                    }
                  });
                  break;
                case 38: //arrow up
                  requestAnimationFrame(function () {
                    if (!isMobile) { //desktop
                      if (!isFirstListItem) {
                        $listItem.prev('li:has(a)').find('a').focus();
                      } else if (!isFirstList) {
                        $list.prev('ul').last('li:has(a)').find('a').focus();
                      }
                    } else { //mobile
                      if (!isFirstListItem) {
                        $listItem.prev('li:has(a)').find('a').focus();
                      } else {
                        if (!isFirstList) {
                          $list.prev('ul').find('li:has(a)').last().find('a').focus();
                        }
                      }
                    }
                  });
                  break;
                case 40: //arrow down
                  requestAnimationFrame(function () {
                    if (!isMobile) { //desktop
                      if (!isLastListItem) {
                        $listItem.next('li:has(a)').find('a').focus();
                      } else {
                        $list.next('ul').first('li:has(a)').find('a').focus();
                      }
                    } else { //mobile
                      if (!isLastListItem) {
                        $listItem.next('li:has(a)').find('a').focus();
                      } else {
                        if (!isLastList) {
                          $list.next('ul').find('li:has(a)').first().find('a').focus();
                        }
                      }
                    }
                  });
                  break;
              }
            },
            onInit: function ($visibleButton) {
              var isMobile = window.isMobileViewport();
              if (isMobile) {
                $visibleButton.attr('aria-selected', 'false');
                var $panel = $($visibleButton.attr('href'));
                if ($panel.length) {
                  $panel.attr('aria-hidden', 'true').removeClass('d3-is-selected').hide();
                }
              }
              else {
                requestAnimationFrame(function () {
                  _this.options.onResize(footerPlugin)();
                });
              }
            }
          }
        });
      }
    };
  }());
  footerPlugin.init();
  return footerPlugin;
});