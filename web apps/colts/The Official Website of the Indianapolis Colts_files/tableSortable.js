﻿
//Note: tableSorter will assign best parser that is applicable (e.g. use digit default parser if text is a digit). To override this simply add the following attribute to the 'th' element:  class="{sorter: '<parser id>'}"
define('modules/tableSortable', ['jquery', 'jquery_metadata', 'tablesorter', 'modules/iconHelper'], function ($, $metadata, tablesorter, iconHelper) {
  var tableSortablePlugin = (function () {
    var defaultOptions = {
      events: {
        onInitialized: function (table) {
          var $th = table.find('th');
          if (!$th.length) {
            return;
          }
          $th.each(function () {
            $(this)
              .attr('role', 'columnheader')
              .attr('scope', 'col')
              .attr('aria-sort', 'none');
          });
        },
        onSortEnd: function (e) {
          var $table = $(e.target);
          $table.find('.selected').removeClass('selected');
          $table.find('th').attr('aria-sort', 'none');
          $table.find('.headerSortDown, .headerSortUp').each(function (idx) {
            var $th = $(this);
            $table.find('td:nth-child(' + ($th.index() + 1) + ')').addClass('selected');

            if ($th.hasClass('headerSortUp')) {
              $th.attr('aria-sort', 'descending');
            } else if ($th.hasClass('headerSortDown')) {
              $th.attr('aria-sort', 'ascending');
            }
          });
        }
      }
    };

    var numericComposedDataParser = {
      id: 'tie',
      is: function (s, table, cell) {
        return cell.className === 'sorter-tie';
      },
      format: function (s) {
        var numberArray = s.split(' - ');
        var result = 0;
        var win = numberArray[0];
        var lose = numberArray[1];
        var tie = !isNaN(numberArray[2]) ? numberArray[2] : 0;

        result = win * 4 - lose * 2 - tie;

        return result;
      },
      type: 'numeric'
    };

    var stringSingleDataParser = {
      id: 'strike',
      is: function (s, table, cell) {
        return cell.className === 'sorter-strike';
      },
      format: function (s) {
        var charValue = s.replace(/\s+/, '');
        var numberValue = charValue[0];
        var stringValue = charValue[1];
        var valueIntMap = 0;
        switch (stringValue.toLowerCase()) {
          case 'w':
            valueIntMap = 4;
            break;
          case 'l':
            valueIntMap = -2;
            break;
          case 't':
            valueIntMap = -1;
            break;
        }

        var result = numberValue * valueIntMap;
        return result;
      },
      type: 'numeric'
    };

    var attributeDataParser = {
      id: 'attribute',
      is: function (s, table, cell) {
        return cell.className === 'sorter-attribute';
      },
      format: function (s, table, cell) {
        var sortValue = cell.getAttribute('data-value') || s;
        return sortValue;
      }
    };

    var lastNameDataParser = {
      id: 'lastname',
      is: function (s, table, cell) {
        return cell.className === 'sorter-lastname';
      }, 
      format: function (s, table, cell) {
        var dataNameElement = cell.querySelector('*[data-name]');
        var fullName = dataNameElement ? dataNameElement.getAttribute('data-name') : s;
        return fullName;
      },
      type: 'text'
    };

    var heightDataParser = {
      id: 'height',
      is: function (s, table, cell) {
        return cell.className === 'sorter-height';
      },
      format: function (s, table, cell) {
        if (!s) {
          return -1;
        }
        var heightRegex = new RegExp(/\d+/g);
        var feet = heightRegex.exec(s);
        if (!feet) {
          return -1;
        }
        var inches = heightRegex.exec(s);
        if (inches > 0) {
          return ((+feet[0]) * 12) + (+inches[0]);
        }
        return ((+feet[0]) * 12);
      },
      type: 'numeric'
    };

    var appendDataParser = {
      id: 'append',
      is: function (s, table, cell) {
        return cell.className === 'sorter-append';
      },
      format: function (s, table, cell) {
        var dataAppendPosition = cell.getAttribute('data-append');
        if (!dataAppendPosition) {
          return s;
        }
        var dataCell = cell.parentNode.querySelector('td:nth-child(' + dataAppendPosition + ')');
        if (!dataCell) {
          return s;
        }
        var dataName = dataCell.getAttribute('data-name');
        if (!dataName) {
          var dataNameElement = dataCell.querySelector('*[data-name]');
          dataName = dataNameElement.getAttribute('data-name') || s;
        }
        var tempValue = cell.getAttribute('data-value') || s;
        var value = /\d+/.test((tempValue)) ? padLeft(tempValue, 3) : $.trim(tempValue.toString());
        var result = value + ' ' + dataName;
        return result;
      },
      type: 'text'
    };

    var customHeightDataParser = {
      id: 'customHeight',
      is: function (s, table, cell) {
        return cell.className === 'sorter-custom-height';
      },
      format: function (s, table, cell) {
        var dataValue = cell.getAttribute('data-name');
        var height;
        if (!s) {
          return padLeft(height, 3) + dataValue;
        }
        var heightRegex = new RegExp(/\d+/g);
        var feet = heightRegex.exec(s);
        if (!feet) {
          return height + dataValue;
        } else {
          height = ((+feet[0]) * 12);
        }

        var inches = heightRegex.exec(s);
        if (inches > 0) {
          height = height + (+inches[0]);
        }
        return padLeft(height, 3) + dataValue;
      },
      type: 'text'
    };

    function padLeft (val, width) {
      val = val + '';
      return val.length >= width ? val : new Array(width - val.length + 1).join('0') + val;
    }

    return {
      init: function (options) {
        var opts = $.extend({}, defaultOptions, options);
        $.tablesorter.addParser(numericComposedDataParser);
        $.tablesorter.addParser(stringSingleDataParser);
        $.tablesorter.addParser(lastNameDataParser);
        $.tablesorter.addParser(heightDataParser);
        $.tablesorter.addParser(appendDataParser); 
        $.tablesorter.addParser(customHeightDataParser);
        $.tablesorter.addParser(attributeDataParser);
        var $sortableTables = $('.d3-o-table--sortable')
          .tablesorter()
          .on('tablesorter-initialized', opts.events.initialized)
          .on('sortEnd', opts.events.onSortEnd);
        $sortableTables.find('th').prepend(iconHelper.get('down', 'small'));
        $sortableTables.each(function () {
          opts.events.onInitialized($(this));
          opts.events.onSortEnd({ target: this });
        });
        this.options = opts;
      }
    };
  }());
  tableSortablePlugin.init();
  return tableSortablePlugin;
});