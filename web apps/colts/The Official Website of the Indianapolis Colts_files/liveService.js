﻿define('modules/services/liveService', ['jquery', 'modules/services/deferredService', 'modules/helpers/googletagHelper', 'modules/lazyload'], function ($, deferredService, googletagHelper, lazyLoadHelper) {
  var getModules = function () {
    return $('*[data-live]');
  };
  var getLiveModules = function () {
    return $('*[data-live="true"]');
  };

  var timer;
  var firstAvailabelGameTime;
  var TIMER_INTERVAL = 30000;

  var getTimeInterval = function () {
    var $reloadTime = $('*[data-reload-time]');
    if (!$reloadTime.length) {
      return TIMER_INTERVAL;
    }
    var reloadTime = $reloadTime.first().attr('data-reload-time');
    if (!reloadTime) {
      return TIMER_INTERVAL;
    }
    return reloadTime;
  };

  var reloadLiveModules = function () {
    var modulesConfig = getModulesConfig();
    var promises = [];
    var newHtml = [];

    $.each(modulesConfig, function (index) {
      var deferred = $.Deferred();
      deferredService.get({
        url: '/api/lazy/load',
        dataType: 'html',
        data: {
          json: JSON.stringify(modulesConfig[index])
        }
      }).done(function (data) {
        newHtml.push(data);
        deferred.resolve(newHtml);
      });

      promises.push(deferred);
    });

    return $.when.apply($, promises).promise();
  };

  var getModulesConfig = function () {
    var $modules = getModules();
    var modulesConfig = $modules.map(function () {
      var $module = $(this);
      return JSON.parse($module.attr('data-json-module'));
    }).toArray();
    return modulesConfig;
  };

  var updateModules = function (newHtml) {
    var $newDom = $(newHtml);
    var $newModules = $newDom.filter('*[data-live]');
    var $oldModules = getModules();
    // Find old module in new modules with data-guid and replace it
    $newModules.each(function () {
      var $newModule = $(this);
      var $oldModule = $oldModules.filter('*[data-guid="' + $newModule.attr('data-guid') + '"]');

      if (!$oldModule.length) {
        return;
      }

      lazyLoadHelper.handleLazyCollection($newModule.find('*[is-lazy]'), {
        skipVisibilityCheck: true,
        applyImmediate: true
      });

      if (areEqual($newModule.clone(), $oldModule.clone())) {
        return;
      }
    
      var $oldModuleADV = $oldModule.find('.d3-o-adv-block');
      var $newModuleADV = $newModule.find('.d3-o-adv-block');
      var $newModuleScript = $newModule.find('script');
      // ADV Switch
      if ($newModuleADV.length) {
        $newModuleADV.each(function (id) {
          $newModuleADV.eq(id).attr('id', $oldModuleADV.eq(id).attr('id'));
        });
      }
      // Script remove
      if ($newModuleScript.length) {
        $newModuleScript.remove();
      }

      // Replace Module
      $oldModule.replaceWith($newModule);

      // GTAG display
      if ($newModuleADV.length) {
        $newModuleADV.each(function (index) {
          googletagHelper.displaySlot($newModuleADV.eq(index).attr('id'));
        });
      }
    });
  };

  var setFirstAvailableGameTime = function () {
    if (typeof (firstAvailabelGameTime) === 'undefined') {
      var gamesTimeList = getModules().map(function () {
        var dateTimeAttr = $(this).attr('data-gametime');
        if (dateTimeAttr) {
          return Date.parse(dateTimeAttr);
        }
      });
      if (gamesTimeList.length) {
        firstAvailabelGameTime = Math.min.apply(null, $.makeArray(gamesTimeList));
      }
    }
  };

  var getUTCTime = function () {
    var now = new Date();
    return Date.UTC(
      now.getUTCFullYear(),
      now.getUTCMonth(),
      now.getUTCDate(),
      now.getUTCHours(),
      now.getUTCMinutes(),
      now.getUTCSeconds(),
      now.getUTCMilliseconds()
    );
  };

  var canPoll = function () {
    if (getLiveModules().length) {
      return true;
    }
    if (firstAvailabelGameTime) {
      var delay = (300 * 1000); // 5' before game start
      var rangeTime = firstAvailabelGameTime - getUTCTime();
      if ((rangeTime <= delay && rangeTime > 0)) {
        return true;
      }
    }
    return false;
  };

  var areEqual = function ($newModule, $oldModule) {
    $newModule.find('.d3-o-adv-block').remove();
    $newModule.find('script').remove();
    $newModule.find('.js-liveservice-skip').remove();

    $oldModule.find('.d3-o-adv-block').remove();
    $oldModule.find('script').remove();
    $oldModule.find('.js-liveservice-skip').remove();
    
    return $newModule.get(0).outerHTML.replace(/\s/g, '') === $oldModule.get(0).outerHTML.replace(/\s/g, '');
  };

  var liveService = (function () {
    return {
      init: function () {
        setFirstAvailableGameTime();
      },
      start: function () {
        if (canPoll()) {
          reloadLiveModules().done(function (data) {
            var newHtml = data.join('');
            updateModules(newHtml);
          });
          return true;
        }
        return false;
      },
      startTimer: function () {
        if (timer) {
          clearTimeout(timer);
        }
        var liveSvc = this;
        timer = setTimeout(function () {
          liveSvc.start();
          liveSvc.startTimer();
        }, getTimeInterval());
      },
      stopTimer: function () {
        if (timer) {
          clearTimeout(timer);
          timer = false;
        }
      }
    };
  }());
  liveService.init();
  liveService.start();
  liveService.startTimer();
  window.liveService = liveService;
  return liveService;
});
