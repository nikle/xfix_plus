﻿define('modules/core/carouselCore', ['jquery', 'owlcarousel2', 'modules/helpers/browserHelper'], function ($, owlCarousel, browserHelper) {
  var defaultOptions = {
    selector: '.d3-o-carousel',
    owlCarousel: {
      loop: true,
      nav: true,
      dots: true,
      autoHeight: true,
      responsiveClass: true,
      responsive: {
        0: {
          items: 1
        }
      }
    }
  };
  var carouselCore = function (options) {
    var opts = $.extend(true, {}, defaultOptions, options);
    return {
      $carousel: $(opts.selector),
      initOwl: function () {
        //fix bug in which the carousel gets bigger each time it is rendered.
        var userBrowser = browserHelper.getBrowser();
        if (userBrowser && userBrowser.name === 'Safari' && parseInt(userBrowser.version) < 11) {

          //for carousel inside photo gallery
          var photoGallery = this.$carousel.parents('[data-require="modules/photogallery"]');

          if (photoGallery.length) {
            //is contained in a photogallery
            photoGallery.css('max-width', '100%');
            if (photoGallery.parent().length) {
              photoGallery.parent().css('max-width', '100%');
            }
          }

          //for carousel inside photo album
          var photoAlbum = this.$carousel.parents('[data-require="modules/photoalbum"]');

          if (photoAlbum.length) {
            //is contained in a photoAlbum
            photoAlbum.css('max-width', '100%');
            if (photoAlbum.parent().length) {
              photoAlbum.parent().css('max-width', '100%');
            }
          }

          $('body').addClass('nfl-u-ua--safari');
          if (this.$carousel.parent().length) {
            this.$carousel.parent().css({ 'max-width': '100%', 'width': '100%' });
          }
        }
        this.$carousel.owlCarousel(opts.owlCarousel);
        this.$carousel.find('.owl-prev').attr('aria-label', 'Previous');
        this.$carousel.find('.owl-next').attr('aria-label', 'Next');
        if (opts.owlCarousel.dots) {
          var $buttons = this.$carousel.find('.owl-dot');
          if ($buttons.length) {
            $buttons.each(function (index) {
              var $btn = $(this);
              $btn.attr('aria-label', 'Slide ' + (index + 1));
            });
          }
        }
      }
    };
  };
  return {
    defaultOptions: defaultOptions,
    core: carouselCore
  };
});
