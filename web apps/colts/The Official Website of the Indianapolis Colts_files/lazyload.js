﻿define('modules/lazyload', ['jquery_visible', 'jquery'], function ($_visible, $) {
  var lazyLoadHelper = (function () {
    return {
      setSrcSet: function (lazyEffects, opts) {
        return function () {
          var _this = this;
          var frameCallback = function () {
            var srcset = _this.getAttribute('srcset');
            if (!srcset) {
              srcset = _this.getAttribute('data-srcset');
            }
            _this.setAttribute('srcset', srcset.replace(new RegExp(lazyEffects, 'gi'), ''));
          };
          if (opts && opts.applyImmediate) {
            frameCallback();
            return;
          }
          requestAnimationFrame(frameCallback);
        };
      },
      setImgSrc: function (lazyEffects, opts) {
        return function () {
          var _this = this;
          var $img = $(this);
          var frameCallback = function () {
            var src = $img.attr('src');
            if (!src || src === 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==') {
              src = _this.getAttribute('data-src');
            }
            if (!$img.data('lazyEffects')) {
              $img.data('lazyEffects', lazyEffects);
            }
            var pattern = $img.data('lazyEffects');
            $img.attr('src', src.replace(new RegExp(pattern, 'gi'), ''));
          };
          if (opts && opts.applyImmediate) {
            frameCallback();
            return;
          }
          requestAnimationFrame(frameCallback);
        };
      },
      handleLazyCollection: function ($collection, opts) {
        if (!$collection.length) {
          return;
        }
        if (!opts) {
          opts = {};
        }
        var _this = this;
        var $toBeShown;
        $collection.each(function () {
          var $lazy = $(this);
          var isVisible = opts.skipVisibilityCheck || $lazy.visible(true, false, 'both');
          if (isVisible) {
            if (!$toBeShown) {
              $toBeShown = $lazy;
            }
            else {
              $toBeShown = $toBeShown.add($lazy);
            }
            var lazyEffects = this.getAttribute('is-lazy');
            $lazy.find('img').each(_this.setImgSrc(lazyEffects, opts));
            $lazy.find('source').each(_this.setSrcSet(lazyEffects, opts));
          }
        });
        if ($toBeShown && $toBeShown.length) {
          if (window.picturefill) {
            window.picturefill({
              reevaluate: true,
              elements: $toBeShown.toArray()
            });
          }
          $toBeShown.removeAttr('is-lazy');
        }
        return $collection;
      },
      manageLazyness: function () {
        var _this = this;
        _this.animationFrame = requestAnimationFrame(function () {
          _this.handleLazyCollection(_this.$lazyCollection);
          _this.animationFrame = requestAnimationFrame(_this.init.bind(_this));
        });
      },
      $lazyCollection: undefined,
      animationFrame: undefined,
      init: function () {
        var _this = this;
        _this.$lazyCollection = $('*[is-lazy]');
        if ('IntersectionObserver' in window) {
          var lazyImageObserver = new window.IntersectionObserver(function (entries, observer) {
            var i = 0;
            var l = entries.length;
            var $toBeShown;
            for (i; i < l; i++) {
              var entry = entries[i];
              if (!entry.isIntersecting) {
                continue;
              }
              var $lazy = $(entry.target);
              if (!$toBeShown) {
                $toBeShown = $lazy;
              }
              else {
                $toBeShown = $toBeShown.add($lazy);
              }
              var lazyImage = entry.target;
              var lazyEffects = lazyImage.getAttribute('is-lazy');
              $lazy.find('img').each(_this.setImgSrc(lazyEffects));
              $lazy.find('source').each(_this.setSrcSet(lazyEffects));
              observer.unobserve(lazyImage);
            }
            if ($toBeShown && $toBeShown.length) {
              if (window.picturefill) {
                window.picturefill({
                  reevaluate: true,
                  elements: $toBeShown.toArray()
                });
              }
              $toBeShown.removeAttr('is-lazy');
            }
          });
          _this.$lazyCollection.each(function () {
            lazyImageObserver.observe(this);
          });
          return;
        }
        _this.manageLazyness();
      },
      refresh: function () {
        var _this = this;
        _this.init();
      }
    };
  }());
  lazyLoadHelper.init();
  window.lazyLoadHelper = lazyLoadHelper;
  return lazyLoadHelper;
});