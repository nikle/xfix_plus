package eval;

import java.util.concurrent.TimeUnit;

import com.crawljax.browser.EmbeddedBrowser;
import com.crawljax.browser.EmbeddedBrowser.BrowserType;
import com.crawljax.core.CrawljaxRunner;
import com.crawljax.core.configuration.BrowserConfiguration;
import com.crawljax.core.configuration.CrawljaxConfiguration;
import com.crawljax.core.configuration.CrawljaxConfiguration.CrawljaxConfigurationBuilder;
import com.crawljax.plugins.cilla.CillaPlugin;
import com.crawljax.plugins.cilla.IndexCSSPlugin;
import org.junit.jupiter.api.Test;
import xfix.XFixConstants;

public class CillaRunner {

    private static final int waitAfterEvent = 400;
    private static final int waitAfterReload = 400;


    @Test
    public void testCilla(String[] args) {
        String index = "http://localhost:9000/source/bitcoin/index.html";
        CrawljaxConfiguration.CrawljaxConfigurationBuilder builder = CrawljaxConfiguration.builderFor(index);
        builder.crawlRules().insertRandomDataInInputForms(false);

        // Set timeouts
        builder.crawlRules().waitAfterReloadUrl(waitAfterReload, TimeUnit.MILLISECONDS);
        builder.crawlRules().waitAfterEvent(waitAfterEvent, TimeUnit.MILLISECONDS);
//        builder.crawlRules().dontClickChildrenOf("body");
        builder.setMaximumStates(2);

        builder.setMaximumRunTime(30, TimeUnit.SECONDS);

        builder.setBrowserConfig(new BrowserConfiguration(EmbeddedBrowser.BrowserType.FIREFOX, 1));

        builder.addPlugin(new IndexCSSPlugin());
        CrawljaxRunner crawljax = new CrawljaxRunner(builder.build());
        crawljax.call();
    }

}
