package xfix;

import xfix.WebDriverSingleton.Browser;

import java.util.*;

public class Constants
{
	public static int FITNESSFUNCTION_DIMENSION = 4;
	public static boolean RUN_IN_DEBUG_MODE = true;
	public static final String REPAIR_CSS_FILENAME = "repair.css";
	public static String INTERNET_EXPLORER_DRIVER_FILEPATH = "E:/lapidary/xfix3/xfix2/exec/IEDriverServer.exe";
	public static String CHROME_DRIVER_FILEPATH = "E:/lapidary/xfix3/xfix2/exec/chromedriver.exe";
	public static Browser REFERENCE_BROWSER = Browser.FIREFOX;
	public static Browser TEST_BROWSER = Browser.FIREFOX;
	
	public static final List<String> NUMERIC_POSITIVE_PROPERTIES = Arrays.asList("height", "max-height", "max-width", "min-height", 
		"min-width", "width", /*"counter-reset",*/ "padding-bottom", "padding-top", "padding-right", "padding-left", /*"border-spacing",
		"background-position",*/ "border-bottom-width", "border-top-width", "border-left-width", "border-right-width", "outline-width", 
		"font-size", "letter-spacing", "line-height","vertical-align","left","float");
	
	public static final List<String> NUMERIC_POSITIVE_NEGATIVE_PROPERTIES = Arrays.asList(/*"counter-increment",*/ "margin-bottom", 
		"margin-top", "margin-right", "margin-left", "bottom", "top", "left", "right", "z-index", "text-indent", /*"vertical-align",*/ 
		"word-spacing","vertical-align","left","float");

	// genetic algorithm
    public static final double OPTIMAL_FITNESS_SCORE = 0.0;
	public static final double[] OPTIMAL_LOCAL_FITNESS_SCORE = {0.0,0.0,0.0,0.0};
    public static final double INITIAL_FITNESS_SCORE = -1;
    public static int SATURATION_POINT = 10;
    
    // AVM search
    public static final int[] EXPLORATORY_MOVES_ARR = {-1, 1};
	public static final Set<String> EXPLORATORY_ENUM_CSS_SET = new HashSet<>(Arrays.asList("vertical-align","float"));
	public static final String[] EXPLORATORY_VERTICAL_ALIGN_ARR = {"top"};
//	public static final String[] EXPLORATORY_LEFT_ARR = {"none"};
	public static final String[] EXPLORATORY_FLOAT_ARR = {"none"};
    public static final int PATTERN_BASE = 2;
	public static final int PATTERN_MAX = 100;
	public static final int ADD_MAX = 2000;
	public static final int DIFF_MAX = 1;
    // XBI
    public static int NEIGHBORHOOD_RADIUS = 2;
    public static final List<String> CONTAINS_XBIS = Arrays.asList("PARENTS DIFFER", "TOP-ALIGNMENT", "BOTTOM-ALIGNMENT", "VMID-ALIGNMENT",
    		"VFILL", "HFILL", "CENTER-ALIGNMENT", "LEFT-JUSTIFICATION", "RIGHT-JUSTIFICATION", "MISSING-PARENT-1", "MISSING-PARENT-2");
    public static final List<String> SIBLING_XBIS = Arrays.asList("MISSING-SIBLING-2", "MISSING-SIBLING-1", "TOP-EDGE-ALIGNMENT", 
    		"RIGHT-EDGE-ALIGNMENT", "BOTTOM-EDGE-ALIGNMENT", "LEFT-EDGE-ALIGNMENT", "TOP-BOTTOM", "BOTTOM-TOP", "LEFT-RIGHT", "RIGHT-LEFT");

    // Methods
	public static List<String> getAllProperties()
	{
		List<String> allPropertiesList = new ArrayList<String>(NUMERIC_POSITIVE_NEGATIVE_PROPERTIES);
		allPropertiesList.addAll(NUMERIC_POSITIVE_PROPERTIES);
		
		return allPropertiesList;
	}
}
