package xfix;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import xfix.fitness.xbi.XbiUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util
{
	public static List<Integer> getNumbersFromString(String string)
	{
		List<Integer> numbers = new ArrayList<Integer>();
		Pattern p = Pattern.compile("-?\\d+");
		Matcher m = p.matcher(string);
		while (m.find())
		{
			numbers.add(Integer.valueOf(m.group()));
		}
		return numbers;
	}

	public static String getUnitFromStringValue(String string)
	{
		Pattern p = Pattern.compile("[a-zA-Z%]+");
		Matcher m = p.matcher(string);
		String returnValue = "";
		while (m.find())
		{
			returnValue = m.group();
		}
		return returnValue;
	}

	public static double convertNanosecondsToSeconds(long time)
	{
		return (double) time / 1000000000.0;
	}

	public static Properties readPropertiesFile(String propertiesFilePath)
	{
		// Read properties file.
		File file = new File(propertiesFilePath);
		Properties properties = new Properties();
		try
		{
			properties.load(new FileInputStream(file));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return properties;
	}

	public static void applyNewValues(RootCauseList chromosome, WebDriver d)
	{
		 Map<String,String> PADDINGS = new HashMap<String, String>();
		 PADDINGS.put("padding-left","paddingLeft");
		 PADDINGS.put("margin-left","marginLeft");
		for(RootCause gene : chromosome.getGenes())
		{
			// check which properties have updated values
			for(String prop : gene.getProcessedProperties())
			{
				String value = gene.getValue(prop);

				System.out.println("Applying " + prop + " = " + value + " to xpath = " + gene.getXpath());

				// modify test page with the new values
				try
				{
					WebElement e = d.findElement(By.xpath(getNormalizedXpath(gene.getXpath())));
					if(PADDINGS.containsKey(prop)){
						((JavascriptExecutor) d).executeScript("arguments[0].style."+
								PADDINGS.get(prop)+" = '"+value+"';", e);
					}else {
						((JavascriptExecutor) d).executeScript("arguments[0].style[arguments[1]] = arguments[2];", e, prop, value);
					}



				}
				catch(NoSuchElementException e)
				{
					System.err.println("Element " + gene.getXpath() + " not found in " + d.getCurrentUrl());
				}
			}
		}
	}

	public static String getNormalizedXpath(String xpath)
	{
		String normalizedXpath = xpath;

		String pattern1 = "(?<=/)([a-zA-Z0-9]*?)(?=/)";
		Pattern r1 = Pattern.compile(pattern1);
		Matcher m1 = r1.matcher(xpath);
		while (m1.find())
		{
			normalizedXpath = normalizedXpath.replaceFirst(pattern1, m1.group(0) + "[1]");
		}

		String pattern2 = "([a-zA-Z0-9]+?)$";
		Pattern r2 = Pattern.compile(pattern2);
		Matcher m2 = r2.matcher(xpath);
		if (m2.find())
		{
			normalizedXpath = normalizedXpath.replaceFirst(pattern2, m2.group(0) + "[1]");
		}

		return normalizedXpath;
	}
	public static boolean doubleArrayEqual(double[] a,double[] b){
		if(a.length != b.length){
			return false;
		}
		for (int i = 0;i<a.length;i++){
			if(Math.abs(a[i] - b[i]) > 0.1){
				return false;
			}
		}
		return  true;
	}
	public static boolean doubleArrayAbsAllLess(double[] a,double[] b){
		if(a.length != b.length){
			return false;
		}
		for (int i = 0;i<a.length;i++){
			if(Math.abs(a[i]) > Math.abs(b[i])){
				return false;
			}
		}
		return  true;
	}

	public static double doubleArrayMaxDiff(double[] a,double[] b){
		if(a.length != b.length){
			return 0;
		}
		double diff = 0;
		for (int i = 0 ; i< a.length;i++){
			if(Math.abs(a[i]-b[i]) > diff){
				diff = Math.abs(a[i]-b[i]);
			}
		}

		return diff;
	}
	public static boolean doubleArrayTooSmall(double[] a){
		for (int i = 0 ; i< a.length;i++){
			if(Math.abs(a[i]) > 1){
				return false;
			}
		}
		return true;
	}
}
