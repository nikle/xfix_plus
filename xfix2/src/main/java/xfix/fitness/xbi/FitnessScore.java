package xfix.fitness.xbi;

public class FitnessScore {
    double upDiff;
    double downDiff;
    double leftDiff;
    double rightDiff;

    public FitnessScore(){}

    public FitnessScore(double up, double down,
                        double left, double right){
        this.downDiff = down;
        this.upDiff = up;
        this.leftDiff = left;
        this.rightDiff = right;
    }
    public double getUpDiff() {
        return upDiff;
    }

    public void setUpDiff(double upDiff) {
        this.upDiff = upDiff;
    }

    public double getDownDiff() {
        return downDiff;
    }

    public void setDownDiff(double downDiff) {
        this.downDiff = downDiff;
    }

    public double getLeftDiff() {
        return leftDiff;
    }

    public void setLeftDiff(double leftDiff) {
        this.leftDiff = leftDiff;
    }

    public double getRightDiff() {
        return rightDiff;
    }

    public void setRightDiff(double rightDiff) {
        this.rightDiff = rightDiff;
    }

    @Override
    public boolean equals(Object obj) {
        FitnessScore f = (FitnessScore)obj;
        return f.getDownDiff() == this.getDownDiff() && f.getLeftDiff() == this.getLeftDiff() &&
                f.getRightDiff() == this.getRightDiff() && f.getUpDiff() == this.getUpDiff();
    }
}
