package xfix.input.xbi;

public class XbiCoord {
    public int left;
    public int top;
    public int right;
    public int bottom;
    public int area;
    public XbiCoord(int left,int top ,int right,int bottom){
        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
        this.area = (right-left)*(bottom-top);
    }
    public XbiCoord(String left,String top ,String right,String bottom){
        this.left = Integer.parseInt(left);
        this.top = Integer.parseInt(top);
        this.right = Integer.parseInt(right);
        this.bottom = Integer.parseInt(bottom);
        this.area = (this.right-this.left)*(this.bottom-this.top);
    }

    @Override
    public String toString() {
        return "[" + left+ ", "+ top+ ", "+ right+ ", "+ bottom +"]";
    }
}
