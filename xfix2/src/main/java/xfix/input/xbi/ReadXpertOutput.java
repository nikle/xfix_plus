package xfix.input.xbi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ReadXpertOutput
{
	private Set<String> reportedElementSetRef;
	private Set<String> reportedElementSetTest;
	private int totalNumberOfXBIsReported;
	private Set<String> xbiStrings;
	private List<XpertXbi> xpertXbis;

	public ReadXpertOutput()
	{
		reportedElementSetRef = new HashSet<String>();
		reportedElementSetTest = new HashSet<String>();
		xbiStrings = new HashSet<>();
		xpertXbis = new ArrayList<>();
	}

	public Set<String> getReportedElementSetRef() {
		return reportedElementSetRef;
	}

	public void setReportedElementSetRef(Set<String> reportedElementSetRef) {
		this.reportedElementSetRef = reportedElementSetRef;
	}

	public Set<String> getReportedElementSetTest() {
		return reportedElementSetTest;
	}

	public void setReportedElementSetTest(Set<String> reportedElementSetTest) {
		this.reportedElementSetTest = reportedElementSetTest;
	}

	public int getTotalNumberOfXBIsReported() {
		return totalNumberOfXBIsReported;
	}

	public void setTotalNumberOfXBIsReported(int totalNumberOfXBIsReported) {
		this.totalNumberOfXBIsReported = totalNumberOfXBIsReported;
	}

	public Set<String> getXbiStrings()
	{
		return xbiStrings;
	}

	public void setXbiStrings(Set<String> xbis)
	{
		this.xbiStrings = xbis;
	}

	public List<XpertXbi> getXpertXbis()
	{
		return xpertXbis;
	}

	public void setXpertXbis(List<XpertXbi> xpertXbis)
	{
		this.xpertXbis = xpertXbis;
	}

	public void readInputFromFile(String reportPath) throws IOException
	{
		if(!new File(reportPath).exists())
			return;

		BufferedReader br = new BufferedReader(new FileReader(reportPath));
		String line = "";
		while((line = br.readLine()) != null)
		{
			xbiStrings.add(line);

			line = line.replace("\"", "");
			String[] lineArr = line.split(",");
			XpertXbi xbi = cleanseInput(lineArr);
			totalNumberOfXBIsReported++;
		}
		br.close();
	}

	public void readInputFromString(String issues)
	{
		if(issues == null || issues.isEmpty())
			return;

		String[] issuesArr = issues.split(System.getProperty("line.separator"));
		for(int i = 0; i < issuesArr.length; i++)
		{
			xbiStrings.add(issuesArr[i]);

			issuesArr[i] = issuesArr[i].replace("\"", "");
			String[] lineArr = issuesArr[i].split(",");
			xpertXbis.add(cleanseInput(lineArr));
			totalNumberOfXBIsReported++;
		}
	}

	private XpertXbi cleanseInput(String[] lineArr)
	{
		XpertXbi xbi = new XpertXbi();
		xbi.setLabel(lineArr[0]);
		String refElement = lineArr[1];
		String testElement = lineArr[2];
		refElement = refElement.replace("(", "");
		refElement = refElement.replace(")", "");
		refElement = refElement.replace("*-*", "");
		refElement = refElement.trim();

		testElement = testElement.replace("(", "");
		testElement = testElement.replace(")", "");
		testElement = testElement.replace("*-*", "");
		testElement = testElement.trim();

		if(refElement.contains("-/"))
		{
			String[] eles = refElement.split("-/");
			eles[1] = "/" + eles[1];
			reportedElementSetRef.add(eles[0].toLowerCase());
			reportedElementSetRef.add(eles[1].toLowerCase());

			xbi.setE1Ref(eles[0].toLowerCase());
			xbi.setE2Ref(eles[1].toLowerCase());
		}
		else if(!refElement.isEmpty())
		{
			refElement = refElement.toLowerCase();
			reportedElementSetRef.add(refElement);
			xbi.setE1Ref(refElement);
			xbi.setE2Ref(refElement);
		}

		if(testElement.contains("-/"))
		{
			String[] eles = testElement.split("-/");
			eles[1] = "/" + eles[1];
			reportedElementSetTest.add(eles[0].toLowerCase());
			reportedElementSetTest.add(eles[1].toLowerCase());

			xbi.setE1Test(eles[0].toLowerCase());
			xbi.setE2Test(eles[1].toLowerCase());
		}
		else if(!testElement.isEmpty())
		{
			testElement = testElement.toLowerCase();
			reportedElementSetTest.add(testElement);
			xbi.setE1Test(testElement);
			xbi.setE2Test(testElement);
		}
		String refCoord = lineArr[3].replace("(","").replace(")","");
		String testCoord = lineArr[4].replace("(","").replace(")","");
		if(refCoord.contains("-1")){
			xbi.setRefArea(new XbiCoord(-1,-1,-1,-1));
		}else if(refCoord.contains("&")){ //Ex. (680#794#1430#818)-(680#1125#1430#1436)
			String[] ref = refCoord.split("&");
			String[] refC1 = ref[0].split("#");
			String[] refC2 = ref[1].split("#");
			xbi.setRefArea(new XbiCoord(calMin(refC1[0],refC2[0]),calMin(refC1[1],refC2[1]),
					calMax(refC1[2],refC2[2]),calMax(refC1[3],refC2[3])));
		}
		if(testCoord.contains("-1")){
			xbi.setTestArea(new XbiCoord(-1,-1,-1,-1));
		}else if(testCoord.contains("&")){
			String[] test = testCoord.split("&");
			String[] testC1 = test[0].split("#");
			String[] testC2 = test[1].split("#");
			xbi.setTestArea(new XbiCoord(calMin(testC1[0],testC2[0]),calMin(testC1[1],testC2[1]),
					calMax(testC1[2],testC2[2]),calMax(testC1[3],testC2[3])));
		}
		return xbi;
	}
	private int calMin(String num1,String num2){
		int a = Integer.parseInt(num1);
		int b = Integer.parseInt(num2);
		return Math.min(a,b);
	}
	private int calMax(String num1,String num2){
		int a = Integer.parseInt(num1);
		int b = Integer.parseInt(num2);
		return Math.max(a,b);
	}
}
