package xfix;

import java.util.Set;

public class OptimalRootCause
{
	private String xpath;
	private String prop;
	private String val;
	private double[] localFitnessScore;
	private Set<String> partXbis;
	private int processingTime;
	private boolean improvement;
	public boolean isImprovement() {
		return improvement;
	}

	public void setImprovement(boolean improvement) {
		this.improvement = improvement;
	}

	public String getXpath() {
		return xpath;
	}

	public void setXpath(String xpath) {
		this.xpath = xpath;
	}

	public String getProp() {
		return prop;
	}

	public void setProp(String prop) {
		this.prop = prop;
	}

	public String getVal() {
		return val;
	}

	public void setVal(String val) {
		this.val = val;
	}

	public double[] getLocalFitnessScore() {
		return localFitnessScore;
	}

	public void setLocalFitnessScore(double[] localFitnessScore) {
		this.localFitnessScore = localFitnessScore;
	}

	public int getProcessingTime() {
		return processingTime;
	}

	public void setProcessingTime(int processingTime) {
		this.processingTime = processingTime;
	}

	public double getGlobalFitnessScore() {
		return globalFitnessScore;
	}

	public void setGlobalFitnessScore(double globalFitnessScore) {
		this.globalFitnessScore = globalFitnessScore;
	}

    public Set<String> getPartXbis() {
        return partXbis;
    }

    public void setPartXbis(Set<String> partXbis) {
        this.partXbis = partXbis;
    }

    private double globalFitnessScore;

	public OptimalRootCause(String xpath, String prop, String val, double[] localFitnessScore,
			int processingTime, boolean improvement, double globalFitnessScore,Set<String> partXbis)
	{
		this.xpath = xpath;
		this.prop = prop;
		this.val = val;
		this.localFitnessScore = localFitnessScore;
		this.processingTime = processingTime;
		this.improvement = improvement;
		this.globalFitnessScore = globalFitnessScore;
		this.partXbis = partXbis;
//		// calculate fitness score improvement value
//		double localFitnessImprovement = initialPartXbi.size() > initialPartXbi.size();
//		this.fitnessScoreImprovement = localFitnessImprovement;
	}
	


//	public int compareTo(OptimalRootCause other)
//	{
//
//		return new Double(localFitnessScore).compareTo(other.localFitnessScore);
//    }
//
	@Override
	public String toString()
	{
		return "<" + xpath + ", " + prop + ", " + val + ", " + printArray( localFitnessScore) + ", "  + ", " + (improvement ? "improvement" : "no improvement") + ", " + ">";
	}

	public String printArray(double[] arr){
		String ret = "[";
		for (double num : arr){
			ret += num + " ";
		}
		ret += "]";
		return ret;
	}
}