﻿# XFix_Plus: A Tool for Automated Repair of Layout Cross Browser Issues (XBIs)

------

**Cross-browser incompatibilities (XBIs)** are discrepancies between a web application's appearance, behavior, or both, when the application is run on two different environments.Such XBIs can negatively impact the functionality of a website as well as users’ impressions of its trustworthiness and reliability. Despite the importance of XBIs, we follow the former work XFix which use guided search-based techniques and improve it to a more stable version, which suppose  to automatically repair these XBIs.

**How to run XFix_Plus?**

 1. Inputs:  
 Run TestXFix.java from the XFix_Plus project by passing the following inputs as Strings:  
(a) URL of the page under test (PUT)  
(b) File system location of the PUT  
(c) Reference browser (FIREFOX/CHROME/INTERNET_EXPLORER) that shows the correct layout of the PUT  
(d) Test browser (FIREFOX/CHROME/INTERNET_EXPLORER) that shows a layout inconsistency with respect to the reference browser.  
 2. Output:   
 The output produced by XFix_Plus can be found in the parent folder of the location provided in input (b):  
(a) log_.txt: stores the detailed log information of one run of the subject. The generated repair patch and summary of the timing results and XBI reduction can be found at the end of this file.  
(b) repair.css: Generated CSS file with the repair patch to fix the observed XBIs. The repair patches are generated specific to the test browser with the relative fix values applied to the CSS properties and their absolute values shown in comments.  
(c) test_fixed.html: Modified PUT with the repair.css file applied.  

**Evaluation Data**  
Subjects:  
The 15 real-world HTML pages used are in the pageData folder. The origin version contains the data derived from former version XFix while the new version means our version.
The screenshot folder includes the screenshots of the project output pages, which also contains origin and new part.  

**web apps**  
This directory contains collected 60 web apps from the internet.  
