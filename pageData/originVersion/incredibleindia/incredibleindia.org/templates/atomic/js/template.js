$(document).ready(function() {
	$('.slideShow').MySlider(5000);

	$(function() {
		$('.mySlider').jcarousel({
			easing: 'linear',
			wrap: 'circular',
			animation: 1000,
			visible: 8,
			scroll: 1
		});
	});

	$('.parent').hover(function() {
		$(this).children('ul').first().slideToggle(400);
	}, function() {
		$(this).children('ul').first().hide();
	});
});