var pathPrefix="";
var saveNavigationPath=1;

var topDX=0;
var topDY=0;
var DX=0;
var DY=0;

var absolutePos=0;
var posX=0;
var posY=0;

var isHorizontal=1;
var menuWidth="100%";  // NEW (NN% or NNpx. Default - 0px)
var blankImage="../images/menu_blank.gif";

var floatable=0;
var floatIterations=5;

var movable=0;
var moveCursor="move";
var moveImage="../images/menu_move.gif";
var moveWidth=12;
var moveHeight=18;

var fontStyle="normal 7pt Verdana, Arial, Helvetica, sans-serif";
var fontColor=["#ffffff","#ffffff"];
var fontDecoration=["none","underline"];

var itemBackColor=["",""];
var itemBorderWidth=0;
var itemAlign="right";
var itemBorderColor=["",""];
var itemBorderStyle=["solid","solid"];
var itemBackImage=["../images/date_blue2.gif","../images/date_blue2.gif"];
var itemSpacing=0;
var itemPadding=3;
var itemCursor="hand";
var itemTarget="_self";

var iconWidth=0;
var iconHeight=0;
var iconTopWidth=0;
var iconTopHeight=0;

var menuBackImage="";
var menuBackColor="";
var menuBorderColor="";
var menuBorderStyle="ridge";
var menuBorderWidth=1;
var subMenuAlign = "left";

var transparency=100;
var transition=39;
var transOptions="";
var transDuration=300;
var shadowColor="";
var shadowLen=0;
var shadowTop=0;

var arrowImageMain=["../images/menu_arrow2.gif","../images/menu_arrow2.gif"];
var arrowImageSub=["../images/menu_arrow.gif","../images/menu_arrow.gif"]; // NEW
var arrowWidth=7;
var arrowHeight=9;

var separatorImage="../images/menu_h_separ.gif";
var separatorWidth="100%";
var separatorHeight="5";
var separatorAlignment="center";
var separatorVImage="../images/menu_v_separ.gif";
var separatorVWidth="5";
var separatorVHeight="16";

var statusString="text";

var pressedItem=-2

var menuStyles = [["menuBorderWidth=0","menuBackImage=../images/1x1.gif"],["menuBorderWidth=1","menuBackImage=../images/1x1.gif"],];