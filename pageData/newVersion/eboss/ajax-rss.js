
// Define a list of Microsoft XML HTTP ProgIDs.
var XMLHTTPREQUEST_MS_PROGIDS = new Array(
  "Msxml2.XMLHTTP.7.0",
  "Msxml2.XMLHTTP.6.0",
  "Msxml2.XMLHTTP.5.0",
  "Msxml2.XMLHTTP.4.0",
  "MSXML2.XMLHTTP.3.0",
  "MSXML2.XMLHTTP",
  "Microsoft.XMLHTTP"
);

// Define ready state constants.
var XMLHTTPREQUEST_READY_STATE_UNINITIALIZED = 0;
var XMLHTTPREQUEST_READY_STATE_LOADING       = 1;
var XMLHTTPREQUEST_READY_STATE_LOADED        = 2;
var XMLHTTPREQUEST_READY_STATE_INTERACTIVE   = 3;
var XMLHTTPREQUEST_READY_STATE_COMPLETED     = 4;


// Returns XMLHttpRequest object.
function getXMLHttpRequest(){
  var httpRequest = null;

  // Create the appropriate HttpRequest object for the browser.
  if (window.XMLHttpRequest != null)
    httpRequest = new window.XMLHttpRequest();
  else if (window.ActiveXObject != null) {
    // Must be IE, find the right ActiveXObject.
    var success = false;
    for (var i = 0;i < XMLHTTPREQUEST_MS_PROGIDS.length && !success;i++){
      try{
        httpRequest = new ActiveXObject(XMLHTTPREQUEST_MS_PROGIDS[i]);
        success = true;
      }catch (ex){}
    }
  }

  // Display an error if we couldn't create one.
  if (httpRequest == null) alert("Error in HttpRequest():\n\n"+ "Cannot create an XMLHttpRequest object.");

  // Return it.
  return httpRequest;
}// end getXMLHttpRequest

function trimSpaces(str) { 
    str.replace(/^\s*/, '').replace(/\s*$/, ''); 

   return str;
} 

// Removes leading whitespaces
function LTrim( value ) {
	
	var re = /\s*((\S+\s*)*)/;
	return value.replace(re, "$1");
	
}

// Removes ending whitespaces
function RTrim( value ) {
	
	var re = /((\s*\S+)*)\s*/;
	return value.replace(re, "$1");
	
}

// Removes leading and ending whitespaces
function trim( value ) {	
	return LTrim(RTrim(value));	
}

var myXmlHttpRequest = getXMLHttpRequest();

var ccid='1';

function load_rss_content(cid){	
  ccid=cid;
  if (cid=='1'){
	myXmlHttpRequest.open('GET','../shared/rss_naftemporiki_news.html',true);
  }else if (cid=='2'){
	myXmlHttpRequest.open('GET','../shared/rss_naftemporiki_fin.html',true);
  }else if (cid=='3'){
	myXmlHttpRequest.open('GET','../shared/rss_naftemporiki_mrk.html',true);
  }else if (cid=='4'){
	myXmlHttpRequest.open('GET','../shared/rss_taxheaven_new.html',true);
  }else if (cid=='5') {
	myXmlHttpRequest.open('GET','../shared/rss_taxheaven_law.html',true);
  }else if (cid=='6'){
	myXmlHttpRequest.open('GET','../shared/rss_taxheaven_dat.html',true);
  }
  myXmlHttpRequest.onreadystatechange = ctnReadyStateChange;
  myXmlHttpRequest.send(null);
}

function ctnReadyStateChange(){	
  if (myXmlHttpRequest.readyState == XMLHTTPREQUEST_READY_STATE_COMPLETED){	 
    if (myXmlHttpRequest.status == 200)   {    	
        var response = trim(myXmlHttpRequest.responseText);
        if(ccid=='1'){		
  	      document.getElementById('rsshtml1').innerHTML = response;	
   		  document.getElementById('rsstab1').className = 'urlbigrtext';
   		  document.getElementById('rsstab2').className = 'urlbigr';
   		  document.getElementById('rsstab3').className = 'urlbigr';
   		  document.getElementById('rssmore1').href = 'http://www.naftemporiki.gr/news/static/rss/news_bus.xml';
        }else if (ccid=='2'){
  	      document.getElementById('rsshtml1').innerHTML = response;	
   		  document.getElementById('rsstab1').className = 'urlbigr';
   		  document.getElementById('rsstab2').className = 'urlbigrtext';
   		  document.getElementById('rsstab3').className = 'urlbigr';
  		  document.getElementById('rssmore1').href = 'http://www.naftemporiki.gr/rssFeed?mode=categ&id=6&atype=story';
        }else if (ccid=='3'){
  	      document.getElementById('rsshtml1').innerHTML = response;	
   		  document.getElementById('rsstab1').className = 'urlbigr';
   		  document.getElementById('rsstab2').className = 'urlbigr';
   		  document.getElementById('rsstab3').className = 'urlbigrtext';
		  document.getElementById('rssmore1').href = 'http://www.naftemporiki.gr/news/static/rss/news_mrk.xml';
        }else if(ccid=='4'){
  	      document.getElementById('rsshtml2').innerHTML = response;
   		  document.getElementById('rsstab4').className = 'urlbigrtext';
   		  document.getElementById('rsstab5').className = 'urlbigr';
   		  document.getElementById('rsstab6').className = 'urlbigr';
		  document.getElementById('rssmore2').href = 'http://www.taxheaven.gr/bibliothiki/soft/xml/soft_new.xml';
        }else if (ccid=='5'){
  	      document.getElementById('rsshtml2').innerHTML = response;	
   		  document.getElementById('rsstab4').className = 'urlbigr';
   		  document.getElementById('rsstab5').className = 'urlbigrtext';
   		  document.getElementById('rsstab6').className = 'urlbigr';
		  document.getElementById('rssmore2').href = 'http://www.taxheaven.gr/bibliothiki/soft/xml/soft_law.xml';
        }else if (ccid=='6'){
  	      document.getElementById('rsshtml2').innerHTML = response;	
   		  document.getElementById('rsstab4').className = 'urlbigr';
   		  document.getElementById('rsstab5').className = 'urlbigr';
   		  document.getElementById('rsstab6').className = 'urlbigrtext';
		  document.getElementById('rssmore2').href = 'http://www.taxheaven.gr/bibliothiki/soft/xml/soft_dat.xml';
        }  
	} else {
      alert("Error: HTTP "+ myXmlHttpRequest.status + " " + myXmlHttpRequest.statusText);
    }
  }
}